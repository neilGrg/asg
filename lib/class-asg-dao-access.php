<?php
/**
 * 
 * This is custom class reponsible for all database 
 * related work
 * @package     Asg
 * @subpackage  lib
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
*/

class ASG_DAO_Access {

	public static $metaDefinition = '';
    public static $tableName = '';
    public static $limit = '';
    public static $offset = '';
   
    /**
     * Insert a new row
     * @global type $wpdb
     * @param type $pairs
     * @return type last inserted id
     */
    public static function insertRow($pairs) {
            global $wpdb;
            $pairs = array_map( 'sanitize_text_field', $pairs );
            $pairs = array_map( 'esc_sql', $pairs );
            $wpdb->insert(self::$tableName, $pairs);
            //echo $wpdb->last_query;die;
            return $wpdb->insert_id;
    }

    /**
     * Select all rows or specific columns
     * @global type $wpdb
     * @param type $extractionType
     * @param type $orderingExpr
     * @param type $limit
     * @return type
     */

    public static function  selectAllRows( $extractionType = "", $colName = array() , $where= array() , $orderingExpr=null, $limit="" ) { 
            global $wpdb;
            if(isset($colName) && !empty($colName)):
               $sql = "SELECT ";
               $sql .= implode(",", $colName);
               $sql .= " ";
               
            else:
              $sql = "SELECT *  ";
            endif;
            $sql .= " FROM  " . self::$tableName ;
            if( 
                isset($where) && 
                is_array($where) &&
                count($where) > 0 
            ) {
                $sql .= " WHERE ";
                foreach ($where as $key => $value) { 
                    $sql .=  is_numeric($value) ?  $key . '=' . $value :  $key . ' like \'%' . $value . '%\' ';
                    unset($where[$key]);
                    $sql .= count($where) > 0 ? " AND " : " ";
                }
            } 
            
            $orderingExpr = !empty($orderingExpr) ?  $orderingExpr : ' ORDER BY id';
            $sql .= $orderingExpr;
            $sql .=  !empty(self::$limit)  ? " LIMIT  " . self::$offset . "," .  self::$limit  : "";
            return $wpdb->get_results($sql,$extractionType);
    }


    /**
     * Select all rows or specific columns
     * @global type $wpdb
     * @param type $extractionType
     * @param type $orderingExpr
     * @param type $limit
     * @return type
     */
 

    public static function  join( $tblName = array() , $on , $colName = array(), $orderingExpr = " ORDER BY t1.id" , $where= array(), $joinType = 'INNER'  ) { 
            global $wpdb; 
            extract($tblName);
            $sql = " SELECT ";
            if(isset($colName) && !empty($colName)):
                foreach ($colName as $key => $col) {
                  $sql .= " $key.`";
                  $sql .= implode("`,$key.`", $col);
                  $sql .= "`, ";
               }
            $sql = substr($sql, 0 , strlen($sql) - 2);
            else:
              $sql .= "  *  ";
            endif;
                
            $sql .= "  FROM ";
            $sql .= " $table1 t1 $joinType JOIN $table2  t2 ON ";
            $sql .= " $on ";
            if( 
                isset($where) && 
                is_array($where) &&
                count($where) > 0 
            ) {
                $sql .= ' WHERE ';

                foreach ($where as $key => $value) {
                    $sql .=  is_numeric($value) ? ' t1.' . $key . '=' . $value : 't1.'. $key . ' like \'%' . $value . '%\' ';
                    unset($where[$key]);
                    $sql .= count($where) > 0 ? " AND " : " ";
                }
            }
            $sql .= $orderingExpr;
            $sql .=  !empty(self::$limit)  ? " LIMIT  " . self::$offset . "," .  self::$limit  : "";
            return $wpdb->get_results($sql , ARRAY_A);
    }

   

    /**
     * Query table
     * @global type $wpdb
     * @param type $where
     * @param type $extractionType
     * @param type $orderingExpr
     * @param type $limit
     * @return type
     */

    public static function  selectRows( $where, $extractionType = "", $colName = array() , $orderingExpr=" ",$limit=""){
            global $wpdb;
            if(isset($colName) && !empty($colName)) {
               $sql = "SELECT ";
               $sql .= implode(",", $colName);
               $sql .= " ";
            } else {
              $sql = "SELECT *  ";
            }
            
            $sql .= "  FROM " . self::$tableName;
            if(count($where) > 0) {
                $sql .=  " where ";
                foreach ($where as $key => $value) {
                    $date = date_parse($value);
                    if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
                            $sql .= ' ' . $key . " = '" . $value . "'";
                    else if(is_numeric($value))
                        $sql .= is_string ($value) ? ' ' . $key . '=' . $value : '';
                    else 
                        $sql .= is_string ($value) ?  ''. $key . ' like \'%' . $value . '%\' ' : ' ' . $key . '=' . $value;
                }
            }
            $orderingExpr = empty($orderingExpr) ? ' ORDER BY id' : '';
            $sql .= ' '. $orderingExpr.' '. $limit;
            return $wpdb->get_results($sql , $extractionType);
    }


    /**
     * count number of rows of set table
     * @global type $wpdb
     * @return type
     */

    public static function  count(){
        global $wpdb;
        $query = "SELECT COUNT(*) FROM  " . self::$tableName;
        return  $wpdb->get_var($query);
    }

     /**
     * Query table
     * @global type $wpdb
     * @param type $where
     * @param type $extractionType
     * @param type $orderingExpr
     * @param type $limit
     * @return type
     */

    public static function  selectRowsUsingLike( $where, $extractionType = "", $colName = array() , $orderingExpr=" ",$limit=""){
            global $wpdb;
            $sql = '';
            if(isset($colName) && !empty($colName)):
               $sql = "SELECT `";
               $sql .= implode("`,`", $colName);
               $sql .= "` ";
               
            else:
              $sql = "SELECT *  ";
            endif;
            $sql .= "  FROM " . self::$tableName . " where `";
            foreach ($where as $key => $value) {
                $sql .= $key . '` like \'%' . $value;
            }
            $sql .= '%\' '. $orderingExpr.' '. $limit;
            return $wpdb->get_results($sql, $extractionType  );
    }

    /**
     * Select rows count
     * @global type $wpdb
     * @return type
     */

    function selectRowsCount(){
            global $wpdb;
            $sql = "select count(*)as count from self::$tableName ";
            return $wpdb->get_results($sql);
    }

    /**
     * Delete row
     * @global type $wpdb
     * @param type $pairs array of paris (columns, new values)
     * @param type $where
     * @param type $whereFormat
     * @return type
     */

    public static function updateRow($pairs,$where,$whereFormat=array("%d")){
        global $wpdb;
        
        $prototypes = array();
        return $wpdb->update(self::$tableName, $pairs, $where);
    }
        
    public static function deleteRow($where){  
            global $wpdb;
            return $wpdb->delete( self::$tableName, $where);
    }
    



}