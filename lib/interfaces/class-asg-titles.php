<?php
  
  /**
   	* This is an simple interface for title
   	* in which each class that implement 
   	* it must use all fields and methods 
   	* declare inside it
	* for auto login form
	* @access public
	* @author 
	* @since  1.0
	* 
	**/

  	interface ASG_Titles {
     
     public function display();
     public function create();
     public function edit();
     public function delete();
     
     
  }