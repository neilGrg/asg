<?php
/*
	Plugin Name: ASG
	Plugin URI:  http://www.wordpress.org/asg
	Description: American Sewing Guild wordpress plugin
	Version: 1.0
	Author: Pidombee
	Text Domain: ASG
	Author URI: http://www.google.np/
 */

	if ( ! defined( 'ABSPATH' ) ) {
		die('Cannot Accessed  directly this file');
	}

	// Template path 
	!defined('TEXT_DOMAIN') ? define('TEXT_DOMAIN', 'ASG' ) : null;
	// Plugin dir path
	!defined('PLUGIN_DIR_PATH') ? define('PLUGIN_DIR_PATH', plugin_dir_path(__FILE__) ) : null;
	// Plugin dir path
	!defined('WPDB_DRIVER') ? define('WPDB_DRIVER', 'mysql' ) : null;
	// Directory separator
	!defined('DS') ? define('DS', DIRECTORY_SEPARATOR) : NULL;
	// Admin dir path
	!defined('ADMIN_DIR_PATH') ? define('ADMIN_DIR_PATH', plugin_dir_path(__FILE__ ) . 'admin' ) : null;
	// Template path 
	!defined('ASG') ? define('ASG', PLUGIN_DIR_PATH .'templates' . DS ) : null;
	// Template path 
	!defined('ADMIN_URL') ? define('ADMIN_URL', admin_url('admin.php')) : null;
	// Template path 
	!defined('PLUGIN_URL') ? define('PLUGIN_URL', plugin_dir_url( __FILE__ )) : null;
	// Template path 
	!defined('VERSION') ? define('VERSION', '1.0') : null;


/**
 * 
 * Main class for ASG to include all library,hooks and filters
 * @package     Asg
 * @subpackage  lib
 * @copyright   Copyright (c) 2015, Neil
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
*/

class ASG {

	private $_libClass = array();
	/**
	 * default upload dir for ASG.
	 *
	 * @since 1.0
	 * @var string
	 */
	public static $upload_dir = '';
	/**
	 * default upload dir for ASG.
	 *
	 * @since 1.0
	 * @var string
	 */
	public static $asg_upload_dir = '';
	/**
	 * default upload dir for ASG.
	 *
	 * @since 1.0
	 * @var string
	 */
	public $asg_plugin_info = array();


	function __construct($uploadDir) {
		//$this->asg_plugin_info = get_plugin_data(__FILE__);
		self:: $upload_dir = $uploadDir;
		self::$asg_upload_dir = self::$upload_dir['baseurl'] . DS . 'asg' . DS ;
		add_action('init',array( $this , 'initializeAllHooks' ));
		add_action('init', array( $this , '_startSession' ));
		$this->_initLoadingClass();
		$this->_include();
		$this->_defineConstants();
		register_activation_hook( __FILE__, array( $this , 'pluginActivationHooks' ) );
		add_action( 'add_meta_boxes', array($this , 'cd_meta_box_add' ));

	}

	function cd_meta_box_add() {
		add_meta_box( 'my-meta-box-id', 'My First Meta Box', 'cd_meta_box_cb', 'post', 'normal', 'high' );
	}

	/**
    * This method is used to generate pdf file for report
    * section
    * @param $name
    * @param $style
    * @return null
    * @since 1.0
    * 
    **/

	private function _generateMembershipCard ($name , $style ) {
		try {
		   $html = unserialize(get_option('report-html'));
		   $mpdf = new mPDF('utf-8','A4');
		   $mpdf->debug = false;
		   $stylesheet = file_get_contents( PLUGIN_URL .'/assets/css/' . $style . '.css' ); // external css
		   $mpdf->WriteHTML($stylesheet,1);
		   $mpdf->WriteHTML($html , 2);
		   $mpdf->Output( $name , 'D');
		} catch(HTML2PDF_exception $e) {
	        echo $e;
	    }
	    exit();
	}

	/**
    * This method is used to generate csv file for print
    * section
    * @param null
    * @return null
    * @since 1.0
    * 
    **/

	private function _generateCsv($name ) {

		if( ! $name) {
	        $name = md5(uniqid() . microtime(TRUE) . mt_rand()). '.csv';
	    }

	    $results = unserialize(get_option('report-csv'));
	    header('Content-Type: text/csv');
	    header('Content-Disposition: attachment; filename='. $name);
	    header('Pragma: no-cache');
	    header("Expires: 0");
	    $header = array('NAME','CHAPTER','ADDRESS','CITY','STATE','ZIP','EMAIL','EXPIRED DATE','PHONE','TITLE1','TITLE2','TITLE3');
	    $outstream = fopen("php://output", "w");
	    fputcsv($outstream, $header);
	    foreach($results as $result) {
	    	fputcsv($outstream, $result);
	    }

	    fclose($outstream);
	    exit();
	}

	

	private function _initLoadingClass() {
		//include  lib class
		$this->_libClass = array(
								'admin' => array(
												array(
													'path' => 'admin/model',
													'class'=> 'title-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'setting-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'report-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'bod-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'chapter-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'title1-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'title2-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'title3-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'table-generator'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'location-insertion'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'member-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'member-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'geolocation-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'state-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'city-model'
												),
												array(
													'path' => 'admin/model',
													'class'=> 'report-model'
												),
											),
								'lib' => array(
											array(
												'path' => '/lib',
												'class'=> 'schema'
											),
											array(
												'path' => '/lib',
												'class'=> 'form-validator'
											),
											array(
												'path' => '/lib',
												'class'=> 'mailchimp'
											),
											array(
												'path' => '/lib',
												'class'=> 'dao-access'
											)											
										),
								'interfaces' => array(
												array(
													'path' => '/lib/interfaces',
													'class'=> 'titles'
												)
											),
								'helper' => array(
												array(
													'path' => '/helper',
													'class'=> 'helper'
												),
												array(
													'path' => '/helper',
													'class'=> 'mailchimp-helper'
												)
											)
								);
	}
	public function initializeAllHooks() { 
		
		$member = new ASG_Member_Model();
		
		//pdf export
		if($_REQUEST['operation'] == 'pdf' && $_REQUEST['page'] == 'asg-admin-sub-page2') {
			$member->generatePdf();
		} else if( $_REQUEST['operation'] == 'pdf' && $_REQUEST['report'] == 'membership_card') {
			$this->_generateMembershipCard('membership-card.pdf' ,'table');
		} else if( $_REQUEST['operation'] == 'pdf' && $_REQUEST['report'] == 'print_label') {
			$this->_generateMembershipCard('print-label.pdf','3blck-table');
		}

		// csv export
		if($_REQUEST['operation'] == 'csv') {
			$this->_generateCsv('print-label.csv');
		}

		$report = new ASG_Report_Model();
		$titleModel = new ASG_Title_Model();
		$chapter = new ASG_Chapter_Model();
		$bod = new ASG_Bod_Model();
		$helper = new ASG_Helper();
		$geoModel = new ASG_Geolocation_Model();
		// hook for enqueueing all required javascripts in admin section
		add_action( 'admin_enqueue_scripts', array( $this , 'load_admin_scripts' ));
		// hook for enqueueing all required css files in admin section
		add_action( 'admin_print_styles', array( $this , 'load_admin_styles' ));
		add_action('wp_ajax_save_data' , array( $titleModel , 'ajax_save_data'));
     	add_action('wp_ajax_save_chapter' , array( $chapter , 'saveChapter'));
     	add_action('wp_ajax_save_bod_form' , array( $bod , 'saveBod'));
     	add_action('wp_ajax_display_president_info' , array( $member , 'getPresidentInfo'));
     	add_action('wp_ajax_get_chapter_info' , array( $chapter , 'getChapterInfo'));
     	add_action('wp_ajax_display_member_info' , array( $member , 'getMemberInfo'));
     	add_action('wp_ajax_get_bod_info' , array( $bod , 'getBodInfo'));
     	add_action('wp_ajax_search_member' , array( $member , 'searchMember'));
     	add_action('wp_ajax_search_chapter' , array( $chapter , 'searchChapter'));
     	add_action('wp_ajax_asg_report' , array( $report , 'generateReport'));
     	add_action('wp_ajax_asg_save_geo_location' , array( $geoModel , 'saveGeoLocation'));
     	add_action('wp_ajax_get_country_info' , array( $geoModel , 'getCountryById'));
     	add_action('wp_ajax_get_state_info' , array( $geoModel , 'getStateById'));
     	add_action('wp_ajax_get_city_info' , array( $geoModel , 'getCityById'));  
     	add_action('wp_ajax_asg_del_geo_info' , array( $geoModel , 'delGeoInfoById'));  
     	add_action('wp_ajax_find_state_by_country_name' , array( $geoModel , 'findStateByCoutryNameOrId'));
     	add_action('wp_ajax_find_city_by_state_name' , array( $geoModel , 'findCityByStateName'));
     	add_action('wp_ajax_asg_limit_save' , array( $helper , 'saveLimit'));
     	add_action('wp_ajax_asg_search_mailchimp_member' , array( $member , 'searchMailChimpMember'));
     	//add_action('admin_notices' , array( $this , 'showErrors'));
     	
    }
	

	/**
 	 * This method is called to include required file
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	protected function _include() { 
		// For Dashboard class
		include_once(ADMIN_DIR_PATH . DS . 'class-asg-dashboard.php');
		// For mpdf class
		include_once(PLUGIN_DIR_PATH . DS . 'external-lib' . DS  . 'mpdf' . DS .'mpdf.php');
		// For Data Grid class
		include_once(PLUGIN_DIR_PATH . DS . 'external-lib' . DS  . 'csv' . DS .'DataGrid.php');
		// For Loader class
		include_once(PLUGIN_DIR_PATH . DS .  'lib' . DS . 'class-asg-loader.php');
		// To load all classes
		$loader = new ASG_CLASS_Loader();
		foreach ($this->_libClass as $key => $classes) {
			foreach ($classes as $key => $class) { 
				// register classes with namespaces
			    $loader->addPrefix($class['class'], PLUGIN_DIR_PATH.$class['path']);
			    //$loader->addPrefix('libPath', PLUGIN_DIR_PATH.'/lib/');
			 
			    // activate the autoloader
			    $loader->register();
			 
			    // to enable searching the include path (e.g. for PEAR packages)
			    $loader->setUseIncludePath(true);
			}
		}
		

		// For Configuartion Section
		include_once(PLUGIN_DIR_PATH . DS . 'config' . DS . 'config.php');
		
	}

	

	public static function createASGDir() { 
		//filter to change the upload dir 
 		self::$asg_upload_dir = apply_filters('change_asg_dir' , self::$asg_upload_dir);
 		
 		// for media upload dir for ASG
 		if(!wp_mkdir_p( $target )) {
 			wp_mkdir_p( $target );
 		}
	}

	/**
 	 * This method is called during plugin activate
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function pluginActivationHooks() { 
		if( get_site_option( 'asg_db_version' ) != VERSION) {
			// Intialise values of table names with columns in array
			ASG_Table_Generator::initTableStructureForASG();
			if( array_key_exists( 'asg_table' , $GLOBALS['wp_filter']) ) {
	 			self::settingTable();
	 		}
	 		//$location = new ASG_LOCATION_INSERTION();
	 		ASG_LOCATION_INSERTION::ASGInsertLocation();
	 		// Inserting Default countries,states,city
	 		add_action('create_asg_dir' , array( __Class__ , 'createASGDir') , 10 , 1 );
	 		//var_dump( $GLOBALS['wp_filter']['create_asg_dir']);die; 
	 		do_action('create_asg_dir' , '');
	 		//die('he');
	 		// for table creation
			//ASG_Table_Generator::createTableForASG();
			update_option('asg_db_version' , VERSION);
 		}
	}


	

	/**
 	 * This method is called custom hook for table creation
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public static function settingTable() {
		$table = array(
						'setting' => array (
										array(
											'column'=> 'setting_name',
											'type'	=> VARCHAR128
										),
										array(
											'column'=> 'setting_value',
											'type'	=> TEXT
										),
										array(
											'column'=> 'autoload',
											'type'	=> VARCHAR128
										),
										
								)
							);
 		return apply_filters('asg_table',$table);
	}

	/**
 	 * This method is used to load admin scripts
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function load_admin_scripts() {
		wp_enqueue_media();
		
		wp_enqueue_script('common');
	    wp_enqueue_script('wp-lists');
	    wp_enqueue_script('postbox');
		//wp_enqueue_script('dashboard');
		
		// parsley.js for form validation
		wp_register_script('parsley', PLUGIN_URL .'/assets/js/parsley.js', array('jquery'), '1.1' , true );
		wp_enqueue_script('parsley');

		// jquery datepicker
		//wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-tabs');
		
		wp_enqueue_script( 'jquery-ui-datepicker' );

		// html2canvas
		/*wp_register_script('html2canvas', PLUGIN_URL .'/assets/js/html2canvas.min.js', array('jquery'), '1.0' , true );
		wp_enqueue_script('html2canvas');*/
		

		// jpdf for generating pdf
		wp_register_script('jspdf', PLUGIN_URL .'/assets/js/jspdf.debug.js', array('jquery','jquery-ui-core'), '1.0' , true );
		wp_enqueue_script('jspdf');

		//jspdf extension plugin
		wp_register_script('asg-jspdf-extension', PLUGIN_URL .'/assets/js/jspdf.plugin.autotable.js', array('jspdf'), '1.0' , true );
		wp_enqueue_script('asg-jspdf-extension');

		//jspdf implementation
		wp_register_script('asg-jspdf', PLUGIN_URL .'/assets/js/asg-jspdf.js', array('jspdf'), '1.1' , true );
		wp_enqueue_script('asg-jspdf');

		//chosen plugin implementation
		wp_register_script('asg-chosen-js', PLUGIN_URL .'/assets/js/chosen.jquery.js', array('jquery'), '1.0' , true );
		wp_enqueue_script('asg-chosen-js');

		//chosen plugin implementation
		wp_register_script('asg-csv-js', PLUGIN_URL .'/assets/js/asg-export-csv.js', array('jquery'), '1.1' , true );
		wp_enqueue_script('asg-csv-js');

		//asg report js
		wp_register_script('asg-report-js', PLUGIN_URL .'/assets/js/asg-report.js', array('jquery'), '1.0.1' , true );
		wp_enqueue_script('asg-report-js');
		
		wp_register_script('asg', PLUGIN_URL .'/assets/js/asg.js', array('jquery'), '1.1' , true );
		wp_enqueue_script('asg');	
	}

	/**
 	 * This method is used to load admin css files
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function load_admin_styles() {
		
		wp_enqueue_style('dashboard');

		// parsley.css for form validation
		wp_register_style('parsley-css', PLUGIN_URL .'/assets/css/parsley.css', array() ,'1.0' , false );
		wp_enqueue_style('parsley-css');

		// custom.css for dashboard
		wp_register_style('custom-css', PLUGIN_URL .'/assets/css/custom.css', array() ,'1.0' , false );
		wp_enqueue_style('custom-css');

		// font-awesome
		wp_register_style('font-awesome-css', PLUGIN_URL .'/assets/css/font-awesome.min.css', array() ,'1.0' , false );
		wp_enqueue_style('font-awesome-css');

		// bootstrap.css for dashboard
		wp_register_style('bootstrap-css', PLUGIN_URL .'/assets/css/bootstrap.css', array() ,'1.0' , false );
		wp_enqueue_style('bootstrap-css');

		// parsley.css for form validation
		wp_register_style('form-style-css', PLUGIN_URL .'/assets/css/form_style.css', array() ,'1.0' , false );
		wp_enqueue_style('form-style-css');

		// parsley.css for form validation
		wp_register_style('reset-css', PLUGIN_URL .'/assets/css/reset.css', array() ,'1.0' , false );
		wp_enqueue_style('reset-css');

		// parsley.css for form validation
		wp_register_style('chosen-css', PLUGIN_URL .'/assets/css/chosen.min.css', array() ,'1.0' , false );
		wp_enqueue_style('chosen-css');



		wp_register_style('jquery-ui-css', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css' , array() ,'1.0' , false );
		wp_enqueue_style('jquery-ui-css');

		// jspdf style for table
		wp_register_style('grid-responsive', PLUGIN_URL .'/assets/css/grids-responsive-min.css' , array() ,'1.0' , false );
		wp_enqueue_style('grid-responsive');

		wp_register_style('pure-min', PLUGIN_URL .'/assets/css/pure-min.css' , array() ,'1.0' , false );
		wp_enqueue_style('pure-min');
		
		wp_enqueue_style( 'jquery-ui-datepicker-style' , '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');
	}

	/**
 	 * This method is used to define constants for asg
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private function _defineConstants() {
		global $wpdb;
		!defined('TABLE_PREFIX') ? define('TABLE_PREFIX', $wpdb->prefix . 'asg_') : null;
		!defined('COUNTRY') ? define('COUNTRY', TABLE_PREFIX . 'country' ) : null;
		!defined('STATE') ? define('STATE', TABLE_PREFIX . 'state' ) : null;
		!defined('CITY') ? define('CITY', TABLE_PREFIX . 'city' ) : null;
		!defined('USER') ? define('USER', $wpdb->prefix . 'users') : null;
		!defined('TITLE1') ? define('TITLE1', TABLE_PREFIX . 'title1' ) : null;
		!defined('TITLE2') ? define('TITLE2', TABLE_PREFIX . 'title2' ) : null;
		!defined('TITLE3') ? define('TITLE3', TABLE_PREFIX . 'title3' ) : null;
		!defined('CHAPTER') ? define('CHAPTER', TABLE_PREFIX . 'chapter' ) : null;
		!defined('MEMBER') ? define('MEMBER', TABLE_PREFIX . 'member' ) : null;
		!defined('BOD') ? define('BOD', TABLE_PREFIX . 'bod' ) : null;
		!defined('SETTING') ? define('SETTING', TABLE_PREFIX . 'setting' ) : null;
		!defined('LIMIT') ? define('LIMIT', 1 ) : null;
		//!defined('CHAPTER') ? define('CHAPTER', TABLE_PREFIX . 'chapter' ) : null;
	}

	/**
 	 * This method is used to start session
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function _startSession() {
		  if( !session_id() )
        	session_start();
	}

	

	
	
}
// global variable file handler
$GLOBALS['ASG']  = new ASG(wp_upload_dir());

// upload dir for asg 
!defined('ASG_UPLOAD_DIR') ? define('ASG_UPLOAD_DIR', ASG::$asg_upload_dir ) : null;
/**
 	 * This method is used for debug purpose
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

function debug($arr) { 
		  echo '<pre>';
		  if(is_array($arr))
		  	var_dump($arr);
		  else 
		  	echo $arr;
		 echo '</pre>';
}