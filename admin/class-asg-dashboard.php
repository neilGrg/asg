<?php 
/**
 * Handles the file upload
 *
 * @package     ASG 
 * @subpackage  admin
 * @copyright   Copyright (c) 2015, 
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access Forbidden' );
}

class ASG_DASHBOARD {

	private $_adminMenus = array();
	private $_operation = '';
	public  $errors = array();
	public  $msgs = array();
	public  $adminUrl = '';
	public static $limitUpdate = false;

	  	
	function __construct() { 
		$this->adminUrl = admin_url('admin.php');
		add_filter('admin_menu', array( $this , 'asg_admin_menu' ));
		add_action('init', array( $this , 'asg_admin_hooks' ));
		add_action('admin_menu', array( $this ,  'admin_page_with_draggable_boxes'));
		$this->_asgSaveLimit();
	}


	private function _asgSaveLimit() {

		if(!isset($_REQUEST['limit']))
			return false;
		unset($_POST['limit']);
		$post = array_map( 'sanitize_text_field', $_POST );
		foreach ($post as $key => $value) {
			update_option($key , $value);

		}
		self::$limitUpdate = true;
	}


	/**
	 *
	 *	This function is used to create admin menus 
	 *	for auto login plugin
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_admin_menu() {  
		$mainMenu = __( 'ASG DASHBOARD' , TEXT_DOMAIN );
		add_menu_page( $mainMenu, $mainMenu , 'administrator','asg-admin-sub-page1', array( $this,'asg_settings'));
	    $this->asg_init_admin_menu_values();
	    // filter for adding menus in DSP admin section
	  	foreach ($this->_adminMenus as $k => $options) {
        	// Add a submenu to the custom top-level menu: add_submenu_page(parent, page_title, menu_title, capability required, file/handle, [function])
       		add_submenu_page( $options['parent'], $options['page_title'], $options['menu_title'], $options['capability'], $options['handle'], $options['func']);
       	}

       	$this->admin_page_with_draggable_boxes();
	    
	}


	public function admin_page_with_draggable_boxes()
	{   
		$my_page = add_dashboard_page( 
	        'Move it', 
	        'Move it', 
	        'add_users',
	        'moveit-page', 
	        'moveit_callback' 
	    ); 
	    add_action( "admin_print_scripts", array($this, 'move_me_around_scripts'));
	}


	

	function move_me_around_scripts() 
	{
		
	     wp_enqueue_script( 
	        'move-it', 
	        PLUGIN_URL . 'assets/js/moveit.js', // <----- get_stylesheet_directory_uri() if used in a theme
	        array( 'jquery-ui-sortable', 'jquery' ) // <---- Dependencies
	    );
	}

	public function asg_admin_hooks() { 
		/*include_once(PLUGIN_DIR_PATH . 'external-lib/mailchimp/MCAPI.class.php');
		*/
		include_once(PLUGIN_DIR_PATH . 'admin/model/report/class-asg-print-labels-model.php');
		include_once(PLUGIN_DIR_PATH . 'admin/model/report/class-asg-monthly-report-model.php');
		include_once(PLUGIN_DIR_PATH . 'admin/model/report/class-asg-print-labels-model.php');
		include_once(PLUGIN_DIR_PATH . 'admin/model/report/class-asg-membership-cards-model.php');
		include_once(PLUGIN_DIR_PATH . 'admin/views/settings/class-asg-setting.php');
		include_once(PLUGIN_DIR_PATH . 'admin/views/reports/class-asg-report.php');
		include_once(PLUGIN_DIR_PATH . 'admin/views/reports/class-asg-report-dashboard.php');
		include_once(PLUGIN_DIR_PATH . 'helper/tester.php');
		add_filter('asg_get_settings' , array( 'ASG_Setting_Model' , 'asgSetting') , 10 , 1);
	}


	/**
	 *
	 *	This function is used to initialize admin menus values
	 *	for auto login plugin
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */
	
	public function asg_init_admin_menu_values ()	{
		
		$this->_adminMenus = array(
									array(
					                    'parent'     => 'asg-admin-sub-page1',
					                    'page_title' => __('Members',TEXT_DOMAIN),
					                    'menu_title' => __('Members',TEXT_DOMAIN), 
					                    'capability' => 'administrator',
					                    'handle'     => 'asg-admin-sub-page2', 
					                    'func'       => array( $this,'asg_member_settings')
				                    ),
									array(
					                    'parent'     => 'asg-admin-sub-page1',
					                    'page_title' => __('Chapters',TEXT_DOMAIN),
					                    'menu_title' => __('Chapters',TEXT_DOMAIN), 
					                    'capability' => 'administrator',
					                    'handle'     => 'asg-admin-sub-page3', 
					                    'func'       => array( $this,'asg_chapter_settings')
				                    ),
				                    array(
					                    'parent'     => 'asg-admin-sub-page1',
					                    'page_title' => __('Titles',TEXT_DOMAIN),
					                    'menu_title' => __('Titles',TEXT_DOMAIN), 
					                    'capability' => 'administrator',
					                    'handle'     => 'asg-admin-sub-page4', 
					                    'func'       => array( $this,'asg_title_settings'),
						            ),
						            array(
					                    'parent'     => 'asg-admin-sub-page1',
					                    'page_title' => __('BOD',TEXT_DOMAIN),
					                    'menu_title' => __('BOD',TEXT_DOMAIN), 
					                    'capability' => 'administrator',
					                    'handle'     => 'asg-admin-sub-page6', 
					                    'func'       => array( $this,'asg_bod_settings')
				                    ),
				                    array(
					                    'parent'     => 'asg-admin-sub-page1',
					                    'page_title' => __('Reports',TEXT_DOMAIN),
					                    'menu_title' => __('Reports',TEXT_DOMAIN), 
					                    'capability' => 'administrator',
					                    'handle'     => 'asg-admin-sub-page7', 
					                    'func'       => array( $this,'asg_report_settings')
				                    ),
				                    array(
					                    'parent'     => 'asg-admin-sub-page1',
					                    'page_title' => __('Setting',TEXT_DOMAIN),
					                    'menu_title' => __('Setting',TEXT_DOMAIN), 
					                    'capability' => 'administrator',
					                    'handle'     => 'asg-admin-sub-page8', 
					                    'func'       => array( $this,'asg_setting_settings')
				                    )
				                   
		           		);
		
		$this->_adminMenus = apply_filters('asg_add_menu' , $this->_adminMenus);
		
		
	}


	/**
	 *  This is magic method in PHP,in which if
	 *  any method not found inside this class will 
	 *  automatically created;
	 * 
	**/

	private function _add_subtitles($options) {  
		add_submenu_page( $options['parent'], $options['page_title'], $options['menu_title'], $options['capability'], $options['handle'], $options['func']);
	}

	/**
	 *
	 *	This function is used for passing data to view
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_settings() { 
		$this->_asgDashBoard();
	}


	/**
	 *
	 *	This function is used for getting all chapters
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _operationForChapter() { 
		$this->_asgSaveLimit();
		$this->_operation = isset($_REQUEST['operation']) ? $_REQUEST['operation'] : $_REQUEST['action'];
		ASG_Chapter_Model::_initDefaultConfig();
		$chapterModel  = new ASG_Chapter_Model();
		if(isset($this->_operation)):
		    switch ($this->_operation) {
				case 'add':
					$chapterModel->create();
					break;
				case 'edit':
					$chapterModel->edit();
					break;
				case 'delete':
					$chapterModel->delete();
					break;
				case 'bulk_delete':
					$chapterModel->bulkDelete();
					break;
				default:
					break;
			}
		endif;
		$datas['success'] = $chapterModel->success;
		$datas['errors'] = $chapterModel->errors;
		$datas['chapters'] = ASG_Chapter_Model::getAllChapterWithPresidents();
		$datas['pages'] = $chapterModel->getNumPages();
		return $datas;
	}


	/**
	 *
	 *	This function is used for getting all bods
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _operationForBods() {

		$this->_operation = isset($_REQUEST['operation']) ? $_REQUEST['operation'] : $_REQUEST['action'];
		$bodModel  = new ASG_Bod_Model();
		//var_dump($this->_operation);die;
		ASG_Bod_Model::_initDefaultConfig();
		if(isset($this->_operation)):
			switch ($this->_operation) {
				case 'add':
					$bodModel->create();
					break;
				case 'edit':
					$bodModel->edit();
					break;
				case 'delete':
					$bodModel->delete();
					break;
				case 'bulk_delete':
					$bodModel->bulkDelete();
					break;
				default:
					break;
			}
		endif;
		$bods['success'] = $bodModel->success;
		$bods['bod_list'] = $bodModel->getBodLists();
		$bods['pages'] = $bodModel->getNumPages();
		return $bods;
	}


	/**
	 *
	 *	This function is used for getting all bods
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _operationForMembers() {
		
		$this->_operation = isset($_REQUEST['save_member']) ? $_REQUEST['save_member'] : null;
		$memberModel  = new ASG_Member_Model();
		$datas = array();
		//ASG_Member_Model::_initDefaultConfig();
		if(isset($this->_operation)):
			switch ($this->_operation) {
				case 'add':
					$memberModel->create();
					break;
				case 'edit':
					$memberModel->edit();
					break;
				default:
					break;
			}
		endif;
		$operation = isset($_REQUEST['operation']) ? $_REQUEST['operation'] : $_REQUEST['action'];
		if($operation == 'delete')
			$memberModel->delete();
		else if ($operation == 'bulk_delete')
			$memberModel->bulkDelete();
		$datas['success'] = $memberModel->success;
		$datas['errors'] = $memberModel->errors;
		$datas['pages'] = $memberModel->getNumPages();
		$datas['members'] = $memberModel->getMemberList();
		return $datas;
	}

	/**
	 *
	 *	This function is used for passing data to view
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_chapter_settings () {
		$data = $this->_operationForChapter();
		$member = new ASG_Member_Model();
		$presidents = $member->getAllPresidents();
		$GLOBALS['data'] = array  (
						'chapters' => $data['chapters'],
						'errors' => $data['errors'],
						'success' => $data['success'],
						'presidents' => (!empty($presidents) && count($presidents) > 0) ? $presidents : array(),
						'states' => ASG_Geolocation_Model::getsortNameStates(),
						'url' => ADMIN_URL . '?page=asg-admin-sub-page3',
						'pages' => $data['pages']
					);
		$this->view('chapters/class-asg-chapter');
	}

	/**
	 *
	 *	This function is used for passing data to view
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _getTitles() {
		$titles = array( 
							'tab1' => 'Title 1',
							'tab2' => 'Title 2',
							'tab3' => 'Title 3',
					);
		return $titles;
	}

	/**
	 *
	 *	This function is used for passing data to view
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_title_settings () {
		$titles = $this->_getTitles();
		$GLOBALS['data'] = array  (
						'titles' => $titles

					);
		$this->view('titles/class-asg-title');
	}

	

	
	/**
	 *
	 *	This function is used for bod section
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_bod_settings () {
		$member = new ASG_Member_Model();
		$chapter = new ASG_Chapter_Model();
		$chapterCol = array('id','chapter');
		$chapters = $this->formatArrayForSelect($chapter->getAllChapterInfo());
		$members = $this->formatArrayForSelect($member->getAllMembers());
		$bods = $this->_operationForBods();
		$GLOBALS['data'] = array  (
			 			'bods' => $bods['bod_list'],
						'members' => $members,
						'chapters' => $chapters,
						'success' => $bods['success'],
						'url' =>  ADMIN_URL . '?page=asg-admin-sub-page6',
						'pages' => $bods['pages']

					);
		$this->view('bods/class-asg-bod');
	}

	/**
	 *
	 *	This method is used for member section
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_member_settings () {
		$data = $this->_operationForMembers();
		$GLOBALS['data'] = array  (
						'url' => ADMIN_URL . '?page=asg-admin-sub-page2',
						'errors'=> $data['errors'],
						'members'=> $data['members'],
						'success'=> $data['success'],
						'pages' => $data['pages']
					);
		$this->view('member/class-asg-members',$data);
	}


	/**
	 *
	 *	This method is used for bod section
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _generateReports() {  
		if(isset($_REQUEST['report_type']) && empty($_REQUEST['report_type']))
			return false;
		$callModelReportType = $_REQUEST['report_type'];
		$reportModel = new ASG_Report_Model();
		switch ($callModelReportType) {
			case 'monthly_report':
				 return $reportModel->generateMonthlyReport();
				break;
			case 'print_label':
				return $reportModel->generatePrintLabelReport();
				break;
			case 'mailchimp_sync':
				return $reportModel->syncMemberIntoMailChimp();
				break;
			case 'membership_card':
				return $reportModel->generateMemberShipCard();
				break;
			default:
				# code...
				break;
		}
		
	}

	/**
	 *
	 *	This method is used for bod section
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _getReports() { 
		$data = $this->_generateReports();
		$report = isset($_GET['report']) && !empty($_GET['report'])  ?  $_GET['report'] : $_REQUEST['report_type'] ;
		if(!has_filter($report) || empty($report)) {
			apply_filters('report_dashboard', '');
			return false;
		}

		// using default filter to display
		apply_filters($report , $data);
		
	}

	
	/**
	 *
	 *	This method is used as report controller 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_report_settings () {
		?>
		<div class="wrap asg-content" >
			<?php $this->_getReports(); ?>
		</div>
		<?php
		//$this->view('reports/class-asg-report',$data);
	}
	

	/**
	 *
	 *	This function is used  to get setting for asg
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _settingValues () {
		$settingModel = new ASG_Setting_Model();
		$updateSetting = isset($_REQUEST['update_setting']) ? $_REQUEST['update_setting'] : '';
		switch ($updateSetting) {
			case 'general':
				$settingModel->includeSetting()	;
				break;
			case 'geography':
				break;
			case 'mailchimp':
				$settingModel->includeSetting()	;
				break;
			default:
				# code...
				break;
		}
		$datas['errors'] = $settingModel->errors;
		$datas['settings'] = apply_filters('asg_format_setting_array',$settingModel->settingValues);
		
		return $datas;
	}


	/**
	 *
	 *	This function is used for bod section
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function asg_setting_settings () { 
		$setting = $this->_settingValues();
		$data = array  (
			 			'settings' => $setting['settings'],
						'url' =>  ADMIN_URL . '?page=asg-admin-sub-page8',
						'pages' => $setting['pages'],
					);
		if( array_key_exists( 'asg_setting_tab_display' , $GLOBALS['wp_filter']) ) {
			apply_filters('asg_setting_tab_display' , $data);
		}
	}

	/**
	 *
	 *	This function is used to display the dashboard
	 *	for ASG
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _asgDashBoard(){
		
		$data = array(
					'adminUrl' => $this->adminUrl,
					'chapterIconUrl' => apply_filters('asg_get_settings' , 'chapter_icon'),
					'bodIconUrl' => apply_filters('asg_get_settings' , 'bod_icon'),
					'memberIconUrl' => apply_filters('asg_get_settings' , 'member_icon'),
					'reportIconUrl' => apply_filters('asg_get_settings' , 'report_icon')
				); 
		$this->view('dashboard',$data);
	}




	/**
	 *
	 *	This method is used for formatting array
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function formatArrayForSelect ($data) {
		$customArr = array();
		if(!is_array($data))
			return false;
		foreach ($data as $key => $value) {
			$v = array_pop($value);
			$k = array_pop($value);
			$customArr[$k] = $v;
			
			
		}
		return $customArr;
	}


	/**
	 *
	 *	This is generic method to include file in view folder
	 *	for register form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function view ($filename,$data = array()) {	
		extract($data);
	?>
		<div class="wrap asg-content" >
			<?php include_once( ADMIN_DIR_PATH . '/views/' . $filename .'.php'); ?>
		</div>
		<?php
	}

	
}

// global variable file handler
$GLOBALS['asgDashboard']  = new ASG_DASHBOARD();