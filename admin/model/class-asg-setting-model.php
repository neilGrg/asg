<?php

 class ASG_Setting_Model {

 	public $settings =  array();
  public $settingValues =  array();
 	public $errors =  array();
  private $_insertData = array();
  private $_updateData = array();
 	private $_settingNames = array();

 	function __construct() { 
 		$this->_setTableName();
    $this->_setSettingValues();
    $this->_setExistSettingNames();
 		
 	}

 	private function _setTableName() { 
 		ASG_DAO_Access::$tableName = SETTING;
  }

  /**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

  public static function asgSetting($name) { 
    ASG_DAO_Access::$tableName = SETTING;
    $columns = array( 'setting_value');
    $where = array('setting_name' => $name);
    $result = ASG_DAO_Access::selectRows( $where , ARRAY_A , $columns );
    return !empty($result) && is_array($result) ? array_pop($result)['setting_value'] : ''; 
  }

  /**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

  private function _setSettingValues() {
      $columns = array( 'setting_name','setting_value');
      $this->settingValues = ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
  }

  /**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

  private function _setExistSettingNames() {
      $columns = array('setting_name');
      $where = array();
      $names = ASG_DAO_Access::selectRows( $where , ARRAY_A , $columns );
      foreach ($names as $key => $value) {
          $this->_settingNames[] = $value['setting_name'];
         
      }
  }

 	
 	
	/**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

  public function includeSetting() {
    $this->settings = isset($_REQUEST['setting']) ? $_REQUEST['setting'] : null;
    $this->setValues();
    $this->create();
    $this->edit();
    $this->_setSettingValues();
  }

  public function setValues() { 
    foreach ($this->settings as $key => $value) {
      if(in_array($key,$this->_settingNames)) {
        $this->_updateData["$key"] = $value;
        continue;
      }
      $this->_insertData["$key"] = $value;
    }

  }

 	public function create() { 
    if(count($this->_insertData) < 1)
      return false;
    foreach ($this->_insertData as $key => $value) {
      $data['setting_value'] = $value;
      $data['setting_name'] = $key;
      ASG_DAO_Access::insertRow($data);
    }
    $message = __( 'Updated Successfully', TEXT_DOMAIN );
    update_option('asg-crud-message' , $message);
 	}
    
  public function edit() {
    if(count($this->_updateData) < 1)
      return false;
    foreach ($this->_updateData as $key => $value) {
      $data['setting_value'] = $value;
      $name['setting_name'] = $key;
      ASG_DAO_Access::updateRow($data,$name);
    }
    $message = __( 'Updated Successfully', TEXT_DOMAIN );
    update_option('asg-crud-message' , $message);
  }

  public function delete() {
   	ASG_DAO_Access::deleteRow($this->_id);
    $message = __( 'Deleted Successfully', TEXT_DOMAIN );
    update_option('asg-crud-message' , $message);
  }

    
 }

