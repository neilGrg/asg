<?php
if ( ! defined( 'ABSPATH' ) ) {
	die('Cannot Accessed  directly this file');
}
class ASG_Chapter_Model {

	protected $_datas =  array();
	protected $_id =  array();
	protected $_type =  array();
	protected $_operation = '';
	protected $_post = '';
	private $_chapterData = array();
	public $totalChapters = '';
	public $success = false;

	/**
	 * Rules for validation
	 *
	 * @var $_rules
	 */

	private $_rules =  array();

	/**
	 * errors after validation
	 *
	 * @var $errors
	 */
	public $errors = array();

	/**
	 * Table name for this model
	 *
	 * @var $_tableName
	 */
	private static $_tableName = CHAPTER;
	/**
	 * limit value for pagination
	 *
	 * @var $_limit
	 */
	public static $limit = '';
	
	private static $_defaultConfig = array();
	public $chapters = array();

	function __construct() {
			//ASG_DAO_Access::$tableName = $this->_tableName;
		$this->_id['id'] = isset($_REQUEST['chapter_id']) ? sanitize_text_field($_REQUEST['chapter_id']) : '';
		$this->_post = $posts;
		$this->setTableName();
		self::setLimit();
		$this->_setRulesForValidation();
	}

	/**
	* This method is used to set table name 
	* array from post data
	* @param $post
	* @return Array
	* @since 1.0
	* 
	**/

	public function setTableName() {

		ASG_DAO_Access::$tableName = self::$_tableName;	
	}

	/**
	* This method is used to remove  word/ from
	*  string word/word
	* @param null
	* @return null
	* @since 1.0
	* 
	**/

	private function _removeSlashString() {
		$chapter = $this->_chapterData['chapter'];
		$this->_chapterData['chapter'] = substr(
								  $chapter,
								 stripos($chapter , '/') + 1,
								strlen($chapter )
							);
		
	}

	/**
    * This method is used to set limit value from setting
    * for pagination
    * @param null
    * @return null
    * @since 1.0
    * 
    **/


	public static function setLimit() {
		self::$limit = get_option('asg-chapter-limit');
 		self::$limit = is_numeric(self::$limit) && !empty(self::$limit) ? 
 						self::$limit : 
 						10;
 	}

	public static function _initDefaultConfig() { 
		self::$_defaultConfig = array(
			'operation' => __('Operation', TEXT_DOMAIN),
			'edit' => __('Edit', TEXT_DOMAIN),
			'delete'=> __('Delete', TEXT_DOMAIN),
			'save'=> __('Save', TEXT_DOMAIN),
			'title_name'=> __('Title Name', TEXT_DOMAIN)
			);
		ASG_DAO_Access::$tableName = self::$_tableName;
	}


	public function create() {
		$message = __( 'Created Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->_removeSlashString();
		$this->_chapterData['chapter'] = $this->_chapterData['state'] . '/' . $this->_chapterData['chapter'];
		return ASG_DAO_Access::insertRow($this->_chapterData);
	}

	public function edit() { 
		$message = __( 'Updated Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->_removeSlashString();
		$this->_chapterData['chapter'] = $this->_chapterData['state'] . '/' . $this->_chapterData['chapter'];
		return ASG_DAO_Access::updateRow( $this->_chapterData , $this->_id);
	}

	public function delete() {
		ASG_DAO_Access::$tableName = self::$_tableName;	
		ASG_DAO_Access::deleteRow($this->_id);
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->success = true;
		return;
	}

	/**
	 * This method is used for delete chapters in bulk
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function bulkDelete() { 
		$this->setTableName();
		/**
		 * do sthg before member info deleted
		 **/
		do_action('asg_chapter_bulk_deleted');
		$ids = isset($_REQUEST['delete_chapter']) ? $_REQUEST['delete_chapter'] : array();
		if(count($ids) == 0) 
			return false;
		foreach ($ids as $id) {
			$whr['id'] = $id;
			ASG_DAO_Access::deleteRow($whr);
		}
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->success = true;
	}

	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param $post
	* @return Array
	* @since 1.0
	* 
	**/

	public static function initializeAllHooks() { 
		add_action('wp_ajax_save_data' , array( &$this , 'ajax_save_data'));
	}


	/**
	* This method is used to return all chapters into 
	* array from post data
	* @param $post
	* @return Array
	* @since 1.0
	* 
	**/

	public static function getAllChapters() { 
		ASG_DAO_Access::$tableName = self::$_tableName; 
		$columns = array( 'id','chapter');
		return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
	}

	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param $post
	* @return Array
	* @since 1.0
	* 
	**/

	public function getAllChapterInfo($cols = array()) { 
		ASG_DAO_Access::$tableName = self::$_tableName;
		$columns = (!empty($cols) && count($cols) > 0 ) ? $cols : array( 'id','chapter');
		return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
	}

	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param $filter
	* @return Array
	* @since 1.0
	* 
	**/

	public static function getAllChapterWithPresidents( $filter = array()) { 
		$pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
		ASG_DAO_Access::$limit = self::$limit;
 		ASG_DAO_Access::$offset = ( $pagenum - 1 ) * self::$limit;
 		$columns = array( 
			t1 => array( 'id','chapter','member_id'),
			t2 => array('first_name' , 'middle_name' , 'last_name')
			);
		$tableName = array( 
			'table1' => CHAPTER,
			'table2' => MEMBER
			);
		$on = 't1.member_id = t2.id';
		return !empty($filter) ? 
		ASG_DAO_Access::join($tableName, $on , $columns , 'ORDER BY t1.id' , $filter) : 
		ASG_DAO_Access::join($tableName, $on , $columns , 'ORDER BY t1.id');

	}

	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param $id (chapter id)
	* @return Array
	* @since 1.0
	* 
	**/

	public function getChapterById($id) {
		$this->setTableName();
		$where = array(
			'id' => $id
			);
		return array_pop(ASG_DAO_Access::selectRows( $where , ARRAY_A ));
	}

	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param $id (chapter id)
	* @return Array
	* @since 1.0
	* 
	**/

	public  static function getChapterNameById($id) {
		ASG_DAO_Access::$tableName = CHAPTER;
		$columns = array('chapter');
		$where = array(
			'id' => $id
			);
		$result = ASG_DAO_Access::selectRows( $where , ARRAY_A  , $columns);
		$result = array_pop($result);
		return $result;
	}


	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param $id (chapter id)
	* @return Array
	* @since 1.0
	* 
	**/

	public function getChapterByIdWithRelatedInfo($id) {
		$columns = array( 
			t1 => array('first_name'),
			t2 =>array( 'chapter', 'email', 'tel', 'ext', 'address', 'city', 'state', 'zip','member_id')
			);
		$tableName = array( 
			'table1' => MEMBER,
			'table2' => CHAPTER
			);
		$on = 't1.id = t2.member_id';
		$where = array('id' => $id);
		$result = ASG_DAO_Access::join($tableName, $on , $columns , $where );
		$result = array_pop($result);
		return $result;
	}


	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param $id (chapter id)
	* @return Array
	* @since 1.0
	* 
	**/

	public static function getChapterByIdWithPresidentInfo($id) {
		$columns = array( 
			t1 => array( 'chapter', 'email', 'tel', 'ext', 'address', 'city', 'state', 'zip','member_id'),
			t2 => array('first_name','middle_name','last_name')
			);
		$tableName = array( 
			'table1' => CHAPTER,
			'table2' => MEMBER
			);
		$on = 't1.member_id = t2.id';
		$where = array('id' => $id);
		$results = ASG_DAO_Access::join($tableName, $on ,  $columns , ' ORDER BY t1.member_id' , $where );
		return array_pop($results);
	}


	/**
	 * This method is used for setting 
	 * rules for validation of all fields
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function _setRulesForValidation() { 

		if(!empty($this->_rules) && count($this->_rules) > 0 )
			return true;
		$this->_rules = array(
			'first_name' => array(
				'rule' => 'required|words',
				'message' => 'First Name is required|First Name should be words '
				),
			'last_name' => array(
				'rule' => 'required|words',
				'message' => 'Last Name is required|Last Name should be words '
				),
			'chapter' => array(
				'rule' => 'required',
				'message' => 'Chapter is required'
				),
			'zip' => array(
				'rule' => 'required|number',
				'message' => 'Zipcode is required|Zipcode should be number'
				),
			'tel' => array(
				'rule' => 'required|number',
				'message' => 'Telephone is required|Telephone should be number'
				),
			'address' => array(
				'rule' => 'required',
				'message' => 'Address is required'
				),
			'city' => array(
				'rule' => 'required',
				'message' => 'City is required'
				),
			'state' => array(
				'rule' => 'required',
				'message' => 'State is required'
				),
			);

	}

	
	/**
	* This method is used to return only chapter data into 
	* array for ajax call
	* @param $id (chapter id)
	* @return Array
	* @since 1.0
	* 
	**/

	public function searchChapter() { 
		$chapter['chapter'] = isset($_POST['chapter']) ? sanitize_text_field($_POST['chapter']) : '';
		$rows = ASG_Chapter_Model::getAllChapterWithPresidents($chapter);
		$return = array(
			'rows' => !empty($rows) ?  $rows : '',
			'error' => empty($rows) ?  __('No Result Found' , TEXT_DOMAIN) : '',
			'url' => ADMIN_URL.'?page=asg-admin-sub-page3'
			);
		echo json_encode($return);
		wp_die();
	}



	/**
	* This method is used to return only chapter data into 
	* array from post data
	* @param null
	* @return null
	* @since 1.0
	**/

	private function _setAllChapterData() { 

		foreach ($this->_post as $key => $value) {

		}

	}


	/**
	* This method is used to simply output errors
	* after from validation
	* @param null
	* @return string
	* @since 1.0
	* 
	**/

	public function displayErrors() { 
		$html = '<div class="error notice">';
		foreach ($this->_errors as $error) {
			$html .= '<span>'. $error . '</span>';
		}
		$html .= '</div>';
		return $html;
	}

	/**
	 *
	 * This method is used to get chapter
	 * by using id (ajax method)
	 * @param null
	 * @return Array
	 * @since 1.0
	 * 
	 **/
	public function saveChapter() { 
		$return = array('errors' => '','success' => '');
		$postDatas = isset($_POST['form_value']) ?  $_POST['form_value'] : '';
	 	$postDatas = apply_filters('asg_value_formatter' , $postDatas);
	 	$this->_chapterData = isset($postDatas['chapter']) ?  $postDatas['chapter'] : '';
		$this->_id['id'] = isset($postDatas['chapter_id']) ?  array_values($postDatas['chapter_id'])[0] : '';
		$operation = count($postDatas['operation']) > 0 ? array_values($postDatas['operation'])[0] : '';
		$postDatas = array_merge($postDatas['chapter'],$postDatas['member']);
		$formValidator = new ASG_Form_Validator($this->_rules);

			// if validate() return false after vaidation
		if(!$formValidator->validate($postDatas)) {
			$return['errors'] = $formValidator->getJSONOrArray(true);
			echo json_encode($return);
			wp_die();
		}
		$this->setTableName();
		$return['success'] = $operation != 'edit' ? $this->create() : $this->edit();
		echo json_encode($return);
	 	//$chapterInfo  = $this->getChapterByIdWithPresidentInfo($Cid);
		wp_die();

	}

	/**
 	 * This method returns total number of the chapter 
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function countChapters() {
 		$this->setTableName();
 		return ASG_DAO_Access::count();
 	} 


	/**
 	 * This method returns total number of pages
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function getNumPages() {
		$total = $this->countChapters();
		return ceil( $total /  self::$limit );
	}

	/**
	 *
	 * This method is used to get chapter
	 * by using id (ajax method)
	 * @param null
	 * @return Array
	 * @since 1.0
	 * 
	 **/
	 public function getChapterInfo() { 
	 	$Cid = isset($_REQUEST['chapterId']) ? sanitize_text_field($_REQUEST['chapterId']) : '';
	 	$chapterInfo  = $this->getChapterByIdWithPresidentInfo($Cid);
	 	echo json_encode($chapterInfo);
	 	wp_die();

	 }
}

