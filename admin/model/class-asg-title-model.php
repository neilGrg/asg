<?php

 class ASG_Title_Model {

 	protected $_datas =  array();
 	protected $_id =  array();
 	protected $_type =  array();
 	protected $_operation = '';
 	protected $_post = '';
 	private $_rules =  array(
				    'title' => 'required',
				);
 	private $_required = array('title');
 	private $_errors = array();
 	protected $_validator = '';
	private $_defaultConfig = array();
	private $_currentTableName = '';
 	public $titles = array(
 						'TREASURER',
 						'NEWSLETTER EDITOR',
 						'SECRETARY',
 						'2ND VICE PRESIDENT',
 						'1ST VICE PRESIDENT',
 						'PRESIDENT'
 					);

 	function __construct() {
 		$this->_initDefaultConfig();
 		$this->_operation = !empty($_REQUEST['operation']) ? $_REQUEST['operation'] : $_REQUEST['action'];
 		$this->_id['id'] = isset($_REQUEST['title_id']) ? $_REQUEST['title_id'] : '';
 		if($this->_operation == 'delete' && $_REQUEST['page'] == 'asg-admin-sub-page4') {
 			$this->delete();
 		} else if($this->_operation == 'bulk_delete' && $_REQUEST['page'] == 'asg-admin-sub-page4') {
 			$this->bulkDelete();
 		}
  	}

 	/**
 	 * This method is used to display title1 lists
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function display($obj) { 
		$url = '?page=asg-admin-sub-page4&tab=' . $obj->config['tab'];
		$class = get_class($obj);
		$fieldName = $obj->columns['title'];
		$this->titles = $obj->getAllTitles();
		update_option('action_table' , $obj->getTableName());
		echo apply_filters('asg_crud_message' , ''); 
		?>

			<div id="add_title"><a href="<?php echo ADMIN_URL . $url . "#TB_inline?width=600&height=50&inlineId=my-content-id"; ?> " class="page-title-action thickbox" onclick="var f = new FormHandler(); f.setActionType(this);" data-operation = "add"><?php echo $obj->config['add']; ?></a></div>
			<form method="POST" id="bulk-action" >
	    	<div class="alignleft actions bulkactions">
				<label for="bulk-action-selector-top" class="screen-reader-text"><?php echo __('Select bulk action' , TEXT_DOMAIN); ?></label>
				<select name="action" id="bulk-action-selector-top">
					<option value="-1"><?php echo __('Bulk Actions' , TEXT_DOMAIN); ?></option>
					<option value="bulk_delete"><?php echo __('Delete' , TEXT_DOMAIN); ?></option>
				</select>
				<input type="button" id="doaction" class="button action" value="Apply">
			</div>
			<table class="wp-list-table widefat fixed striped pages" >
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo $obj->config['title']; ?></th>
					<th ><?php echo strtoupper($this->_defaultConfig['edit']) ?></th>
					<th ><?php echo strtoupper($this->_defaultConfig['delete']) ?></th>
				</thead>
				<tbody>
					<?php foreach ($this->titles as $key => $title) { ?>
						<tr>
							<th scope="row" class="check-column">		
								<label class="screen-reader-text" for="cb-select-2"><?php echo __('Select Member' , TEXT_DOMAIN); ?></label>
								<input id="cb-select-2" type="checkbox" name="delete_title[]" value="<?php echo $title['id']; ?>">
							</th>
							<td><?php echo $title[$fieldName]; ?></td>
							<td><a href="<?php echo ADMIN_URL .  $url . "#TB_inline?width=600&height=50&inlineId=my-content-id"; ?> " class="page-title-action thickbox" onclick="var f = new FormHandler(); f.setActionType(this);" data-operation = "edit" data-id="<?php echo $title['id'];?>" data-value="<?php echo $title[$fieldName];?>"><i class="fa fa-pencil-square-o"></i></a></td>
							<td><a href="<?php echo ADMIN_URL  .  $url . "&operation=delete&title_id=". $title['id']  ?>" onclick ="return confirm('Are you sure you want to Delete?')"><i class="fa fa-trash-o"></i></a></td>
						</tr>
					<?php } ?>
				</tbody>
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo $obj->config['title']; ?></th>
					<th ><?php echo strtoupper($this->_defaultConfig['edit']) ?></th>
					<th ><?php echo strtoupper($this->_defaultConfig['delete']) ?></th>
				</thead>
			</table>

			<!-- Add Section -->
			<?php add_thickbox(); ?>

			<div id="my-content-id" style="display:none;" data-parsley-validate >
				<form action="" method="post" onsubmit="event.preventDefault();">
					<div class="form-control">
						<label><?php echo $this->_defaultConfig['title_name'];?></label>
						<input type="text" name="<?php echo $fieldName; ?>" value=""  id="title" placeholder="<?php echo __('Enter Title',TEXT_DOMAIN); ?>" required />
						<input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo wp_create_nonce( 'asg_action_nonce'); ?>" />
						<input type="hidden" name="operation" value="" id="operation" />
						<input type="hidden" name="tblName" value="<?php echo $obj->getTableName(); ?>" id="tblName" />
						<input type="hidden" name="title_id" value="" id="titleId" />
						<input type="submit" name="save" value="<?php echo $this->_defaultConfig['save'] ?>" id="titleSave" onclick="f.saveTitle(this);"/>
					</div>
				</form>
			</div>

		<?php
		
	}

	private function _initDefaultConfig() {
		$this->_defaultConfig = array(
						'operation' => __('Operation', TEXT_DOMAIN),
						'edit' => __('Edit', TEXT_DOMAIN),
						'delete'=> __('Delete', TEXT_DOMAIN),
						'save'=> __('Save', TEXT_DOMAIN),
						'title_name'=> __('Title Name', TEXT_DOMAIN)
					);
	}

	/**
    * This method return all titles
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

	public function getTitles() { 
		$title1 = new ASG_Title1_Model();
		$title1Title = $title1->getAllTitles();
		$title2 = new ASG_Title2_Model();
		$title2Title = $title2->getAllTitles();
		return array_merge(
							$title1Title,
							$title2Title
						) ;
	}

	/**
    * This method is used to create title 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

	public function create() { 
		$message = __( 'Created Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		//$this->success = true;
		return ASG_DAO_Access::insertRow($this->_datas);
	}


	/**
    * This method is used to edit title 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

	public function edit() { 
		/*$message = __( 'Edited Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);*/
		return ASG_DAO_Access::updateRow($this->_datas,$this->_id);
	}

	/**
    * This method is used to delete title 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

	public function delete() {
		$tblName = get_option('action_table');
		if(empty($tblName)) {
			$message = __( 'Table name is empty', TEXT_DOMAIN );
			update_option('asg-crud-message' , $message);
			return false;
		}
		ASG_DAO_Access::$tableName = $tblName;
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		ASG_DAO_Access::deleteRow($this->_id);
	}



	/**
    * This method is used to delete title in bulk
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

	public function bulkDelete() {
		if(empty($_REQUEST['tblName'])) {
			$message = __( 'Table name is empty', TEXT_DOMAIN );
			update_option('asg-crud-message' , $message);
			return false;
		}
		ASG_DAO_Access::$tableName = $_REQUEST['tblName'];
		/**
		 * do sthg before member info deleted
		 **/
		do_action('asg_chapter_bulk_deleted');
		$ids = isset($_REQUEST['delete_title']) ? $_REQUEST['delete_title'] : array();
		if(count($ids) == 0) 
			return false;
		foreach ($ids as $id) {
			$whr['id'] = $id;
			ASG_DAO_Access::deleteRow($whr);
		}
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->success = true;
		
	}
	

     public static function initializeAllHooks() { 
     	add_action('wp_ajax_save_data' , array( &$this , 'ajax_save_data'));
     }

     /**
     * To simply output array
     *
     **/
     public function displayErrors() { 
     	$html = '<div class="error notice">';
     	foreach ($this->_errors as $error) {
     			$html .= '<span>'. $error . '</span>';
     	}
     	$html .= '</div>';
     	return $html;
     }


     /**
     *
     * To save title using ajax
     **/
     public function ajax_save_data() { 
     	$reqKey = isset($_REQUEST['reqKey']) ? sanitize_text_field($_REQUEST['reqKey']) : '';
     	$this->_datas[$reqKey] = isset($_REQUEST['title']) ? sanitize_text_field($_REQUEST['title']) : '';
     	//$this->_datas['type'] = isset($_REQUEST['type']) ? sanitize_text_field($_REQUEST['type']) : '';
     	$this->_operation = isset($_REQUEST['operation']) ? sanitize_text_field($_REQUEST['operation']) : '';
     	$this->_id['id'] = isset($_REQUEST['id']) ? sanitize_text_field($_REQUEST['id']) : '';
     	$this->_validator = new ASG_Form_Validator($this->_rules, $this->_required, $this->_sanatize);
     	ASG_DAO_Access::$tableName = isset($_REQUEST['tblName']) ? sanitize_text_field($_REQUEST['tblName']) : '';;
     	$return = array();
     	ASG_Form_Validator::$errMessages = array(
 							'title' => 'Title field is not empty.'
 						);
     	$id  = '';
     	if($this->_validator->validate($_POST)){
	     	$return['id'] = $this->_operation == 'add' ? $this->create() : $this->edit();
			$return['errors'] = '';
		} else {   
			$return['errors'] = __('Title field is required' , TEXT_DOMAIN ); 
		}
		echo json_encode($return);
		exit();
 		
     }
 }

