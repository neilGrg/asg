<?php

 class ASG_Member_Model {

 	protected $_id =  array();
 	/**
     * table name for model
     *
     * @var $_tableName
     * 
     */
 	private $_tableName = MEMBER;

 	/**
     * Rules for validation
     *
     * @var $_rules
     */
 	private  $_rules = array();

 	/**
     * President Id
     *
     * @var $_rules
     */
 	private  $_pId = array();
 	
 	public $config	= array();
 	/**
     * Rules for validation
     *
     * @var $_pdfLayout
     */
 	public $pdfLayout = '';
 	public $errors = array();
 	public $success = false;

 	function __construct() { 

 		$this->_initDefaultConfig();
 		$this->_id['id'] = isset($_REQUEST['id']) ? sanitize_text_field($_REQUEST['id']) : '';
 		
 		//ASG_DAO_Access::$tableName = $this->_$tableName;
 		$this->_setLimit();
 		$this->_setRulesForValidation();
 		add_filter('asg_membership_pdf_layout' , array($this , 'membershipPdfLayout'), 10 , 1);
 		add_action('asg_mail_user', array($this , 'sendMailToNewUser'), 10 , 1);
 		add_filter('generate_pdf' ,  array($this , 'generatePdf'), 10 , 1);
 		add_filter('asg_get_all_member_info' , array($this , 'asgGetMemberInfo') , 10 , 1);
 		add_filter('asg_member_report' , array($this , 'reportForMembers') , 10 , 1);
 		add_filter('asg_individual_member_report' , array($this , 'reportForIndividualMembers') , 10 , 1);
 	}

 	function getTableName() {
 		return $this->tableName;
 	}

 	function setTableName() {
 		ASG_DAO_Access::$tableName = $this->_tableName;
 	}

 	function membershipPdfLayout($members = array()) { 
 		if(count($members) < 1)
 			return false;
 		$pdfConfigs = array(
			 			'site_name' => __('American Sewing Guild',TEXT_DOMAIN),
			 			'thank_you' => array(
						 				'thank_you_msg' => __('Thank you for joining the American Sewing Guild', TEXT_DOMAIN),
						 				'extra' => __('You may be required to present this card at chapter events', TEXT_DOMAIN),
						 				),
			 			'office_info' => array(
						 				'membership_card' =>__('MEMBERSHIP CARD',TEXT_DOMAIN),
						 				'location' =>__('9660 Hillcroft St. Suite 510',TEXT_DOMAIN),
						 				'address' =>__('Houstan,TX 77096',TEXT_DOMAIN),
						 				'tel' =>__('(713) 729-3000 Fax (713) 721-9230',TEXT_DOMAIN),
						 				'url' =>__('www.asg.org',TEXT_DOMAIN),
					 				),
			 			'president' => __('PRESIDENT',TEXT_DOMAIN),
			 			'signature' =>  __('Member Signature',TEXT_DOMAIN)
					);
 		foreach ($members as $key => $member) { /*echo '<pre>';var_dump($member);die;*/
 			$this->_pId = $member['member_id'];
 			$member['name'] = ASG_Helper::getFullName($member);
 			$expireDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($member['join_date'])) . " + 365 day"));
 			$president = $this->getPresidentInfo();
 			$this->pdfLayout .= '<table style="width:100%">
 									<tr>
 										<td></td>
 										<td><div>' . $member['status'] . '</div>
	 									</td>
	 									<td></td>
	 									<td></td>
 									</tr>
 									<tr>
 										<td><div>president- ' . $president['name'] . '</div></td>
 										<td><div> ' . $president['cell_phone'] . ' </div></td>
 									</tr>
 									<tr>
 										<td></td>
 										<td></td>
 										<td><div> ' . $member['name'] . ' </div></td>
 										<td><div> ' . $expireDate . '  </div></td>
 									</tr>
 									<tr>
 										<td><div> ' . $member['name'] . ' </div></td>
 										<td></td><td><div> ' . $member['chapter'] . ' </div></td>
 										<td><div> ' . $member['id'] . '</div></td>
 									</tr>
 									<tr>
 										<td colspan="2"><div>Eve</div></td>
 									</tr>
 									<tr>
 										<td colspan="2"><div> ' . $member['address'] . ' </div></td>
 									</tr>
 								</table>';
 			}
 			//$this->pdfLayout = sprintf('<table style="width:100%"><tr><td></td><td><div>%s</div></td><td></td><td></td></tr><tr><td><div>president- %s</div></td><td><div> %s</div></td></tr><tr><td></td><td></td><td><div> %s </div></td><td><div> %s </div></td></tr><tr><td><div> %s </div></td><td></td><td><div> %s </div></td><td><div> %d </div></td></tr><tr><td colspan="2"><div>Eve</div></td></tr><tr><td colspan="2"><div> %s </div></td></tr></table>' ,'');
 		/*	$this->pdfLayout .= '<div id="asg-col-1"><h1>'. $pdfConfigs['site_name'] .'</h1> ';
	 		$this->pdfLayout .= sprintf("<div class='asg-president-info'><p>%s - %s %s'</p></div>" , 
	 										$pdfConfigs['president'] ,
	 										$president['name'],
	 										$president['cell_phone']
	 									);
	 		$this->pdfLayout .= sprintf("<div class='asg-member-info'>
	 										<p>
	 										<span class='asg-name'>%s<span>
	 										<span class='asg-tel'>%s<span>
	 										<span class='asg-address'>%s<span>
	 										</p>
	 									</div>" , 
	 										$member['name'],
	 										$member['cell_phone'],
	 										$president['address']
	 									);
	 		$this->pdfLayout .= '</div>';
	 		$this->pdfLayout .= sprintf("<div id='asg-col-2'><div class='asg-office-info'><h5>%s<h5><p>%s<p></div>",
	 										$pdfConfigs['thank_you']['thank_you_msg'],
	 										$pdfConfigs['thank_you']['extra']
									);
	 		$this->pdfLayout .= sprintf("<div><h4>%s</h4>
	 										<div class='asg-office_info'>
	 											<h3>%s<h3>
		 										<p>
		 											<span class='asg-location'>%s<span>
			 										<span class='asg-address'>%s<span>
			 										<span class='asg-tel'>%s<span>
			 										<span class='asg-url'>%s<span>
		 										</p>
										</div>",
										$pdfConfigs['site_name'],
										$pdfConfigs['office_info']['membership_card'],
										$pdfConfigs['office_info']['location'],
										$pdfConfigs['office_info']['address'],
										$pdfConfigs['office_info']['tel'],
										$pdfConfigs['office_info']['url']
									);
	 		$this->pdfLayout .=  sprintf("<div class='asg-member-info-col-2'>
	 										<p>
	 										<span class='asg-name'>%s<span>
	 										<span class='asg-tel'>%s<span>
	 										</p>
	 									</div>
	 									<div class='asg-member-info-col-2'>
	 										<p>
	 										<span class='asg-join-data'>%s<span>
	 										<span class='asg-id'>%s<span>
	 										</p>
	 									</div>" , 
	 										$member['address'],
	 										$member['name'],
	 										$member['created'],
	 										$member['id']
	 									);
	 		$this->pdfLayout .=  sprintf("<div class='asg-signature'>
	 										<p>
	 										<span class='asg-dash'>-------------------------<span>
	 										<span class='asg-name'>%s<span>
	 										</p>
	 									</div>
	 									" , 
	 										$pdfConfigs['signature']
	 									);*/
			
			$this->pdfLayout = apply_filters('asg_pdf_layout' , $this->pdfLayout);
	 		return $this->pdfLayout;
  		
 		
  	}


 	/**
    * This method is used to generate pdf file for member
    * section
    * @param null
    * @return null
    * @since 1.0
    * 
    **/


	function generatePdf($memberInfo = '') { 
		ob_end_clean();
		try {
		   $memId = isset($_REQUEST['id']) ? $_REQUEST['id'] : 1;
		   $filter['id'] = $memId;
		   $memberInfo = empty($memberInfo) ? $this->getMemberList($filter) : $memberInfo;
		   apply_filters('asg_membership_pdf_layout' , $memberInfo);
		   $mpdf = new mPDF('utf-8','A4');
		   $mpdf->debug = false;
		   $stylesheet = file_get_contents( PLUGIN_URL .'/assets/css/table.css' ); // external css
		   $mpdf->WriteHTML($stylesheet,1);
		   $mpdf->WriteHTML($this->pdfLayout , 2);
		   //$mpdf->Output('MyPDF.pdf', 'D');
		   $mpdf->Output($memberInfo[0]['first_name'] . '.pdf' , 'D');
		} catch(HTML2PDF_exception $e) {
	        echo $e;
	    }
	    exit();
 	}

 	
 	/**
    * This method is used to set limit value from setting
    * for pagination
    * @param null
    * @return null
    * @since 1.0
    * 
    **/


	function _setLimit() { 
		$this->_limit = get_option('asg-member-limit');
 		$this->_limit = is_numeric($this->_limit) && !empty($this->_limit) ? 
 						$this->_limit : 
 						10;
 	}


 	/**
 	 * This method is used config default values
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	function _initDefaultConfig() { 
 	 	$this->config= array(
							'title' => __('Title 2', TEXT_DOMAIN),
							'add'	=> __('Add New Title 2', TEXT_DOMAIN),
							'tab'	=> 'tab2',
						);
 	}

 	/**
 	 * This method is used to get all presidents
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	function getAllPresidents() { 
 		$titleModel = new ASG_Title1_Model();
 		$presidentId = $titleModel->getPresidentId();
 		$presidentId = $presidentId[0]['id'];
 		$this->setTableName();
 		$columns = array('id','first_name' , 'middle_name', 'last_name');
 		$where = array(
 					'title1_id' => $presidentId
 			   );
 		return ASG_DAO_Access::selectRows( $where , ARRAY_A , $columns );
 	} 

 	/**
 	 * This method is used to get all members
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function getAllMembers() { 
 		$titleModel = new ASG_Title1_Model();
 		$presidentId = $titleModel->getPresidentId();
 		$presidentId = $presidentId[0]['id'];
 		$this->setTableName();
 		$columns = array('id','CONCAT_WS(" ", `first_name`, `last_name`) AS `name`' );
 		return ASG_DAO_Access::selectAllRows( ARRAY_A , $columns  );
 	} 

 	/**
 	 * This method is used to get all members
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function giveMeAllMemberInfo() { 
 		$this->setTableName();
 		$pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
 		ASG_DAO_Access::$limit = get_option('asg-mailchimp-limit');
 		ASG_DAO_Access::$offset = ( $pagenum - 1 ) * ASG_DAO_Access::$limit;
 		//$orderExp =  ' ORDER BY id ';
 		return ASG_DAO_Access::selectAllRows( ARRAY_A );
 	} 

 	/**
 	 * This method is used to get all members
 	 * @param $mid
 	 * @return Array
 	 * @since 1.0
 	 * 
 	 **/

 	public static function asgGetMemberInfo( $mid ) { 
 		/*$columns = array( 
			t1 => array( 'id','chapter','member_id'),
			t2 => array('first_name')
			);*/
		$tableName = array( 
			'table1' => MEMBER,
			'table2' => USER
			);
		$where = array(
					'id' => $mid
				);
		$on = 't1.user_id = t2.ID ';
		$result = ASG_DAO_Access::join($tableName, $on , array() , "" , $where);
		$result = array_pop($result);
 		return $result;
 	} 


 	/**
 	 * This method is used to get all members with chapter
 	 * @param $memberId
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function getMemberList($filter = '') { 
 		$pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
 		ASG_DAO_Access::$limit = $this->_limit;
 		ASG_DAO_Access::$offset = ( $pagenum - 1 ) * $this->_limit;
 		// member_id of chapter table = president_id
 		$columns = array( 
      					t1 => array('id','user_id','first_name', 'middle_name', 'last_name' , 'created' , 'cell_phone' ,'address','status', 'join_date'),
      					t2 =>array( 'chapter','member_id' )
      				);
      	$tableName = array( 
      				'table1' => MEMBER,
      				'table2' => CHAPTER
      			);
      	$on = 't1.chapter_id = t2.id';
      	$orderExp =  ' ORDER BY t1.id ';
		return empty($filter) ? 
			   ASG_DAO_Access::join($tableName, $on , $columns , $orderExp) :
			   ASG_DAO_Access::join($tableName, $on , $columns , $orderExp , $filter) ;
 	} 

 	 	
 	/**
 	 * This method is used to get particular president info by 
 	 * using id
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function getPresidentInfo() { 
 		$this->setTableName();
 		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
       		$columns = array(
       						'id',
       						'user_id',
       						'first_name', 
       						'middle_name', 
       						'last_name',
       						'work_phone',
							'ext',
							'zip',
							'city',
							'state',
							'address'
       					 );
       		$presidentId = isset($_POST['presidentId']) ? sanitize_text_field($_POST['presidentId']) : '';
       		$where = array(
 					'id' => $presidentId
 			   );
       		$result = ASG_DAO_Access::selectRows( $where , ARRAY_A , $columns );
       		$result = array_pop($result);
       		if(!empty($result['user_id'])) {
	       		$user = get_user_by( 'ID', $result['user_id'] );
	       		$usrData = $user->data;
	       		$email = $usrData->user_email;
	       		$result['email'] = $email;
	       	}
       		echo json_encode($result);
 			wp_die();
 		} else {
 			$columns = array(	
 								'id',
 								'CONCAT_WS(" ", `first_name`, `last_name`,`middle_name`) AS `name`',
 								'cell_phone'
 							);
 			$where = array(
 					'id' => $this->_pId
 			   );
 			$result = ASG_DAO_Access::selectRows( $where , ARRAY_A , $columns );
 			return array_pop($result);
 		}
 	} 


 	/**
 	 * This method is used to get particular member info by 
 	 * using id
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function getMemberInfo() { 
 		$memberId = isset($_POST['memberId']) ? sanitize_text_field($_POST['memberId']) : '';
 		$columns = array( 
      					t1 => array('id','chapter_id', 'city', 'state','home_phone', 'address', 'zip'),
      					t2 =>array( 'chapter' )
      				);
      	$tableName = array( 
      				'table1' => MEMBER,
      				'table2' => CHAPTER
      			);
      	$on = 't1.chapter_id = t2.id';
		$where = array('id' => $memberId);
		$result = ASG_DAO_Access::join($tableName, $on , $columns , 'ORDER BY t1.id' , $where );
		$result = is_array($result) && count($result) > 0  ? array_pop($result) : array();
		$error = count($result) > 0 ? false : true;
      	echo json_encode(array(
      						'result' => $result,
      						'error'  => $error
      					));
 		wp_die();
 	} 

 	/**
	 * This method is used by mailchimp report to
	 * get members batch
	 * @param $cols
	 * @param $whr
	 * @return boolean
	 * @since 1.0
	 **/

	public static function getBatchMemberInformation($ids) { 
		global $wpdb;
        $query = "SELECT t1.`first_name` as FNAME,t1.`last_name` as LNAME,t2.`user_email` EMAIL " ;
        $query .= "  FROM " . MEMBER . " t1";
        $query .= " INNER JOIN " . USER . " t2 ";
        $query .= " ON t1.user_id = t2.ID";
        $query .= " WHERE t1.id IN (" . $ids . ")";
		return $wpdb->get_results($query , ARRAY_A);
    }

	/**
	 * This method is used by mailchimp report to
	 * get members batch
	 * @param $cols
	 * @param $whr
	 * @return boolean
	 * @since 1.0
	 **/

	public static function updateMemberMailchimpStatus($ids) { 
		global $wpdb;
		$query = " UPDATE " .  MEMBER . " SET `sync_status` = 1 WHERE id IN (" . $ids . ")";
		return $wpdb->query($query);
    }



   	/**
 	 * This method is used to get all member info
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/
  	public static function getAllMemberInfo( $mId ) { 
    	$tableName = array( 
      				'table1' => MEMBER,
      				'table2' => USER
      			);
      	$on = 't1.user_id = t2.id';
      	$where = array(
 					'id' => $mId
 			   );
		return array_pop(ASG_DAO_Access::join($tableName, $on , $columns , $where ));
 		
   	}

   	/**
    * This method is used to send mail new members
    * in asg
    * @param null
    * @return null
    * @since 1.0
    * 
    **/


	function sendMailToNewUser($userInfo) { 
		$userEmail = isset($userInfo['email']) ? $userInfo['email'] : '';
		$adminEmail = get_option( 'admin_email' );
		if(empty($userEmail))
			return false;
		$to = 'nil.gurung@podamibenepal.com';
		$subject = 'Welcome to asg world !';
		$body = "Thank you for your registration ! \n";
		$body .= ' Your account details are as follows : \n';
		$body .= ' Username : ' . $userInfo['username']  . '\n';
		$body .= ' Password : ' . $userInfo['password']  . '\n';
		$headers = 'Content-Type: text/html; charset=UTF-8';
		$headers .= 'From: ' . $adminEmails . "\r\n";
		wp_mail( $to, $subject, $body, $headers );
 	}

 	/**
	 * This method is used for check for existence
	 * of username
	 * @param null
	 * @return null
	 * @since 1.0
	 * 
	 **/

 	private function _checkUserNameExist($username) {
 		$userId = username_exists( $username );
	    if( empty($username) || $userId ) {
	       $this->errors['first_name'] = __('First or Last Name Already Exist', TEXT_DOMAIN);
	    }
	    return;
 	}

 	/**
	 * This method is used for check for existence
	 * of email
	 * @param null
	 * @return null
	 * @since 1.0
	 * 
	 **/

 	private function _checkUserEmailExist($email) {
 		if(empty($email)) {
	      $this->errors['user_email'] = __('Empty Email' , TEXT_DOMAIN);
	    } else if(!is_email($email)) {
	      $this->errors['user_email'] = __('Invalid Email' , TEXT_DOMAIN);
	    } else if(email_exists($email) != false) {
	      $this->errors['user_email'] = __('Email Already Exist' , TEXT_DOMAIN);
		} 
		return;
 	}

 	/**
	 * This method is used for check for title
	 * @param null
	 * @return null
	 * @since 1.0
	 * 
	 **/

 	private function _checkTitle() {
	 	if (
 			empty($_POST['title1_id']) &&
 			empty($_POST['title2_id']) && 
 			empty($_POST['title3_id']) 
 		) {
				$this->errors['title1_id'] = __('Any one title should be select' , TEXT_DOMAIN);
		}
		return;
 	}
  
	/**
	 * This method is used for create member
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

 	public function create() {  
 		// checking for nonce value CSRF
 		if( do_action('asg_check_for_nonce' , '')) {
 			return false;
 		}
 		$this->setTableName();
 		$member = isset($_REQUEST['member']) ? $_REQUEST['member'] : '';
 		$formValidator = new ASG_Form_Validator($this->_rules);
 		$userInfo = array();
 		//if validate() return false after vaidation
 		if(!$formValidator->validate($member)) {
 			$this->errors = $formValidator->getJSONOrArray(true);
 		}
 		$userdata = isset($_REQUEST['user']) ? $_REQUEST['user'] : '';
 		$firstname = isset($member['first_name']) ? $member['first_name'] : '';
	    $lastname = isset($member['last_name']) ? $member['last_name'] : '';
	    $userInfo['username'] = $firstname . '_' . $lastname;
	    $userInfo['email'] = isset($userdata['user_email']) ? $userdata['user_email'] : '';
	    $user_id = '';

	    // validation of users
	    $this->_checkUserNameExist($userInfo['username']);
	    $this->_checkUserEmailExist($userInfo['email']);
	    $this->_checkTitle();
				
		if(count($this->errors) > 0 ) {
	       return false;
	    } 
	    $userInfo['password'] = wp_generate_password( $length=12, $include_standard_special_chars=false );
		$user_id = wp_create_user( $userInfo['username'] , $userInfo['password'] , $userInfo['email'] );
		if($user_id)
			do_action('asg_mail_user' , $userInfo);
	    $now = date('Y-m-d h:i:s ', time());
	    $_REQUEST['member']['user_id'] = $user_id;
	    $_REQUEST['member']['created'] = $now;
	    $_REQUEST['member']['updated'] = $now;
	    /**
		 * do sthg before member info edited
		 **/
		do_action('asg_member_inserted');
		$this->success = true;
		$message = __( 'Created Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
	    return ASG_DAO_Access::insertRow($_REQUEST['member'] , ARRAY_A);
	}


	/**
	 * This method is used for edit member
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function edit() {
		//var_dump(array_key_exists( 'asg_check_for_nonce' , $GLOBALS['wp_filter']));die;
		//debug(do_action('asg_check_for_wpnonce' , ''));
		// checking for nonce value CSRF
 		if(do_action('asg_check_for_nonce' , '')) {
 			echo 'Invalid nonce value';
 			return false;
 		}

		$this->setTableName();
		/**
		 * do sthg  member info edited
		 **/
		do_action('asg_member_edit');
		$user_id = wp_update_user( array( 'ID' => $_POST['user_id'], 'user_email' => $_POST['user']['user_email'] ));
		if ( is_wp_error( $user_id ) ) {
			$message = __( 'Error occured during update of member information , Try again !', TEXT_DOMAIN  );
			$type = 'error';
			add_settings_error(
		        'error',
		        esc_attr( 'error' ),
		        $message,
		        $type
		    );
		    $this->success = false;
		    return false;
		}	
		$this->success = true;
		$message = __( 'Updated Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		return ASG_DAO_Access::updateRow($_REQUEST['member'] , $this->_id);
	}

	/**
	 * This method is used for delete member
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function delete() { 
		if(empty($this->_id))
			return false;
		$this->setTableName();
		/**
		 * do sthg before member info deleted
		 **/
		do_action('asg_member_deleted');
		$ids = explode('.' , $this->_id);
		ASG_DAO_Access::deleteRow($ids[0]);
		wp_delete_user($ids[1]);
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->success = true;
	}

	/**
	 * This method is used for delete member
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function bulkDelete() { 
		$this->setTableName();
		/**
		 * do sthg before member info deleted
		 **/
		do_action('asg_bulk_deleted');
		$ids = isset($_REQUEST['delete_member']) ? $_REQUEST['delete_member'] : array();
		/*if(count($ids) == 0) 
			return false;
		foreach ($ids as $id) {
			$memids = explode('.' , $id);
			$whr['id'] = $memids[0];
			ASG_DAO_Access::deleteRow($whr);
			wp_delete_user($memids[1]);
		}*/
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->success = true;
	}

	/**
	 * This is private method used to get user id 
	 * from member id
	 * @param null
	 * @return boolean
	 * @since 1.0
	 **/
	private function _getUserIdbyMemberId() { 
		global $wpdb;
		$query = "SELECT `user_id` FROM " . MEMBER . " WHERE id = '%d'";
		return $wpdb->get_var($wpdb->prepare($query,$this->_id));

	}
	
	/**
	 * This method is used by report to get membership
	 * info according to parameters
	 * @param $cols
	 * @param $whr
	 * @return boolean
	 * @since 1.0`
	 **/

	public function reportForIndividualMembers($date = '') { 
		global $wpdb;
        $query = "SELECT `id`,`last_name`,`first_name`,`middle_name`,`renewal` , `new`,`late`,`complimentary`,`donation`" ;
        $query .= " FROM " . MEMBER;
        $query .= " WHERE `join_date`= '%s'";
        return $wpdb->get_results($wpdb->prepare($query,$date));
        //return ASG_DAO_Access::selectRows( $whr , ARRAY_A , $cols );
	}

	/**
	 * This method is used by report to get membership
	 * info according to parameters
	 * @param $cols
	 * @param $whr
	 * @return boolean
	 * @since 1.0
	 **/

	public function reportForMembers($date = '') { 
		global $wpdb;
        $query = "SELECT SUM(new) AS new ,SUM(renewal) AS renewal,SUM(late) AS late,SUM(complimentary) AS complimentary,SUM(donation) AS donation " ;
        $query .= " ,SUM(new)+  SUM(renewal)+SUM(complimentary)+SUM(donation)+SUM(late) AS total FROM " . MEMBER;
        $query .= " WHERE `join_date`= '%s'";
        return $wpdb->get_results($wpdb->prepare($query,$date));
        //return ASG_DAO_Access::selectRows( $whr , ARRAY_A , $cols );
	}

	
	/**
	 * This method is used by ajax call for search member
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/


	public function searchMember() { 
		$this->setTableName();
		$post = array_filter($_POST);
		unset($post['action']);
		foreach ($post as $key => $value) {
			$where[$key] =  sanitize_text_field(esc_js($value));
		}
		$columns = array( 
      					t1 => array('id','first_name', 'middle_name', 'last_name'),
      					t2 =>array( 'chapter' )
      				);
      	$tableName = array( 
      				'table1' => MEMBER,
      				'table2' => CHAPTER
      			);
      	$on = 't1.chapter_id = t2.id';
      	$rows = ASG_DAO_Access::join($tableName, $on , $columns , 'ORDER BY t1.id' , $where);
      	$return = array(
      				'rows' => '',
      				'error' => '',
      				'url' => ADMIN_URL.'?page=asg-admin-sub-page2'
      			);
      	if(!empty($rows))
      		$return['rows'] = !empty($rows) ?  $rows : '';
      	else 
      		$return['error'] = __('No Result Found' , TEXT_DOMAIN);
      	echo json_encode($return);
 		wp_die();
	}

	/**
	 * This method is used by ajax call for search member
	 * in mailchimp section
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/


	public function searchMailChimpMember() { 
		$this->setTableName();
		$post = apply_filters('asg_ajax_form_data_formatter' , $_POST['form_value']);
		foreach ($post as $key => $value) {
			$where[$key] =  sanitize_text_field(esc_js($value));
		}
		$columns = array( 
      					
      				);
      	$tableName = array( 
      				'table1' => MEMBER,
      				'table2' => USER
      			);
      	$on = 't1.user_id = t2.ID';
      	$rows = ASG_DAO_Access::join($tableName, $on , $columns , 'ORDER BY t1.id' , $where);
      	$return = array(
      				'rows' => '',
      				'error' => '',
      				'url' => ADMIN_URL.'?page=asg-admin-sub-page2'
      			);
      	if(!empty($rows))
      		$return['rows'] = !empty($rows) ?  $rows : '';
      	else 
      		$return['error'] = __('No Result Found' , TEXT_DOMAIN);
      	echo json_encode($return);
 		wp_die();
	}
	

	/**
	 * This method is used for setting 
	 * rules for validation of all fields
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function _setRulesForValidation() { 

		if(!empty($this->_rules) && count($this->_rules) > 0 )
			return true;
		$this->_rules = array(
							'first_name' => array(
								'rule' => 'required|words',
								'message' => 'First Name is required|First Name should be words '
								),
							'last_name' => array(
								'rule' => 'required|words',
								'message' => 'Last Name is required|Last Name should be words '
								),
							'address' => array(
								'rule' => 'required|words',
								'message' => 'Address is required|Address should be words '
								),
							'country' => array(
								'rule' => 'required',
								'message' => 'Country Field is required'
								),
							'chapter_id' => array(
								'rule' => 'required',
								'message' => 'Chapter Field is required'
								),
						);
		
	}

	/**
 	 * This method returns total number of the members 
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function countMembers() {
 		$this->setTableName();
 		return ASG_DAO_Access::count();
 	} 


	/**
 	 * This method returns total number of pages
 	 * @param $other
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function getNumPages($other = false) {
		$total = $this->countMembers();
		if($other)
			$this->_limit = !empty(get_option('asg-mailchimp-limit')) ?
								get_option('asg-mailchimp-limit'):
								10 ;
		return ceil( $total /  $this->_limit );
	}
}
