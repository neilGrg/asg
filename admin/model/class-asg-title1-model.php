<?php

 class ASG_Title1_Model implements ASG_Titles {

 	protected $_title =  array();
 	protected $_id =  array();
 	const TYPE = 1;
 	protected $_operation = '';
 	protected $_post = '';
 	private $_rules =  array(
				    'title1' => 'not_empty',
				);
 	private $_required = array('title1');
 	private $_errors = array();
 	protected $_validator = '';
 	private $tableName =  TITLE1;
 	public $config	= 	array();
 	public $columns = array(
 		 				'title' => 'title1'
 					);
 	public $titles = array(
 						'TREASURER',
 						'NEWSLETTER EDITOR',
 						'SECRETARY',
 						'2ND VICE PRESIDENT',
 						'1ST VICE PRESIDENT',
 						'PRESIDENT'
 					);

 	function __construct() {
 		$this->_initConfig();
 		ASG_DAO_Access::$tableName = TITLE1;
 		
 	}

 	public function getTableName() {
 		return $this->tableName;
 	}

 	/**
 	 * This method is used to display title1 lists
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function display() { 
		?>
			<div id="add_title"><a href="<?php echo ADMIN_URL . "?page=asg-admin-sub-page4#TB_inline?width=600&height=50&inlineId=my-content-id"; ?> " class="page-title-action thickbox" onclick="var f = new FormHandler(); f.setActionType(this);" data-operation = "add"><?php echo __('Add New Title'); ?></a></div>
			<table class="wp-list-table widefat fixed striped pages" >
				<thead>
					<th><?php echo __('Title 1');?></th>
					<th><?php echo __('Operations');?></th>
				</thead>
				<tbody>
					<?php foreach ($this->titles as $key => $title) { ?>
						<tr>
							<td><?php echo $title['title']; ?></td>
							<td><a href="<?php echo ADMIN_URL . "?page=asg-admin-sub-page4&tab=tab1#TB_inline?width=600&height=50&inlineId=my-content-id"; ?> " class="page-title-action thickbox" onclick="var f = new FormHandler(); f.setActionType(this);" data-operation = "edit" data-id="<?php echo $title['id'];?>" data-value="<?php echo $title['title'];?>"><?php echo __('Edit'); ?></a></td>
							<td><a href="<?php echo ADMIN_URL . "?page=asg-admin-sub-page4&tab=tab1&operation=delete&title_id=". $title['id']  ?>"><?php echo __('Delete'); ?></a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>

			<!-- Add Section -->
			<?php add_thickbox(); ?>

			<div id="my-content-id" style="display:none;" data-parsley-validate >
				<form action="" method="post" onsubmit="event.preventDefault();">
					<div class="form-control">
						<label><?php echo __('Title Name');?></label>
						<input type="text" name="title" value="" placeholder = "Enter Title"  id="title" onfocus="clearMe(this)" required />
						<input type="hidden" name="operation" value="" id="operation" />
						<input type="hidden" name="title_id" value="" id="titleId" />
						<input type="submit" name="save" value="<?php echo __('Save'); ?>" id="titleSave" onclick="var f = new FormHandler(); f.saveData(this);"/>
					</div>
				</form>
			</div>

		<?php
		
	}

	/**
 	 * This method is used config default values
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function _initConfig() { 
 	 	$this->config= array(
							'title' => __('Title 1', TEXT_DOMAIN),
							'add'	=> __('Add New Title 1', TEXT_DOMAIN),
							'tab'	=> 'tab1',
						);
 	}

 	/**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

    public static function getTitles() { 
      ASG_DAO_Access::$tableName = TITLE1;
      $columns = array( 'id','title1');
      return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
    }


 	public function getAllTitles() { 
 		return ASG_DAO_Access::selectAllRows(ARRAY_A );
 	}
 	
 	public function getPresidentId() {
 		$columns = array('id');
 		$where = array('title1' => 'president');
      	return  ASG_DAO_Access::selectRowsUsingLike( $where, ARRAY_A , $columns );
    }
 	
 	public function create() { 
 	 	return ASG_DAO_Access::insertRow($this->_title);
 	}
    
    public function edit() {
     	ASG_DAO_Access::updateRow($this->_title,$this->_id);
    }

    public function delete() {
     	ASG_DAO_Access::deleteRow($this->_id);
    }

    
 }

