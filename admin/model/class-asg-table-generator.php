<?php
/**
 * This is custom class reponsible for all table generator
 * for ASG
 * @package     Asg
 * @subpackage  admin/class
 * @since       1.0
*/

 class ASG_Table_Generator {
 	protected static $tables = array();
 	// ASG for table prefix
 	function __construct() {

 	}

 	/**
     * Create all required table for ASG plugin
     * @param null
     * @return null
     *
     */

 	public static function createTableForASG() {
 			global $wpdb;
	 		//TableGenerator::$db = $wpdb;
 			$schema = new ASG_Schema( $wpdb);
 			foreach (self::$tables as $tableName => $columns) {
 				$tableName =  TABLE_PREFIX . $tableName;
 				$table = $schema->createTable($tableName);
	 			foreach ($columns as $key => $col) { 
	 				$table->addColumn($col['column'])->type($col['type']);
				}
			 	$table->build();
 			}
 		}

 	/**
     * Intialise the name of table to create
     * @param null
     * @return null
     * Data Types = ['BOOLEAN' 'INT1' 'INT2' 'INT4' 
     *	'INT8' 'FLOAT' 'DOUBLE''VARCHAR128'
     *	'VARCHAR256''VARCHAR512'  'TEXT' 'LONGTEXT' 
     *	'DATE''DATETIME' 'TIMESTAMP'  'BLOB' ]       
     */

 	public static function initTableStructureForASG() {
 		ob_start();
 		self::_buildTitleTable();
 		self::_buildChapterTable();
 		self::_buildBODTables();
 		self::_buildMemberTable();
 		self::_buildRegionTables();
 		self::_buildMemberAuditTables();
 		self::_buildMembershipCardTables();
 		self::_buildCountryStateCityTables();
 		add_filter('asg_table' , array( __Class__, 'asg_hook_table' ));
 		ob_end_clean();
 		
	}

	public static function asg_hook_table($table = array()) { 
		ob_start();
		self::$tables = $table;
		self::createTableForASG();
		ob_end_clean();
	}

	
	

	/**
 	 * This method is used to call all required table build method 
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/


	private static function  _buildTitleTable() {
		ob_start();
		self::$tables = array(
						'title1' => array (
											array(
												'column'=> 'title1',
												'type'	=> VARCHAR128
											)
										),
						'title2' => array (
											array(
												'column'=> 'title2',
												'type'	=> VARCHAR128
											)
										),
						'title3' => array (
											array(
												'column'=> 'title3',
												'type'	=> VARCHAR128
											)
										)
						);
		self::createTableForASG();
		ob_end_clean();
	}

	/**
 	 * This method is used to call all required table build method 
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private static function  _buildCountryStateCityTables() {
		ob_start();
		self::$tables = array(
						'country' => array (
											array(
												'column'=> 'sort_name',
												'type'	=> VARCHAR50
											),
											array(
												'column'=> 'name',
												'type'	=> VARCHAR128
											)
										),
						'state' => array (
											array(
												'column'=> 'country_id',
												'type'	=> VARCHAR128
											),
											array(
												'column'=> 'sort_name',
												'type'	=> VARCHAR50
											),
											array(
												'column'=> 'name',
												'type'	=> VARCHAR128
											)
										),
						'city' => array (
											array(
												'column'=> 'state_id',
												'type'	=> VARCHAR128
											),
											array(
												'column'=> 'sort_name',
												'type'	=> VARCHAR50
											),
											array(
												'column'=> 'name',
												'type'	=> VARCHAR128
											)
										)
						);
		 //array_push(self::$tables,$titles);
		 self::createTableForASG();
		ob_end_clean();
	}


	


	/**
 	 * This method is used to buildialize chapter table structure
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private static function  _buildChapterTable() {
		ob_start();
		self::$tables = array(
								'chapter' => array (
												array(
													'column'=> 'member_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'region_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'chapter_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'email',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'tel',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'ext',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'address',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'city',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'state',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'zip',
													'type'	=> VARCHAR128
												),
										)
									);
		//array_push(self::$tables,$chapter);
		self::createTableForASG();
		ob_end_clean();
	}

	/**
 	 * This method is used to buildialize BOD table structure
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private static function  _buildBODTables() {
		ob_start();
		self::$tables = array(
			'bod' => array (
							array(
								'column'=> 'member_id',
								'type'	=> INT4
							),
							array(
								'column'=> 'membership_id',
								'type'	=> INT4
							),
							array(
								'column'=> 'chapter_id',
								'type'	=> INT4
							),
							array(
								'column'=> 'address',
								'type'	=> VARCHAR50
							),
							array(
								'column'=> 'city',
								'type'	=> VARCHAR50
							),
							array(
								'column'=> 'state',
								'type'	=> VARCHAR50
							),
							array(
								'column'=> 'zip',
								'type'	=> VARCHAR50
							),
							array(
								'column'=> 'email',
								'type'	=> VARCHAR50
							),
							array(
								'column'=> 'phone',
								'type'	=> VARCHAR50
							)
						)
				);
		self::createTableForASG();
		ob_end_clean();
	}

	/**
 	 * This method is used to buildialize member table structure
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private static function  _buildMemberTable() {
		ob_start();
		self::$tables = array(
								'member' => array (
												array(
													'column'=> 'user_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'membership_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'region_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'title1_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'title2_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'title3_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'title3_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'chapter_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'country',
													'type'	=> VARCHAR512
												),
												array(
													'column'=> 'state',
													'type'	=> VARCHAR512
												),
												array(
													'column'=> 'city',
													'type'	=> VARCHAR512
												),
												array(
													'column'=> 'first_name',
													'type'	=> VARCHAR512
												),
												array(
													'column'=> 'middle_name',
													'type'	=> VARCHAR512
												),
												array(
													'column'=> 'last_name',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'work_phone',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'ext',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'home_phone',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'cell_phone',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'work_phone',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'ext',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'address',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'zip',
													'type'	=> INT4
												),
												array(
													'column'=> 'field_updated',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'transfer_from',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'transfer_date',
													'type'	=> DATETIME
												),
												array(
													'column'=> 'new_membership_fees',
													'type'	=> MONEY
												),
												array(
													'column'=> 'membership_renewal_fees',
													'type'	=> MONEY
												),
												array(
													'column'=> 'membership_fees',
													'type'	=> MONEY
												),
												array(
													'column'=> 'late_membership_fees',
													'type'	=> MONEY
												),
												array(
													'column'=> 'complimentary_fees',
													'type'	=> MONEY
												),
												array(
													'column'=> 'complimentary',
													'type'	=> BOOLEANS
												),
												array(
													'column'=> 'check_no',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'donation',
													'type'	=> MONEY
												),
												array(
													'column'=> 'payment_type',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'comments',
													'type'	=> TEXT
												),
												array(
													'column'=> 'sponsor',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'auto_renewal',
													'type'	=> BOOLEANS
												),
												array(
													'column'=> 'source',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'notes',
													'type'	=> VARCHAR512
												),
												array(
													'column'=> 'new',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'renewal',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'compl',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'late',
													'type'	=> VARCHAR128
												),
												array(
													'column'=> 'join_date',
													'type'	=> DATETIME
												),
												array(
													'column'=> 'paid_thru',
													'type'	=> DATETIME
												),
												array(
													'column'=> 'sale_no_sale',
													'type'	=> BOOLEANS
												),
												array(
													'column'=> 'eligible_for_board',
													'type'	=> BOOLEANS
												),
												array(
													'column'=> 'status',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'sync_status',
													'type'	=> INT4
												),
												array(
													'column'=> 'is_cab_user',
													'type'	=> BOOLEANS
												),
												array(
													'column'=> 'updated_by',
													'type'	=> VARCHAR512
												),array(
													'column'=> 'created',
													'type'	=> DATETIME
												),array(
													'column'=> 'updated',
													'type'	=> DATETIME
												)
										)
									);
		self::createTableForASG();
		ob_end_clean();
	}

	/**
 	 * This method is used to buildialize region table structure
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private static function  _buildRegionTables() {
		ob_start();
		self::$tables = array(
								'region' => array (
												array(
													'column'=> 'member_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'membership_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'region',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'address',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'city',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'state',
													'type'	=> VARCHAR256
												),
												array(
													'column'=> 'zip',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'regional_rep',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'phone',
													'type'	=> VARCHAR50
												)
											)
									);
		self::createTableForASG();
		ob_end_clean();
	}

	/**
 	 * This method is used to buildialize member_audit_table table structure
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private static function  _buildMemberAuditTables() {
		ob_start();
		self::$tables = array(
								'member_audit' => array (
												array(
													'column'=> 'member_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'membership_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'email',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'name',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'property',
													'type'	=> VARCHAR256
												),
												array(
													'column'=> 'change_from',
													'type'	=> VARCHAR256
												),
												array(
													'column'=> 'change_to',
													'type'	=> VARCHAR256
												),
												array(
													'column'=> 'crud_status',
													'type'	=> INT1
												),
												array(
													'column'=> 'sync_status',
													'type'	=> INT1
												),array(
													'column'=> 'tError',
													'type'	=> VARCHAR256
												),array(
													'column'=> 'updated_by',
													'type'	=> VARCHAR256
												),array(
													'column'=> 'updated',
													'type'	=> DATETIME
												),
											),
								'member_audit_property' => array (
												array(
													'column'=> 'property',
													'type'	=> VARCHAR256
												),
												array(
													'column'=> 'display_name',
													'type'	=> VARCHAR256
												)
											)
									);
		self::createTableForASG();
		ob_end_clean();
	}

	/**
 	 * This method is used to buildialize membership_card_table table structure
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	private static function  _buildMembershipCardTables() {
		ob_start();
		self::$tables = array(
								'membership_card' => array (
												array(
													'column'=> 'member_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'membership_id',
													'type'	=> INT4
												),
												array(
													'column'=> 'last_name',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'first_name',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'address',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'city',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'state',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'zip',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'paid_thru',
													'type'	=> DATETIME
												),
												array(
													'column'=> 'status',
													'type'	=> VARCHAR50
												),
												array(
													'column'=> 'president',
													'type'	=> VARCHAR50
												)
											)
									);
		self::createTableForASG();
		ob_end_clean();
	}

	
 }