<?php

 class ASG_City_Model {

 	public $_cities =  array();
 	
 	

 	function __construct() {
 		$this->_initConfig();
 		ASG_DAO_Access::$tableName = CITY;
 		
 	}

 	public function getTableName() {
 		return $this->tableName;
 	}

 	
	/**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

	public static function getAllCities() { 

		ASG_DAO_Access::$tableName = CITY;
		$columns = array( 'id','city');
		return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
	}

 	public function create() { 
 	 	return ASG_DAO_Access::insertRow($this->_title);
 	}
    
    public function edit() {
     	ASG_DAO_Access::updateRow($this->_title,$this->_id);
    }

    public function delete() {
     	ASG_DAO_Access::deleteRow($this->_id);
    }

    
 }

