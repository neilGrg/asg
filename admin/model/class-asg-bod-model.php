<?php

 class ASG_Bod_Model {

 	public $bodLists = array();
 	protected $_operation = '';
 	private static $_defaultConfig = array();
 	protected $_id =  array();
 	private static $_tableName = BOD;
 	protected $_type =  array();
 	private $_chapterData = array();
 	private $_rules =  array();
 	private $_required = array('bod');
 	private $_errors = array();
 	protected $_validator = '';
 	private $_bods = array();
 	private $_limit = '';
 	public $success = false;

 	function __construct() {

 		$this->_id['id'] = isset($_REQUEST['bod_id']) ? sanitize_text_field($_REQUEST['bod_id']) : '';
 		$this->_post = $posts;
        $this->setTableName();
        $this->_setLimit();
        $this->_setRulesForValidation();
        
 	}

 	/**
    * This method is used to set table name 
    * array from post data
    * @param $post
    * @return Array
    * @since 1.0
    * 
    **/


	public function setTableName() {

		ASG_DAO_Access::$tableName = self::$_tableName;	
 	}

 	/**
    * This method is used to set limit value from setting
    * for pagination
    * @param null
    * @return null
    * @since 1.0
    * 
    **/


	public function _setLimit() {
		$this->_limit = get_option('asg-bod-limit');
 		$this->_limit = is_numeric($this->_limit) && !empty($this->_limit) ? 
 						$this->_limit : 
 						10;
 	}


 	/**
	 * This method is used for setting 
	 * rules for validation of all fields
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function _setRulesForValidation() { 

		if(!empty($this->_rules) && count($this->_rules) > 0 )
			return true;
		$this->_rules = array(
			'member_id' => array(
				'rule' => 'required',
				'message' => 'Member Id is required'
				),
			'address' => array(
				'rule' => 'required',
				'message' => 'Address is required'
				),
			'city' => array(
				'rule' => 'required',
				'message' => 'City is required'
				),
			'state' => array(
				'rule' => 'required',
				'message' => 'State is required'
				),
			'zip' => array(
				'rule' => 'required|number',
				'message' => 'Zipcode is required|Zipcode should be number'
				),
			'phone' => array(
				'rule' => 'required|number',
				'message' => 'Telephone is required|Telephone should be number'
				)
		);

	}

	

 	/**
 	 * This method returns all the BOD LISTS
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function getBodLists() {
 		global $wpdb;
 		$pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
 		$offset = ( $pagenum - 1 ) * $this->_limit;
 		$columns = array( 'id', 'member_id', 'member','chapter');
 		$query = "SELECT t1.`id` , t1.`member_id`, CONCAT_WS(\" \", `first_name`,`middle_name`, `last_name`) AS `name` ,`chapter` FROM " . BOD . " t1
				INNER JOIN ".  CHAPTER ."  t2 ON t1.chapter_id = t2.id
				INNER JOIN ". MEMBER ." t3  ON t1.member_id = t3.id
				ORDER BY t3.id
				LIMIT $offset, " . $this->_limit;
		return 	$wpdb->get_results( $query , ARRAY_A);
 	}


 	/**
    * This method is used to return only bod data into 
    * array by using id
    * @param $id (bod id)
    * @return Array
    * @since 1.0
    * 
    **/

    public function getBODById($id) {
    	$this->setTableName();
 		$columns = array('id', 'member_id',  'chapter_id', 'address', 'city', 'state', 'zip',  'phone');
 		$where = array(
 					'id' => $id
 			   );
 		$result = ASG_DAO_Access::selectRows( $where , ARRAY_A , $columns );
 		$result = array_pop($result);
 		return $result;
    }


 	/**
 	 * This method create new bod
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function create() {
 		$message = __( 'Created Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
 		return ASG_DAO_Access::insertRow($this->_bods);
	}

	/**
 	 * This method edit new bod
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function edit() { 
		$message = __( 'Updated Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		return ASG_DAO_Access::updateRow($this->_bods,$this->_id);
	}

	/**
 	 * This method delete bod
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function delete() {
		ASG_DAO_Access::deleteRow($this->_id);
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->success = true;
		return;
	}

	/**
	 * This method is used for delete chapters in bulk
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/

	public function bulkDelete() { 
		$this->setTableName();
		/**
		 * do sthg before member info deleted
		 **/
		do_action('asg_bod_bulk_deleted');
		$ids = isset($_REQUEST['delete_bod']) ? $_REQUEST['delete_bod'] : array();
		if(count($ids) == 0) 
			return false;
		foreach ($ids as $id) {
			$whr['id'] = $id;
			ASG_DAO_Access::deleteRow($whr);
		}
		$message = __( 'Deleted Successfully', TEXT_DOMAIN );
		update_option('asg-crud-message' , $message);
		$this->success = true;
	}


 	/**
 	 * This method is used to set default values
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public static function _initDefaultConfig() { 
		self::$_defaultConfig = array(
						'operation' => __('Operation', TEXT_DOMAIN),
						'edit' => __('Edit', TEXT_DOMAIN),
						'delete'=> __('Delete', TEXT_DOMAIN),
						'save'=> __('Save', TEXT_DOMAIN),
						'title_name'=> __('Title Name', TEXT_DOMAIN)
					);
		ASG_DAO_Access::$tableName = self::$_tableName;
	}

	/**
 	 * This method is used to set default values
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function saveBod() { 
 		$return = array('errors' => '','success' => '');
	 	$postDatas = isset($_POST['form_value']) ?  $_POST['form_value'] : '';
	 	//json_decode(stripslashes($formValue));
	 	$postDatas = ASG_Helper::formatArray($postDatas);
	 	$this->_bods = isset($postDatas['bod']) ?  $postDatas['bod'] : '';
	 	$this->_id['id'] = isset($postDatas['bod_id']) ?  array_values($postDatas['bod_id'])[0] : '';
	 	$operation = count($postDatas['operation']) > 0 ? array_values($postDatas['operation'])[0] : '';
	 	$postDatas = count($postDatas) > 0 ? $postDatas['bod'] : array();
	 	$formValidator = new ASG_Form_Validator($this->_rules);

 		// if validate() return false after vaidation
 		if(!$formValidator->validate($postDatas)) {
 			$return['errors'] = $formValidator->getJSONOrArray(true);
 			echo json_encode($return);
 			wp_die();
 		}
 		$this->setTableName();
		$return['success'] = $operation != 'edit' ? $this->create() : $this->edit();
		echo json_encode($return);
	 	//$chapterInfo  = $this->getChapterByIdWithPresidentInfo($Cid);
	 	wp_die();

	}

	


	 /**
     *
     * This method is used to get bod list 
     * by using id (ajax method)
     * @param null
     * @return Array
     * @since 1.0
     * 
     **/
     public function getBodInfo() { 
     	$bId = isset($_REQUEST['bodId']) ? sanitize_text_field($_REQUEST['bodId']) : '';
     	$bodInfo  = $this->getBODById($bId);
		echo json_encode($bodInfo);
		wp_die();
 		
     }

    /**
 	 * This method returns total number of the BOD 
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public function countBods() {
 		global $wpdb;
 		
 		$columns = array( 'id', 'member_id', 'member','chapter');
 		$query = "SELECT COUNT(*) FROM " . BOD . " t1
				INNER JOIN ".  CHAPTER ."  t2 ON t1.chapter_id = t2.id
				INNER JOIN ". MEMBER ." t3  ON t1.member_id = t3.id
				";	
		return 	$wpdb->get_var($query);
 	} 

 	/**
 	 * This method returns total number of pages
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function getNumPages() {
		$total = $this->countBods();
		return ceil( $total /  $this->_limit );
	}
    
	
 }