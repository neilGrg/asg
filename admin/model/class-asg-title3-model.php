<?php

 class ASG_Title3_Model implements ASG_Titles {

 	public $titles = array(
 						'BLANK',
 						
 					);
    private $tableName =  TITLE3;
    public  $config   = array();
    public $columns = array(
                        'title' => 'title3'
                    );
    const TYPE = 3;


 	function __construct() {
        $this->_initConfig();
        ASG_DAO_Access::$tableName = TITLE3;
 	}

    public function getTableName() {
        return $this->tableName;
    }

       
    /**
     * This method is used config default values
     * @param null
     * @return void
     * @since 1.0
     * 
     **/

    public function _initConfig() { 
        $this->config= array(
                            'title' => __( 'Title 3', TEXT_DOMAIN ),
                            'add'   => __( 'Add New Title 3', TEXT_DOMAIN ),
                            'tab'   => 'tab3',
                        );
    }

    public function getAllTitles() { 
       return ASG_DAO_Access::selectAllRows(ARRAY_A );
    }

    /**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

    public static function getTitles() { 
      ASG_DAO_Access::$tableName = TITLE3;
      $columns = array( 'id','title3');
      return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
    }

 	/**
 	 * This method is used to display title3 lists
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function display() {
		?>
			<div id="add_title"><h3><?php echo __('Leave blank for further use'); ?></h3></div>
		<?php
	}



 	 public function create() {
 	 	echo "creating";
 	 }
     public function edit() {
     	echo "editing";
     }
     public function delete() {
     	echo "deleting";
     }
 }
