<?php

 class ASG_Geolocation_Model {

 	private $_countries =  array();
 	private $_id = array();
 	private $_datas = '';
 	private $_curTable = '';
 	private $_formData = '';
 	private $_db = '';

 	function __construct() { 
 		global $wpdb;
 		//$this->_initConfig();
 		add_filter('asg_get_countries' , array($this , 'getCountries'));
	    add_filter('asg_get_states' , array($this , 'getStates'));
	    add_filter('asg_get_cities' , array($this , 'getCities'));
	    $this->_db = $wpdb;
 		
 	}
 	
 	
	/**
    * This method is used to return all countries into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

	function getCountries() {  
	  ASG_DAO_Access::$tableName = COUNTRY;
	  ASG_DAO_Access::$limit = 0;
	  $columns = array( 'id','name');
	  $res = ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
	  return $res;
	}

	/**
	* This method is used to return all states into 
	* array 
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	function getStates() { 
	  ASG_DAO_Access::$tableName = STATE;
	  $columns = array( 'id','name' , 'sort_name');
	  return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
	}

	/**
	* This method is used to return all states into 
	* array 
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	public static function getsortNameStates() { 
	  ASG_DAO_Access::$limit = '';
	  ASG_DAO_Access::$tableName = STATE;
	  $columns = array( 'sort_name');
	  return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);

	}

	
	/**
	* This method is used to return all citties into 
	* array 
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	function getCities() { 
	  ASG_DAO_Access::$tableName = CITY;
	  $columns = array( 'id','name');
	  return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
	}

	/**
	* This method is used to return country info using Id 
	* array 
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	function getCountryById() { 
	   ASG_DAO_Access::$tableName = COUNTRY;
	   $id = isset($_POST['id']) ? $_POST['id'] : '';
	   $where = array(
 					'id' => $id
 			   );
	   $result = ASG_DAO_Access::selectRows( $where , ARRAY_A );
	   $result = array_pop($result);
	   unset($result['id']);
	   echo json_encode($result);
 	   wp_die();
	}

	/**
	* This method is used to return state info using Id 
	* array 
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	function getStateById() { 
	    ASG_DAO_Access::$tableName = STATE;
	    $id = isset($_POST['id']) ? $_POST['id'] : '';
	  	$where = array(
 					'id' => $id
 			   );
 		$result = ASG_DAO_Access::selectRows( $where , ARRAY_A );
   		$result = array_pop($result);
   		unset($result['id']);
   		echo json_encode($result);
	    wp_die();
	}

	/**
	* This method is used to return all states using country name or id
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	function findStateByCoutryNameOrId() { 
		$value = isset($_POST['value']) ? $_POST['value'] : '';
	   	$query = "SELECT t1.`sort_name` FROM " . STATE . " t1
				INNER JOIN ".  COUNTRY ."  t2 ON t1.country_id = t2.id
				WHERE ";
		$query .= is_numeric($value) ? "t2.id = '". $value ."'" : "t2.name  LIKE  '%" . $value . "%'";	
		//echo $query;
		//echo $wpdb->last_query;die;
		$result = $this->_db->get_results($query);
 		echo json_encode($result);
	    wp_die();
	}

	

	/**
	* This method is used to return all city using  
	* state name
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	function findCityByStateName() { 
  		$name = isset($_POST['name']) ? $_POST['name'] : '';
	    $query = "SELECT t1.name FROM " . CITY . " t1
				INNER JOIN ".  STATE ."  t2 ON t1.state_id = t2.id
				WHERE t2.sort_name  LIKE  '%" . $name. "%'";	
		$result = $this->_db->get_results($query);
 		echo json_encode($result);
	    wp_die();
	}

	/**
	* This method is used to return city info using Id 
	* array 
	* @param null
	* @return Array
	* @since 1.0
	* 
	**/

	function getCityById() { 
  		ASG_DAO_Access::$tableName = CITY;
  		$id = isset($_POST['id']) ? $_POST['id'] : '';
	  	$where = array(
 					'id' => $id
 			   );
 		$result = ASG_DAO_Access::selectRows( $where , ARRAY_A );
   		$result = array_pop($result);
   		unset($result['id']);
   		echo json_encode($result);
	    wp_die();
	}
	

 	public function create() { 
 		return ASG_DAO_Access::insertRow($this->_formData);
 	}
    
    public function edit() { 
    	return ASG_DAO_Access::updateRow($this->_formData,$this->_id);
    }

    public function delete() {
    	return ASG_DAO_Access::deleteRow($this->_id);
    }

    function saveGeoLocation() { 
    	$this->_formData = isset($_POST['form_value']) ? 
		apply_filters('asg_ajax_form_data_formatter' , $_POST['form_value']) :
		'';
		$this->_id['id'] = isset($_POST['id']) ? $_POST['id'] : '';
		$action = $this->_formData['action'];
		unset($this->_formData['_wpnonce']);
		$this->_setTableName();
		$this->_delElemArray();
		//sanitizing fields 
		$this->_formData = array_map( 'sanitize_text_field', $this->_formData );
		$this->_formData = array_map( 'esc_sql', $this->_formData );
		$jsonResult =  array( 
								'result' => '' , 
								'errors' => '' , 
							);
		$id = empty($this->_id['id']) ? $this->create() : $this->edit();
		if(!empty($id)) 
			$jsonResult['result'] = 1;
		else 
			$jsonResult['result'] = 0;
		echo json_encode($jsonResult);
		wp_die();
     	
    }

     function delGeoInfoById() { 
    	$this->_id['id'] = isset($_POST['id']) ? $_POST['id'] : '';
		$section = isset($_POST['section']) ? $_POST['section'] : '';
		$this->_formData['section'] = $section;
		$this->_setTableName();
		$id = $this->delete();
		$jsonResult =  array( 
								'result' => '' , 
								'errors' => '' , 
							);
		///$id = empty($this->_id) ? $this->create() : $this->edit();
		$jsonResult['result'] = 1;
		
		echo json_encode($jsonResult);
		wp_die();
     	
    }


    private function _delElemArray() {
	    $keys = array('update_setting','section','action');
	    // Search for the array key and unset   
	    foreach($keys as $k){
	       unset($this->_formData[$k]);
	    }
	}

    public function _setTableName() { 
    	switch ($this->_formData['section']) {
    		case 'country':
    			ASG_DAO_Access::$tableName  = COUNTRY;
    			break;
    		case 'state':
    			ASG_DAO_Access::$tableName  = STATE;
    			break;
    		case 'city':
    			ASG_DAO_Access::$tableName  = CITY;
    			break;	
    		default:

    			//trigger_error('oops ! wrong value supplied ! try again ');
    			return false;
    			break;
    	}
    }
    

    
 }

