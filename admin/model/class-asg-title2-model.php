<?php

 class ASG_Title2_Model implements ASG_Titles {

 	protected $_title = 'Title 2';
 	public 	$titles = array(
 						'BOD',
 					);
 	private $tableName =  TITLE2;
 	public  $config	= array();
 	public $columns = array(
 		 				'title' => 'title2'
 					);
 	const TYPE = 2;


 	function __construct() {
 		$this->_initConfig();
 		ASG_DAO_Access::$tableName = TITLE2;
 	}

 	public function getTableName() {
 		return $this->tableName;
 	}

 	/**
 	 * This method is used config default values
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function _initConfig() { 
 	 	$this->config= array(
							'title' => __('Title 2', TEXT_DOMAIN),
							'add'	=> __('Add New Title 2', TEXT_DOMAIN),
							'tab'	=> 'tab2',
						);
 	}

 	/**
 	 * This method is used to get all titles
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

 	public static function getAllTitles() { 

 		return ASG_DAO_Access::selectAllRows(ARRAY_A );
 	}

 	/**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

    public static function getTitles() { 
      ASG_DAO_Access::$tableName = TITLE2;
      $columns = array( 'id','title2');
      return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
    }


 	/**
 	 * This method is used to display title1 lists
 	 * @param null
 	 * @return void
 	 * @since 1.0
 	 * 
 	 **/

	public function display() {
		?>
			<!-- <div id="add_title"><a href="<?php echo ADMIN_URL . "?page=asg-admin-sub-page4&tab=tab2&operation=addTB_inline?width=600&height=50&inlineId=my-content-id"; ?>" class="page-title-action thickbox"><?php echo __('Add New Title 2'); ?></a></div>
			<table class="wp-list-table widefat fixed striped pages">
				<thead>
					<th>Title 2</th>
					<th>Operations</th>
				</thead>
				<tbody>
					<?php foreach ($this->titles as $key => $title) { ?>
						<tr>
							<td><?php echo $title; ?></td>
							<td><a href="<?php echo ADMIN_URL . "?page=asg-admin-sub-page4&tab=tab2&operation=addTB_inline?width=600&height=50&inlineId=my-content-id"; ?>" class="page-title-action thickbox"><?php echo __('Edit'); ?></a></td>
							<td><a href=""><?php echo __('Delete'); ?></a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<!-- Add Section -->
			<?php add_thickbox(); ?>
			<div id="my-content-id" style="display:none;">
				<form action="" method="post" >
					<div class="form-control" >
						<label><?php echo __('Title Name');?></label>
						<input type="text" name="title" value="title 1" />
						<input type="submit" name="title" value="<?php echo __('Save'); ?>" />
					</div>
				</form>
			</div> -->
		<?php
		
	}

 	 public function create() {
 	 	echo "creating";
 	 }
     public function edit() {
     	echo "editing";
     }
     public function delete() {
     	echo "deleting";
     }
 }
