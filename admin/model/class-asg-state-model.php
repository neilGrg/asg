<?php

 class ASG_State_Model {

 	public $_states =  array();
 	
 	

 	function __construct() {
 		$this->_initConfig();
 		ASG_DAO_Access::$tableName = STATE;
 		
 	}

 	public function getTableName() {
 		return $this->tableName;
 	}

 	
	/**
    * This method is used to return all title1 into 
    * array 
    * @param null
    * @return Array
    * @since 1.0
    * 
    **/

    public static function getAllStates() { 
      ASG_DAO_Access::$tableName = STATE;
      $columns = array( 'id','state');
      return ASG_DAO_Access::selectAllRows(ARRAY_A , $columns);
    }

 	public function create() { 
 	 	return ASG_DAO_Access::insertRow($this->_title);
 	}
    
    public function edit() {
     	ASG_DAO_Access::updateRow($this->_title,$this->_id);
    }

    public function delete() {
     	ASG_DAO_Access::deleteRow($this->_id);
    }

    
 }

