<?php 

class ASG_Monthly_Report_Model {

	private $_db = array();
	private $_posts = array();
	private $_status = '';
	private $_query = '';

	function __construct() { 
		global $wpdb;
		$this->_posts = !empty($_POST) ? $_POST : '';
		$this->_db = $wpdb;
		add_filter('monthly_rebate_report' , array($this , 'getMonthlyRebates') , 10 , 1);
		add_filter('monthly_new_members' , array($this , 'getMonthlyNewMembers') , 10 , 1);
		add_filter('monthly_new_junior_members' , array($this , 'getMonthlyNewMemberJuniors') , 10 , 1);
		add_filter('monthly_renew_members' , array($this , 'getMonthlyReNewMembers') , 10 , 1);
		add_filter('monthly_renew_junior_members' , array($this , 'getMonthlyReNewMemberJuniors') , 10 , 1);
		add_filter('monthly_tranfer_members' , array($this , 'getMonthlyTransferMembers') , 10 , 1);
		add_filter('monthly_expired_members' , array($this , 'getExpiredMember') , 10 , 1);
		add_filter('monthly_renewal_notice_members' , array($this , 'getRenewalNoticeMembers') , 10 , 1);
		add_filter('monthly_renewal_notice_card_members' , array($this , 'getRenewalNoticeCardMembers') , 10 , 1);
		add_filter('monthly_full_members' , array($this , 'getFullMembers') , 10 , 1);
	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	private function _setQuery() { 
		$this->_query = '';
		$this->_query = "SELECT t1.`id` ,
				 t1.`first_name`,
				 t1.`last_name`,
				 t1.`address`,
				 t1.`state`,
				 t1.`city`,
				 t1.`zip`,
				 t1.`work_phone`, 
				 t1.`join_date`, 
				 t1.`created`
				 FROM " . MEMBER . " t1
					INNER JOIN ".  USER ." t2 ON t1.user_id = t2.ID";
		$this->_query .= " WHERE t1.chapter_id = '%d' AND (t1.join_date BETWEEN '%s' AND  '%s') ";
		
	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getMonthlyRebates() { 

		echo 'calculating monthly rebate';
	}

	/**
	 *
	 *	This method is used to sort array by member Id
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	private function _sortByMemId($a, $b) {
		    return $a['id'] - $b['id'];
	}

	/**
	 *
	 *	This method is used to generate simple query
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	private function _getMonthlyMemberQuery() { 
		$totalChapterValues = array();
		$chapterNames = array();
		$chapterIds = $this->_posts['chapter_id'];
		$inChapterIds = implode( ',' , $this->_posts['chapter_id']);
		$allMembers = array();
		$this->_posts = array_map( 'esc_sql', $this->_posts );
		$counter = array();
		$chapterNames['all'] = __('all' , TEXT_DOMAIN);
		$totalChapterValues['all'] = array();
		foreach( $chapterIds as $chapterId) {
		 	$chapterName = ASG_Chapter_Model::getChapterNameById($chapterId);
		 	$chapterNames[$chapterId] = $chapterName['chapter'];
			$result = $this->_db->get_results(
									 $this->_db->prepare(
										$this->_query , 
										$chapterId,
										$this->_posts['start_date'] , 
										$this->_posts['end_date'] ,
										$this->_status
									),
									ARRAY_A
								);
			//echo $this->_db->last_query;
			//$emb = count($result) == 1 ? $result[0] : $result;
			foreach ($result as $key => $res) { 
				count( $res ) > 0 ? array_push($allMembers , $res) : '';	
			}
			$counter[] = (count($result) > 0)  ? 1 : 0;
			$totalChapterValues[$chapterName['chapter']] = $result;
		}
		
		usort($allMembers , array($this , '_sortByMemId'));
		$totalChapterValues['all'] = $allMembers;

		$counter = array_filter($counter);
		return array(
					'chapters'=> $chapterNames,
					'datas' => $totalChapterValues,
					'count' => count($counter) > 0 ? true : false
				);
	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getMonthlyNewMembers() { 
		if(empty($this->_posts))
			return false;
		//die('hell');
		$this->_setQuery();
		$this->_query .= "  AND status = 'N' ";
		return $this->_getMonthlyMemberQuery();
	}

	

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getMonthlyNewMemberJuniors() {
		if(empty($this->_posts))
			return false; 
		$this->_setQuery();
		$this->_query .= "  AND status = 'JN' ";
		return $this->_getMonthlyMemberQuery();

	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getMonthlyReNewMembers() { 
		if(empty($this->_posts))
			return false;
		$this->_setQuery();
		$this->_query .= "  AND status = 'R' ";
		return $this->_getMonthlyMemberQuery();
	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getMonthlyReNewMemberJuniors() { 
		if(empty($this->_posts))
			return false;
		$this->_setQuery();
		$this->_query .= "  AND status = 'JR' ";
		return $this->_getMonthlyMemberQuery();
	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getMonthlyTransferMembers() { 
		if(empty($this->_posts))
			return false;
		$this->_query = '';
		$this->_query = "SELECT t1.`id` ,
				 t1.`first_name`,
				 t1.`last_name`,
				 t1.`address`,
				 t1.`state`,
				 t1.`city`,
				 t1.`zip`,
				 t1.`work_phone`, 
				 t1.`join_date`, 
				 t1.`created`
				 FROM " . MEMBER . " t1
					INNER JOIN ".  USER ." t2 ON t1.user_id = t2.ID";
		$this->_query .= " WHERE  t1.transfer_from = '%d'   ";
		$this->_query .= " AND (t1.join_date BETWEEN '%s' AND  '%s')";
		return $this->_getMonthlyMemberQuery();
	}

	/**
	 *
	 *	This method is used to get expired member
	 *	between the selected date (Member expired after 1 year)
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getExpiredMember() { 
		if(empty($this->_posts))
			return false;
		$this->_query = " SELECT * FROM 
							(SELECT t2.*,t1.join_date AS reg_date,DATE_ADD(t1.join_date, INTERVAL  1 YEAR ) AS exp
								 FROM `wp_asg_member`  AS  t1 INNER JOIN `wp_asg_member`  AS  t2 
								 	ON t1.id = t2.id) t3
										WHERE t3.chapter_id = '%d' ";
		$this->_query .= " AND t3.exp BETWEEN '%s' AND  '%s' ";								
		return $this->_getMonthlyMemberQuery();
	}

	/**
	 *
	 *	This method is used to get all members who
	 *	need to renew before expiration
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getRenewalNoticeMembers() { 
		if(empty($this->_posts))
			return false;
		$this->_query = " SELECT * FROM 
							(SELECT t2.*,t1.join_date AS reg_date,DATE_ADD(t1.join_date, INTERVAL  1 YEAR ) AS exp
								 FROM `wp_asg_member`  AS  t1 INNER JOIN `wp_asg_member`  AS  t2 
								 	ON t1.id = t2.id) t3
										WHERE (t3.exp BETWEEN DATE_SUB('" . $this->_posts['end_date'] . "' , INTERVAL 2 MONTH)  AND '" . $this->_posts['end_date'] . "') ";
		$this->_query .= " AND t3.chapter_id = '%d' ";
		return $this->_getMonthlyMemberQuery();
	}

	/**
	 *
	 *	This method is used to create auto renewal 
	 *	filter
	 *  @param null 
	 * 	@access private
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	private function _getAutoRenewalQuery() { 
		if(empty($this->_posts))
			return false;
		switch ($this->_posts['auto_renewal_opt']) {
			case '0':
				return " AND t3.auto_renewal != 1 ";
				break;
			case '1':
				return " ";
				break;
			case '2':
				return " AND t3.auto_renewal = 1 ";
				break;
			default:
				# code...
				break;
		}
		
	}

	/**
	 *
	 *	This method is used to get all members who
	 *	need to renew notice card before expiration
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getRenewalNoticeCardMembers() { 
		if(empty($this->_posts))
			return false;
		
		$this->_query = " SELECT * FROM 
							(SELECT t2.*,t1.join_date AS reg_date,DATE_ADD(t1.join_date, INTERVAL  1 YEAR ) AS exp
								 FROM `wp_asg_member`  AS  t1 INNER JOIN `wp_asg_member`  AS  t2 
								 	ON t1.id = t2.id) t3
										WHERE (t3.exp BETWEEN DATE_SUB('" . $this->_posts['end_date'] . "' , INTERVAL 2 MONTH)  AND '" . $this->_posts['end_date'] . "') ";
		$this->_query .= $this->_getAutoRenewalQuery();
		$this->_query .= " AND t3.chapter_id = '%d' ";
		return $this->_getMonthlyMemberQuery();
	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param null 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getFullMembers() { 
		if(empty($this->_posts))
			return false;
		$this->_setQuery();
		$this->_query .= " ORDER BY t1.id ";
		return $this->_getMonthlyMemberQuery();
	}
	
}

new ASG_Monthly_Report_Model();