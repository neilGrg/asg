<?php 

class ASG_Membership_Cards_Model {

	private $_db = array();
	private $_posts = array();
	private $_status = '';
	private $_query = '';
	private $_member = '';

	function __construct() { 
		global $wpdb;
		$this->_db = $wpdb;
		$this->_member = new ASG_Member_Model();
		//$this->generatePdf();
	}

	/**
	 *
	 *	This method is used to generate membership card
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function select() {
		$this->_query = "SELECT ";
		$this->_query .= "t1.`id`,t1.`user_id`,t1.`first_name`,t1.`middle_name`,t1.`last_name`,";
		$this->_query .= " t1.`created`,t1.`cell_phone` ,t1.`address`,t1.`status`,t1.`join_date`,t2.`chapter`,t2.`member_id` FROM ";
		$this->_query .=   MEMBER . " t1 INNER JOIN ".  CHAPTER ." t2 ON t1.chapter_id = t2.id";
		return $this;
	}

	/**
	 *
	 *	This method is used to generate membership card
	 *   based on join date
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function byJoinDate($joinDate) { 
		if(empty($joinDate))
			return $this;
		$this->_query .= " WHERE  DATE(t1.join_date) >= '".  sanitize_text_field(esc_sql($joinDate)) . "'";
		return $this;
	}

	/**
	 *
	 *	This method is used to generate membership card
	 *  based on memberId
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function byId($id) { 
		if(empty($id))
			return $this;
		$this->_query .= " WHERE t1.id = " . sanitize_text_field(esc_sql($id));
		return $this;
	}

	/**
	 *
	 *	This method is used to generate members for membership card
	 *  based on build query
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function executeQuery() {
		if(empty($this->_query))
			return false;
		//echo $this->_query;
		return $this->_db->get_results($this->_query , ARRAY_A);
	}

	/**
	 *
	 *	This method is used to generate membership card
	 *  based on memberId
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function getMembersById() { 
		$id = isset($_POST['member_id']) ? $_POST['member_id'] : '';
		$members =  $this->select()->byId($id)->executeQuery();
		return $this->_member->membershipPdfLayout($members);
		//return apply_filters('asg_membership_pdf_layout' , $members);
		//apply_filters('generate_pdf' , $members);
	}

	/**
	 *
	 *	This method is used to generate membership card
	 *  based on join date
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function getMembersByJoinDate() { 
		$joinDate = isset($_POST['start_date']) ? $_POST['start_date'] : '';
		$members = $this->select()->byJoinDate($joinDate)->executeQuery();
		return $this->_member->membershipPdfLayout($members);
		//apply_filters('generate_pdf' , $members);
	}


}