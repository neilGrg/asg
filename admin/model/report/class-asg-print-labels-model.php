<?php 

class ASG_Print_Label_Report_Model {

	private $_db = array();
	private $_posts = array();
	private $_status = '';

	function __construct() { 
		global $wpdb;
		$this->_posts = !empty($_POST) ? array_filter($_POST) : '';
		$this->_db = $wpdb;
		add_filter('print_label_report' , array($this , 'getPrintLabelReport') , 10 , 1);
	}

	function test($v , $k) {
		switch ($k) {
    		case 'chapter_id':
    			return sprintf("%s IN (%s)" , $k , implode(',' , $this->_posts['chapter_id']));
    			break;
    		case 'title':
    			$titleIds = implode(',', $v);
    			$q = " (t1.title1_id IN (". $titleIds .")   OR t1.title2_id IN (". $titleIds .") OR t1.title3_id IN (". $titleIds ."))";
				return sprintf($q);
    			//return sprintf("%s IN (%s)" , $k , implode(',' , $this->_posts['title']));
    			break;
    		case 'date':
    			$q =  (!empty($v['start']) && isset($v['end']))  ? 
				" (t1.join_date BETWEEN '". $v['start'] . "' AND  '". $v['end'] . "') " :
				" (t1.join_date BETWEEN '". $v['start'] . "' AND  NOW()) " ;
    			return sprintf($q);
    			break;
    		case 'end_date':
    			break;	
    		default:
    			return sprintf("%s='%s'", $k, $v);
    			break;
    	}
							    	
	}

	/**
	 *
	 *	This method is used to generate simple query
	 *  @param 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	private function _getPrintLabelQuery() {
		//$this->_posts = array_map( 'sanitize_text_field', $this->_posts );
		$noListed = array(	
							'_wpnonce', 
							'report_type',
							'report',
							'run',
							'start_date',
							'end_date' 
						);
		$datas = array();
		foreach ($this->_posts as $key => $value) {
			if(!in_array($key , $noListed))
				$datas[$key] = $value;
		}
		if(!empty($this->_posts['start_date']))
			$datas['date']['start'] = $this->_posts['start_date'];
		if(!empty($this->_posts['end_date']))
			$datas['date']['end'] = $this->_posts['end_date'];
		$this->_posts = array_filter(array_map( 'esc_sql', $datas ));
		$this->_posts = array_filter($this->_posts);
		$query = "SELECT 
						 CONCAT_WS(\" \", `first_name`,`middle_name`, `last_name`) AS `name`,
						 t2.`chapter`,
						 t1.`address`,
						 t1.`city`,
						 t1.`state`,
						 t1.`zip`,
						 t6.`user_email`,
						 DATE_ADD(t1.`join_date`, INTERVAL  1 YEAR ) AS expired_date,
						 t1.`work_phone`,	
						 `title1`,
						 `title2`,
						 `title3`
						 FROM " . MEMBER . " t1 ";
		$query .= " INNER JOIN ".  CHAPTER ." t2 ON t1.chapter_id = t2.id ";							
		$query .= " INNER JOIN ".  TITLE1 ." t3 ON t1.title1_id = t3.id";
		$query .= " INNER JOIN ".  TITLE2 ." t4 ON t1.title2_id = t4.id";
		$query .= " INNER JOIN ".  TITLE3 ." t5 ON t1.title3_id = t5.id ";
		$query .= " INNER JOIN ".  USER ." t6 ON t1.user_id = t6.ID ";
		if(count($this->_posts) > 0) {
			$query .= " WHERE ";
			$query .= implode(' AND ' , array_map(
							    array($this , 'test'),
							    $this->_posts,
							    array_keys($this->_posts)
							)
						);
		}
		$result = $this->_db->get_results(
										$query,
										ARRAY_A
									);	
		//echo $result;
		//echo $this->_db->last_query;
		return $result;
	}

	/**
	 *
	 *	This method is used to get monthly rebate for
	 *	each members
	 *  @param $status
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return Array
	 * 	
	 */

	function getPrintLabelReport($status) { 
		return $this->_getPrintLabelQuery();
	}

}

new ASG_Print_Label_Report_Model();