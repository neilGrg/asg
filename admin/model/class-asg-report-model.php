<?php

class ASG_Report_Model {

	public $settings =  array();
	public $settingValues =  array();
	public $errors =  array();
	private $_insertData = array();
	private $_updateData = array();
	private $_settingNames = array();
	private $_formData = array();
	private $_columns = array();
	private $_where = array();

	function __construct() { 


	}

	/**
	 *
	 *	This method is used to build report 
	 *	for membership
	 *  @param null
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function generateReport() {  
		$this->_formData = isset($_POST['form_value']) ? 
		apply_filters('asg_ajax_form_data_formatter' , $_POST['form_value']) :
		'';
		$reportType = isset($this->_formData['report_type']) ? $this->_formData['report_type'] : '';
		$jsonResult =  array( 
								'result' => '' , 
								'errors' => '' , 
								'date' =>  $this->_formData['join_date']
							);

		switch ($reportType ) {
			case 'membership_fee_total':
				$jsonResult['result'] = $this->_getMembershipFeeReport();
				echo json_encode($jsonResult);
				break;
			
			default:

				# code...
				break;
		}
		wp_die();
	}

	/**
	 *
	 *	This method is used to call the required method 
	 *	to get report data
	 *  @param null
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _getMembershipFeeReport() { 
		$result = array();
		// section for total report
		if($this->_formData['report'] == 0) {
			$result = apply_filters('asg_member_report' , $this->_formData['join_date']);
		} else {
			$result = apply_filters('asg_individual_member_report' , $this->_formData['join_date']);
		}
		return $result;
	}

	/**
	 *
	 *	This method is used to set columns and conditions
	 *	to get report data
	 *  @param $flag
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _setColAnCondition($flag) { 
		switch ($flag) {
			case '0':
				$this->_columns = array('new','renewal','late','complimentary','donation');
				$this->_where = array('join_date' => $this->_formData['join_date']);
				break;
			
			default:

				# code...
				break;
		}
		return true;
	}

	/**
	 *
	 *	This method handles monthly report
	 *	for asg
	 *  @param null
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function generateMonthlyReport() {  
		$reportType = isset($_POST['report']) ? (int) $_POST['report'] : '';

		switch ($reportType) {
			case 0:
				return apply_filters('monthly_rebate_report', '');
				break;
			case 1:
				return apply_filters('monthly_new_members', '');
				break;
			case 2:
				return apply_filters('monthly_new_junior_members', '');
				break;
			case 3:
				return apply_filters('monthly_renew_members', '');
				break;
			case 4:
				return apply_filters('monthly_renew_junior_members', '');
				break;
			case 5:
				return apply_filters('monthly_tranfer_members', '');
				break;
			case 6:
				return apply_filters('monthly_expired_members', '');
				break;
			case 7:
				return apply_filters('monthly_renewal_notice_members', '');
				break;
			case 8:
				return apply_filters('monthly_renewal_notice_card_members', '');
				break;
			case 9:
				return apply_filters('monthly_full_members', '');
				break;
			default:
				# code...
				break;
		}
	}

	/**
	 *
	 *	This method is used to generate label report
	 *	for asg
	 *  @param null
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function generatePrintLabelReport() {  
		$reportType = isset($_POST['report']) ? (int) $_POST['report'] : '';

		switch ($reportType) {
			case 0:
				return apply_filters('print_label_report', '');
				break;
			case 1:
				return apply_filters('print_label_report', '');
				break;
			case 2:
				return apply_filters('print_label_report', '');
				break;

			default:
				echo __('Whoops ! Not in the list', TEXT_DOMAIN);
				break;
		}
	}


	/**
	 *
	 *	This method is used to handle monthly report
	 *	for asg
	 *  @param null
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function syncMemberIntoMailChimp() {  
		/*$message = count($_POST['sync_member']) < 1 ? 
				   __(" OOPS ! You haven't select any members or list id " , TEXT_DOMAIN) : 
				   __(" OOPS ! You haven't select any list to sync" , TEXT_DOMAIN);*/
		if(count($_POST['sync_member']) < 1) {
			$message = __(" OOPS ! You haven't select any members or list id " , TEXT_DOMAIN);
			update_option('asg-crud-message' , $message);
			update_option('asg-error-message' , 1);
			return false;
		}
		$memberIds = implode(',' , $_POST['sync_member']);
		$memberInfo = ASG_Member_Model::getBatchMemberInformation($memberIds);
		/*foreach ($memberInfo as $key => $member) {
			$data = [
					    'email'     => $member['EMAIL'],
					    'status'    => 'subscribed',
					    'firstname' => $member['FNAME'],
					    'lastname'  => $member['LNAME']
					];
			debug($this->syncMailchimp($data));
		}*/
		/*$merge_vars = array(
							'EMAIL' => 'test@yourdomain.com',
							'FNAME'=>'Test',
							'LNAME'=>'Account',
							'status' => 'subscribed'
						);
		$api = new MCAPI('db9da9f1d2b21d967f4067313c0f3d28-us12');
		//$client->listBatchSubscribe($apikey, $list['id'], $batch, false, false, false);
		// By default this sends a confirmation email - you will not see new members
		// until the link contained in it is clicked!
		$listId = '83234643ed';
		$optin = true; //yes, send optin emails
		$up_exist = true; // yes, update currently subscribed users
		$replace_int = true; // no, add interest, don't replace
		$vals = $api->listBatchSubscribe($listId,$merge_vars,$optin, $up_exist, $replace_int);
		debug($vals);*/
		/*$api = new MCAPI('db9da9f1d2b21d967f4067313c0f3d28-us12');
 		$listId = '83234643ed';
		$batch[] = array('EMAIL'=>'test@yourdomain.com', 'FNAME'=>'Joe' , 'status' => 'subscribed');
		//$batch[] = array('EMAIL'=>'test1@yourdomain.com', 'FNAME'=>'Me', 'LNAME'=>'Chimp');
		 
		$optin = true; //yes, send optin emails
		$up_exist = true; // yes, update currently subscribed users
		$replace_int = false; // no, add interest, don't replace
		 
		$vals = $api->listBatchSubscribe($listId,$batch,$optin, $up_exist, $replace_int);
		debug($vals);
		if ($api->errorCode){
		    echo "Batch Subscribe failed!\n";
			echo "code:".$api->errorCode."\n";
			echo "msg :".$api->errorMessage."\n";
		} else {
			echo "added:   ".$vals['add_count']."\n";
			echo "updated: ".$vals['update_count']."\n";
			echo "errors:  ".$vals['error_count']."\n";
			foreach($vals['errors'] as $val){
				echo $val['email_address']. " failed\n";
				echo "code:".$val['code']."\n";
				echo "msg :".$val['message']."\n";
			}
		}*/
		$appId = apply_filters('asg_get_settings' , 'mail_chimp_api');
		$listId = apply_filters('asg_get_settings' , 'list_id');
		$listId = isset($_POST['asg_list_id']) ? $_POST['asg_list_id'] : $listId;
		$mailChimp = new ASG_MailChimp($appId);
		$batch = array();
		foreach ($memberInfo as $key => $member) {
			$batch[] = array(
				        'email' => array('email' => $member['EMAIL']),
				        'email_type' => 'html',
				        'merge_vars' => array(
				        					'FNAME' => $member['FNAME'],
				        					'LNAME' => $member['LNAME'],
				        				)
				    );
		}
		
		$result1 = $mailChimp->call('lists/batch-subscribe', array(
								'id'                => $listId,
								'batch'             => $batch,
								'double_optin'      => false,
								'update_existing'   => true,
								'replace_interests' => true
								)
							);
		if($result1['error_count'] <= 0 ) {
			$message = __(" Members Subscribed Successfully \n" , TEXT_DOMAIN);
			ASG_Member_Model::updateMemberMailchimpStatus($memberIds);
			update_option('asg-crud-message' , $message);
			update_option('asg-error-message' , 0);

		} else {
			$errors = '';
			foreach ($result1['errors'] as $key => $error) {
				$errors .= $key+1 . ') ' . $error['error'] . "\n";
			}
			$message = __("Batch Subscription Failed \n Erors: \n". $errors . "" , TEXT_DOMAIN);
			update_option('asg-crud-message' , $message);
			update_option('asg-error-message' , 1);
			//echo get_option('asg-error-message');
		}
		
		/*if ($api->errorCode){
		    echo "Batch Subscribe failed!\n";
			echo "code:".$api->errorCode."\n";
			echo "msg :".$api->errorMessage."\n";
		} else {
			echo "added:   ".$vals['add_count']."\n";
			echo "updated: ".$vals['update_count']."\n";
			echo "errors:  ".$vals['error_count']."\n";
			ASG_Member_Model::updateMemberMailchimpStatus($memberIds);
			foreach($vals['errors'] as $val){
				echo $val['email_address']. " failed\n";
				echo "code:".$val['code']."\n";
				echo "msg :".$val['message']."\n";
			}
		}*/
	}

	/**
	 *
	 *	This method is used to sync with mailchimp
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function syncMailchimp($data) {
	    $apiKey = 'db9da9f1d2b21d967f4067313c0f3d28-us12';
	    $listId = '83234643ed';

	    $memberId = md5(strtolower($data['email']));
	    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

	    $json = json_encode([
	        'email_address' => $data['email'],
	        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
	        'merge_fields'  => [
	            'FNAME'     => $data['firstname'],
	            'LNAME'     => $data['lastname']
	        ]
	    ]);
	    try {
		    $ch = curl_init($url);

		    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

		    $result = curl_exec($ch);
		    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch); 
		    return $result;
		} catch (Exception $e) {
			trigger_error('Exception Occured' . $e->getMessage());	
			return false;
		}
	    
	}

	/**
	 *
	 *	This method is used to generate membership card
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */	

	function generateMemberShipCard() { 
		$membershipCard = new ASG_Membership_Cards_Model();
		$reportType = isset($_POST['report']) ? $_POST['report'] : '';
		if($reportType == 0) {
			return $membershipCard->getMembersByJoinDate();
		} else {
			return $membershipCard->getMembersById();
		}
	}

}

