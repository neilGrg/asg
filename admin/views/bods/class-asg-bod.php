<?php

class ASG_BOD {

	private $_bodModel = array();
	private $_fields = array();
	private $_chapters = array();
	private $_bods = array();
	private $_url = array();
	private $_pages = '';

	function __construct() {
		global $data;
		$this->_url = (!empty($data) && isset($data['url'])) ? $data['url'] : array();
		if(
			($_REQUEST['operation'] == 'delete' || $_REQUEST['action'] == 'bulk_delete') && $data['success'] ||
			(ASG_DASHBOARD::$limitUpdate)) {
			ASG_DASHBOARD::$limitUpdate = false;
			ASG_Helper::asgRedirect($this->_url);
		}
		$this->_chapters = (!empty($data) && isset($data['chapters'])) ? $data['chapters'] : array();
		$this->_members = (!empty($data) && isset($data['members'])) ? $data['members'] : array();
		$this->_bods = (!empty($data) && isset($data['bods'])) ? $data['bods'] : array();
		$this->_pages = (!empty($data) && isset($data['pages'])) ? $data['pages'] : array();
		$this->_fieldsForChapters();
		$this->_bodDisplay();
	}

	private function _bodDisplay() { 
		?>
		    <!-- Create a header in the default WordPress 'wrap' container -->
		    <?php echo apply_filters('asg_crud_message' , ''); ?>
		    <?php 
				echo apply_filters(
						'asg_limit_setting' ,
						'asg-bod-limit' , 
						get_option('asg-bod-limit')
					);
			?>		
	    	<h2><?php echo __('Board Of Director Lists' , TEXT_DOMAIN) ?></h2>
	    	<form method="POST" id="bulk-action" >
	    	<div class="alignleft actions bulkactions">
				<label for="bulk-action-selector-top" class="screen-reader-text"><?php echo __('Select bulk action' , TEXT_DOMAIN); ?></label>
				<select name="action" id="bulk-action-selector-top">
					<option value="-1"><?php echo __('Bulk Actions' , TEXT_DOMAIN); ?></option>
					<option value="bulk_delete"><?php echo __('Delete' , TEXT_DOMAIN); ?></option>
				</select>
				<input type="button" id="doaction" class="button action" value="Apply">
			</div>
	     	<p class="search-box">
				<label class="screen-reader-text" for="chapter-search-input"><?php echo __('Board Of Director Lists' , TEXT_DOMAIN ) ?></label>
				<div id="add_title"><a href="<?php echo $this->_url . "&operation=addTB_inline?width=600&height=550&inlineId=my-content-id"; ?> " class="page-title-action thickbox"  onclick="f.setBodActionType(this);" data-operation = "add" onclick="f.setBodActionType(this);"><?php echo __('Add' , TEXT_DOMAIN); ?></a></div>
			</p>
			<table class="wp-list-table widefat fixed striped pages">
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo __('MEMBERID' , TEXT_DOMAIN ); ?></th>
					<th><?php echo __('MEMBER' , TEXT_DOMAIN ); ?></th>
					<th><?php echo __('CHAPTER' , TEXT_DOMAIN); ?></th>
					<th><?php echo __('EDIT' , TEXT_DOMAIN); ?></th>
					<th><?php echo __('DELETE' , TEXT_DOMAIN); ?></th>
				</thead>
				<tbody>
					<?php foreach ($this->_bods as $key => $bod) { ?>
						<tr>
							<th scope="row" class="check-column">		
								<label class="screen-reader-text" for="cb-select-2"><?php echo __('Select Member' , TEXT_DOMAIN); ?></label>
								<input id="cb-select-2" type="checkbox" name="delete_bod[]" value="<?php echo $bod['id']; ?>">
							</th>
							<td><?php echo $bod['member_id']; ?></td>
							<td><?php echo $bod['name']; ?></td>
							<td><?php echo $bod['chapter']; ?></td>
							<td><a href="<?php echo $this->_url . "#TB_inline?width=600&height=550&inlineId=my-content-id"; ?>" class="page-chapter-action thickbox" data-operation = "edit"  data-id="<?php echo $bod['id'] ?>" class="page-bod-action thickbox" onclick="f.setBodActionType(this);"><i class="fa fa-pencil-square-o"></i></a></td>
							<td><a href="<?php echo $this->_url . "&operation=delete&bod_id=". $bod['id']  ?>" onclick ="return confirm('Are you sure you want to Delete?')"><i class="fa fa-trash-o"></i></a></td>
							<td></td>
						</tr>
					<?php } ?>
				</tbody>
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo __('MEMBERID' , TEXT_DOMAIN ); ?></th>
					<th><?php echo __('MEMBER' , TEXT_DOMAIN ); ?></th>
					<th><?php echo __('CHAPTER' , TEXT_DOMAIN); ?></th>
					<th><?php echo __('EDIT' , TEXT_DOMAIN); ?></th>
					<th><?php echo __('DELETE' , TEXT_DOMAIN); ?></th>
				</thead>
				</table>
			</form>
			<div>
				<?php 
					$pagination = apply_filters('asg_display_pagination' , 'Chapter' , $this->_pages ); 
				  	if(!empty($pagination)):
					?>
						<div id="asg-pagination">
						 <?php echo $pagination; ?>
						</div>
				<?php endif; ?>
			</div>	
						
	     	<!-- Add Section -->
			<?php add_thickbox(); ?>
			<div id="my-content-id" style="display:none;">
				<h2><?php echo __('BOD Information' , TEXT_DOMAIN ); ?></h2>
				<form action="" method="post" onsubmit="event.preventDefault();" id="bod-form" >
					<table class="form-table" data-parsley-validate >
						<tbody>
						<?php foreach ($this->_fields as $field):
								$validators  = !empty($field['validator']) ? $field['validator'] : '';
								$event = !empty($field['onChange']) ? 'onchange = "' . $field['onChange'] . '"' : '';
								switch ($field['type']) {
									case 'text':
										echo '<tr class="user-login-wrap">
												<th><label for="">' . __( $field['label'] ) . '</label></th>
												<td><input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="" class="regular-text" '. $validators .'></td>
											  </tr>';
										break;
									case 'select':
									    $html = '<tr class="-user-login-wrap">
												<th><label for="">' . __( $field['label'] ) . '</label></th>
												<td><select class="president" name="'. $field['model'] . '[' . $field['name'] . ']"  id="'. $field['id'] .'" ' . $event . '>';
												 foreach ($field['value'] as $key => $values) {
												 	if(is_array($values)) {
												 		 foreach ($values as $key => $value) {
												   			$html .= '<option value=' . $key . '>' . $value . '</option>';
												 		}
												 	} else {
												 		$html .= '<option value=' . $key . '>' . $values . '</option>';
												 	}

												 }
										$html .= '</select></td>
											</tr>';
										echo $html;
										break;
									
									default:
										echo '<tr class="user-login-wrap">
												<th><label for="">' . __( $field['label'] ) . '</label></th>
												<td><input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="" class="regular-text" '. $validators .'></td>
											  </tr>';
										break;
								}
							endforeach;
						?>
						
						</tbody>
					</table>
					<input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo wp_create_nonce( 'asg_action_nonce'); ?>" />
					<input type="hidden" name="operation" value="" id="operation" />
					<input type="hidden" name="bod_id" value="" id="bod-id" />
					<input type="submit" name="save" value="<?php echo __('Save', TEXT_DOMAIN ); ?>" class="button button-primary"   onclick = "f.saveBod(this)" />
				</form>
			</div>
		<?php
	} // end sandbox_theme_display

	/**
	* array(
	*	'label' => 'MEMBERID',
	*	'type'	=> 'text',
	* 	'name'	=> 'member_id',
	*	'model' => 'bod',
	*   'id'    => 'member_id',
	*	'validator'    => ''
	*	),
	**/
	protected function _fieldsForChapters() {
		$this->_fields = array(
								array(
									'label' => 'MEMBER',
									'type'	=> 'select',
									'name'	=> 'member_id',
									'model' => 'bod',
									'id'    => 'member_id',
									'validator' => '',
									'onChange' => 'f.getMemberInfoById(this);',
									'value' => $this->_members
								),
								array(
									'label' => 'CHAPTERS',
									'type'	=> 'select',
									'name'	=> 'chapter_id',
									'model' => 'bod',
									'id'    => 'chapter_id',
									'validator' => '',
									'onChange' => '',
									'value' => $this->_chapters
								),
								array(
									'label' => 'ADDRESS',
									'type'	=> 'text',
									'name'	=> 'address',
									'model' => 'bod',
									'id'    => 'address',
									'validator' => 'required data-parsley-type="alphanum"'
								),
								array(
									'label' => 'CITY',
									'type'	=> 'text',
									'name'	=> 'city',
									'model' => 'bod',
									'id'    => 'city',
									'validator'    => 'required data-parsley-type="alphanum"'
								),
								array(
									'label' => 'STATE',
									'type'	=> 'text',
									'name'	=> 'state',
									'model' => 'bod',
									'id'    => 'state',
									'validator'    => 'required data-parsley-type="alphanum"'
								),
								array(
									'label' => 'ZIP',
									'type'	=> 'text',
									'name'	=> 'zip',
									'model' => 'bod',
									'id'    => 'zip',
									'validator'    => 'required data-parsley-type="number"'
								),
								array(
									'label' => 'TELEPHONE',
									'type'	=> 'text',
									'name'	=> 'phone',
									'model' => 'bod',
									'id'    => 'phone',
									'validator'    => 'required data-parsley-type="number"'
								)
						);
	}


	
}

// global variable file handler
$GLOBALS['bod']  = new ASG_BOD();
