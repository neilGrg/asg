<!-- Create a header in the default WordPress 'wrap' container -->
<?php 

	$details['titles'] = array(
					 'chapter' => __('CHAPTERS' , TEXT_DOMAIN),
					 'bod' 	   => __('BOD' , TEXT_DOMAIN),
					 'member'  => __('MEMBERS' , TEXT_DOMAIN),
					 'report'  => __('REPORTS' , TEXT_DOMAIN)
				); 
	$details['info'] = array(
				'chapter' => array(
									'page' => 'asg-admin-sub-page3',
									'icon' => $chapterIconUrl
								),
				'bod' => array(
									'page' => 'asg-admin-sub-page6',
									'icon' => $bodIconUrl
								),
				'member' => array(
									'page' => 'asg-admin-sub-page2',
									'icon' => $memberIconUrl
								),
				'report' => array(
									'page' => 'asg-admin-sub-page7',
									'icon' => $reportIconUrl
								)
			);
class ASG_Dashboar_Layout {

	private $_titles = array();
	private $_info = array();

	function __construct($details) {
		$this->_titles = $details['titles'];
		$this->_info = $details['info'];
		$this->displayDashboard();
	}

	function displayDashboard() {
    ?>
        <div id="poststuff">
	    <!-- #post-body .metabox-holder goes here -->
			<div id="post-body" class="metabox-holder columns-2">
		    <!-- meta box containers here -->
		    	<form  method="get" action="">
			        <?php wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false ); ?>
			        <?php wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false ); ?>
			        <?php foreach ($this->_titles as $key => $title) {
			        			$func = $key .'Display';
			        			$id = $key . '-asg';
								add_meta_box( 
							    $id,
							    $title,
							    array($this,$func),
							    'asg-admin-sub-page1'
							    );
							    
						}
					?>
	                <?php do_meta_boxes('asg-admin-sub-page1','advanced',null);?>
			    </form>
			</div>
		</div>
	    
	    <script type="text/javascript">
		jQuery(document).ready(function($){$(".if-js-closed").removeClass("if-js-closed").addClass("closed");
				       
					postboxes.add_postbox_toggles( 'asg-admin-sub-page1');
					});
	    </script>
	<?php    
	}

	function chapterDisplay($key) {
		$key = 'chapter';
		$this->_layout($key);
	}	

	function _layout($key) {
		if(!array_key_exists($key, $this->_info)) 
			return false;
		$details = $this->_info[$key];
		?>
		 <a href="<?php echo $data['adminUrl'] . "?page=" . $details['page']?>"/>
		    <div class='inside'>
		       <img src="<?php echo $details['icon'] ?>" />
			</div>
		</a>
		<?php
	}


	function bodDisplay($key) {
		$key = 'bod';
		$this->_layout($key);
	
	}

	function memberDisplay($key) {
		$key = 'member';
		$this->_layout($key);
	
	}

	function reportDisplay() {
		$key = 'chapter';
		$this->_layout($key);
	
	}

}

new ASG_Dashboar_Layout($details);
