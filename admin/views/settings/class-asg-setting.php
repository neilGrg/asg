<?php
class ASG_Setting {
	private $_settings = array();
	private $_fields = array();
	private $_tabContents = array();
	private $_presidents = array();
	private $_errors = array();
	private $_postedFields = array();	
	private $_id = '';
	private $_url = '';
	private $_activeTab = 'tab1';
	private $_countries = array();
	private $_states = array();
	private $_cities = array();
	private $_geoFields = array();
	public $tabSettings = array();
	public $defaultValues = array();
	public $geographyMenus = array();

	function __construct($data = array()) {
		$this->_url = (!empty($data) && isset($data['url'])) ? $data['url'] : array();
		$this->_settings = (!empty($data) && isset($data['settings'])) ? $data['settings'] : array();
		$this->_pages = (!empty($data) && isset($data['pages'])) ? (int) $data['pages'] : '';
		$this->_initDefaultValues();
		add_filter('asg_setting_feilds' , array( $this , 'setGeneralFormStructre'), 10 , 2 );
		add_filter('asg_geography_fields' , array( $this , 'setGeographyFormStructre'),10 , 2);
		add_filter('asg_mailchimp_fields' , array( $this , 'setMailChimpFormStructre'),10 , 2);
		add_filter('asg_api_fields' , array( $this , 'setApiFormStructre'), 10 , 2 );
		add_action('asg_setting_form_display' , array( $this , 'settingFormDisplay'));
		add_action('asg_setting_geoForm_display', array( $this , 'settingGeoFormDisplay'));
		add_filter('asg_mailchimp_display' , array( $this , 'displayMailchimp'));
		add_action('asg_geo_list_display', array( $this , 'displayGeoLists'));
		add_filter('asg_setting_tab_display' , array( $this , '_settingTabDisplay'));
		
	}


	private function _initDefaultValues() {
		$this->_countries = apply_filters('asg_get_countries',array());
 		$this->_states = apply_filters('asg_get_states',array());
 		$this->_cities = apply_filters('asg_get_cities',array());
 		$this->defaultValues  = array(
		 							'menus' => array(
		 										__( 'List Of Settings' , TEXT_DOMAIN ),
		 										__( 'Change Passwords' , TEXT_DOMAIN ),
		 									),
		 							'title' => __( 'Setting List' , TEXT_DOMAIN ),
		 							'member_id' => __( 'Setting ID' , TEXT_DOMAIN ),
		 							'name' => __( 'Name' , TEXT_DOMAIN ),
		 							'fname' => __( 'First Name' , TEXT_DOMAIN ),
		 							'lname' => __( 'Last Name' , TEXT_DOMAIN ),
		 							'chapter' => __( 'Setting' , TEXT_DOMAIN ),
		 							'operation' => __( 'Operations' , TEXT_DOMAIN ),
		 							'add' => __( 'Add Setting' , TEXT_DOMAIN ),
		 							'save' => __( ' Save Setting' , TEXT_DOMAIN ),
		 							'cancel' => __( ' Cancel' , TEXT_DOMAIN ),
		 							'edit' => __( 'Edit' , TEXT_DOMAIN ),
		 							'delete' => __( 'Delete' , TEXT_DOMAIN ),
		 							'search' => __( 'Search' , TEXT_DOMAIN ),
		 							'clear' => __( 'Clear' , TEXT_DOMAIN ),
		 						);
 		$this->defaultValues = apply_filters('asg_default_setting', $this->defaultValues);
 		$this->geographyMenus = array(
 										array(
 											'title' => __('Country' , TEXT_DOMAIN)
 										),
 										array(
 											'title' => __('State' , TEXT_DOMAIN)
 										),
 										array(
 											'title' => __('City' , TEXT_DOMAIN)
 										)
 									);
 		$this->geographyMenus = apply_filters('asg_geography_menus', $this->geographyMenus);
 		$this->tabSettings =   array(
 								 	'general' => array(
							 				'menu' => __( 'General' , TEXT_DOMAIN ),
							 				'title' =>__('General Setting Information' , TEXT_DOMAIN ),
							 				'hook_to_set_form' => 'asg_setting_feilds',
							 				'submenu' => ''
							 			),
 								 	'geography' => array(
							 				'menu' => __( 'Geography' , TEXT_DOMAIN ),
							 				'title' =>__('Geography Setting Information' , TEXT_DOMAIN ),
							 				'hook_to_set_form' => 'asg_geography_fields',
							 				'submenu' => $this->geographyMenus
							 			),
 								 	'mailchimp' => array(
							 				'menu' => __( 'Mail Chimp' , TEXT_DOMAIN ),
							 				'title' =>__('Mail Chimp' , TEXT_DOMAIN ),
							 				'hook_to_set_form' => 'asg_mailchimp_fields',
							 				'submenu' => ''
							 			)
 								);
 		$this->tabSettings = apply_filters('asg_tab_menus', $this->tabSettings);
 		
 	} 	

 	private function _setCountryGeoFields() {
 		$this->_geoFields = array(
								array(
									'label' => __('Country' , TEXT_DOMAIN),
									'value' => $this->_countries,
									'id' => 'country'
								)						
							);
 	}

 	private function _setStateGeoFields() {
 		$this->_geoFields = array(
								array(
									'label' => __('State' , TEXT_DOMAIN),
									'value' => $this->_states,
									'id' => 'state'
								)
							);
 	}

 	private function _setCityGeoFields() {
 		$this->_geoFields = array(
								array(
									'label' => __('City' , TEXT_DOMAIN),
									'value' => $this->_cities,
									'id' => 'city'
								)
							);
 	}

	/**
	 *
	 *	This method is used to build form for chapter section
	 * 	@access private
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function setGeneralFormStructre($structure = array() , $data) {  
		$settings = isset($data['settings']) ? $data['settings'] : $_REQUEST['setting'];
		$defaults = array(
							array(
								'label' => 'LIMIT',
								'type'	=> 'text',
								'name'	=> 'limit',
								'model' => 'setting',
								'id'    => 'limit',
								'class' => '',
								'validator'    => '',
								'message'    => 'Set the value for limit number of rows for each page',
								'value' => ASG_Helper::giveMeValueIfSet($settings,'limit')
							),
							array(
								'label' => 'CHAPTER ICON',
								'type'	=> 'button',
								'name'	=> 'chapter_icon',
								'model' => 'setting',
								'id'    => 'upload_image',
								'validator' => '',
								'message' => 'Set icon for chapter',
								'imgClass' => 'chapter_icon',
								'btn_value' => 'Upload Image',
								'value' => ASG_Helper::giveMeValueIfSet($settings,'chapter_icon')
							),
							array(
								'label' => 'BOD ICON',
								'type'	=> 'button',
								'name'	=> 'bod_icon',
								'model' => 'setting',
								'id'    => 'upload_image',
								'validator' => '',
								'message' => 'Set icon for bod',
								'imgClass' => 'bod_icon',
								'btn_value' => 'Upload Image',
								'value' => ASG_Helper::giveMeValueIfSet($settings,'bod_icon')
							),
							array(
								'label' => 'MEMBER ICON',
								'type'	=> 'button',
								'name'	=> 'member_icon',
								'model' => 'setting',
								'id'    => 'upload_image',
								'validator' => '',
								'message' => 'Set icon for member',
								'imgClass' => 'member_icon',
								'btn_value' => 'Upload Image',
								'value' => ASG_Helper::giveMeValueIfSet($settings,'member_icon')
							),
							array(
								'label' => 'REPORTS ICON',
								'type'	=> 'button',
								'name'	=> 'report_icon',
								'model' => 'setting',
								'id'    => 'upload_image',
								'validator' => '',
								'message' => 'Set icon for report',
								'imgClass' => 'report_icon',
								'btn_value' => 'Upload Image',
								'value' => ASG_Helper::giveMeValueIfSet($settings,'report_icon')
							),

						);
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
	}

	/**
	 *
	 *	This method is used to build form for chapter section
	 * 	@access private
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function setApiFormStructre($structure = array() , $data) {  
		$api = isset($data['api']) ? $data['api'] : $_REQUEST['api'];
		$defaults = array(
							array(
								'label' => 'Api Key',
								'type'	=> 'text',
								'name'	=> 'api_key',
								'model' => 'api',
								'id'    => 'api_key',
								'class' => '',
								'validator'    => '',
								'value' => ASG_Helper::giveMeValueIfSet($settings,'limit')
							),
						);
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
	}

	

	/**
	 *
	 *	This method is used to build form for chapter section
	 * 	@access private
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function setGeographyFormStructre($structure = array() , $data) {  
		$settings = isset($data['geography']) ? $data['geography'] : $_REQUEST['geography'];
		$section =  isset($_REQUEST['section']) ? $_REQUEST['section'] : 'country';
		/*$defaults = array(
						array(
								array(
										'label' => __('COUNTRY' , TEXT_DOMAIN),
										'type'	=> 'select',
										'name'	=> 'name',
										'id'    => 'country',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => $this->_countries,
									),
								array(
										'label' => __('EDIT' , TEXT_DOMAIN),
										'type'	=> 'button',
										'name'	=> __('EDIT' , TEXT_DOMAIN),
										'id'    => 'country',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => __('EDIT' , TEXT_DOMAIN),
										'click' => ''
								),
								array(
										'label' => __('COUNTRY' , TEXT_DOMAIN),
										'type'	=> 'text',
										'name'	=> 'name',
										'id'    => 'country',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => ASG_Helper::giveMeValueIfSet($settings,'country')
								),
								array(
										'label' => __('SAVE' , TEXT_DOMAIN),
										'type'	=> 'button',
										'name'	=> __('SAVE' , TEXT_DOMAIN),
										'id'    => 'country',
										'class' => 'save',
										'validator'    => '',
										'message'    => '',
										'click' => '',
										'value' => __('SAVE' , TEXT_DOMAIN)
								),
						),
						array(
								array(
										'label' => __('STATE' , TEXT_DOMAIN),
										'type'	=> 'select',
										'name'	=> 'name',
										'id'    => 'state',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => $this->_states,
									),
								array(
										'label' => __('EDIT' , TEXT_DOMAIN),
										'type'	=> 'button',
										'name'	=> __('EDIT' , TEXT_DOMAIN),
										'id'    => 'state',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'click' => '',
										'value' => __('EDIT' , TEXT_DOMAIN)
								),
								array(
										'label' => __('STATE' , TEXT_DOMAIN),
										'type'	=> 'text',
										'name'	=> 'name',
										'id'    => 'state',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => ASG_Helper::giveMeValueIfSet($settings,'state')
								),
								array(
										'label' => __('SAVE' , TEXT_DOMAIN),
										'type'	=> 'button',
										'name'	=> __('SAVE' , TEXT_DOMAIN),
										'id'    => 'state',
										'class' => 'save',
										'validator'    => '',
										'message'    => '',
										'value' => __('SAVE' , TEXT_DOMAIN),
										'click' => ''
								),
							),
							array(
								array(
										'label' => __('CITY' , TEXT_DOMAIN),
										'type'	=> 'select',
										'name'	=> 'name',
										'id'    => 'city',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => $this->_cities,
								),
								array(
										'label' => __('EDIT' , TEXT_DOMAIN),
										'type'	=> 'button',
										'name'	=> __('EDIT' , TEXT_DOMAIN),
										'id'    => 'city',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => __('EDIT' , TEXT_DOMAIN),
										'click' => ''
								),
								array(
										'label' => __('CITY' , TEXT_DOMAIN),
										'type'	=> 'text',
										'name'	=> 'name',
										'id'    => 'city',
										'class' => '',
										'validator'    => '',
										'message'    => '',
										'value' => ASG_Helper::giveMeValueIfSet($settings,'city')
								),
								array(
										'label' => __('SAVE' , TEXT_DOMAIN),
										'type'	=> 'button',
										'name'	=> __('SAVE' , TEXT_DOMAIN),
										'id'    => 'city',
										'class' => 'save',
										'validator'    => '',
										'message'    => '',
										'value' => __('SAVE' , TEXT_DOMAIN),
										'click' => ''
								),
						)
					);*/
		switch ($section) {
			case 'country':
				$formStructure = array(
							array(
								'label' => __('COUNTRY' , TEXT_DOMAIN),
								'type'	=> 'text',
								'name'	=> 'name',
								'id'    => 'name',
								'class' => '',
								'validator'    => '',
								'message'    => '',
								'value' => '',
								'selected_value'=> ASG_Helper::giveMeValueIfSet($settings,'country')
							),
							array(
								'label' => __('ABBREVIATION' , TEXT_DOMAIN),
								'type'	=> 'text',
								'name'	=> 'sort_name',
								'id'    => 'sort_name',
								'class' => '',
								'validator'    => '',
								'message'    => '',
								'value' => ASG_Helper::giveMeValueIfSet($settings,'sort_name')
							)
						);
				$this->_setCountryGeoFields();
				break;
			case 'state':
				$formStructure = array(
								array(
									'label' => __('COUNTRY' , TEXT_DOMAIN),
									'type'	=> 'select',
									'name'	=> 'country_id',
									'id'    => 'country_id',
									'class' => 'country-id',
									'validator'    => '',
									'message'    => '',
									'value' => $this->_countries,
									'selected_value'=> ASG_Helper::giveMeValueIfSet($settings,'country')
								),
								array(
									'label' => __('STATE' , TEXT_DOMAIN),
									'type'	=> 'text',
									'name'	=> 'name',
									'id'    => 'name',
									'class' => 'state-id',
									'validator'    => '',
									'message'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($settings,'state'),
								),
								array(
									'label' => __('ABBREVIATION' , TEXT_DOMAIN),
									'type'	=> 'text',
									'name'	=> 'sort_name',
									'id'    => 'sort_name',
									'class' => '',
									'validator'    => '',
									'message'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($settings,'sort_name')
								)
							);
				$this->_setStateGeoFields();
				break;
			case 'city':
				$formStructure = array(
								array(
									'label' => __('COUNTRY' , TEXT_DOMAIN),
									'type'	=> 'select',
									'name'	=> 'country_id',
									'id'    => 'country_id',
									'class' => 'country-id dyn-state',
									'validator'    => '',
									'message'    => '',
									'value' => $this->_countries,
									'selected_value'=> ASG_Helper::giveMeValueIfSet($settings,'country')
								),
								array(
									'label' => __('STATE' , TEXT_DOMAIN),
									'type'	=> 'select',
									'name'	=> 'state_id',
									'id'    => 'state_id',
									'class' => 'state-id',
									'validator'    => '',
									'message'    => '',
									'value' => $this->_states,
									'wrapper' => true,
									'wrapper_id' => 'asg-state',
									'selected_value' => ASG_Helper::giveMeValueIfSet($settings,'state'),
								),
								array(
									'label' => __('CITY' , TEXT_DOMAIN),
									'type'	=> 'text',
									'name'	=> 'name',
									'id'    => 'name',
									'class' => '',
									'validator'    => '',
									'message'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($settings,'limit')
								),
								array(
									'label' => __('ABBREVIATION' , TEXT_DOMAIN),
									'type'	=> 'text',
									'name'	=> 'sort_name',
									'id'    => 'sort_name',
									'class' => '',
									'validator'    => '',
									'message'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($settings,'sort_name')
								)
							);
				$this->_setCityGeoFields();
				break;
			default:
				# code...
				break;
		}
		$defaults = $formStructure;
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
	}

	/**
	 *
	 *	This method is used to build form for mailchimp Setting
	 * 	@access private
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	/*
	backup-code
	array(
			'label' => 'List Id 1',
			'type'	=> 'text',
			'name'	=> 'list_id_1',
			'model' => 'setting',
			'id'    => 'list-id-1',
			'validator' => '',
			'message' =>  __('Set List Id 1 to get subscriber lists', TEXT_DOMAIN),
			'value' => ASG_Helper::giveMeValueIfSet($settings,'list_id_1')
		),
		array(
			'label' => 'List Id 2',
			'type'	=> 'text',
			'name'	=> 'list_id_2',
			'model' => 'setting',
			'id'    => 'list-id-2',
			'validator' => '',
			'message' =>  __('Set List Id 2 to get subscriber lists', TEXT_DOMAIN),
			'value' => ASG_Helper::giveMeValueIfSet($settings,'list_id_2')
		),
		array(
			'label' => 'List Id 3',
			'type'	=> 'text',
			'name'	=> 'list_id_3',
			'model' => 'setting',
			'id'    => 'list-id-3',
			'validator' => '',
			'message' =>  __('Set List Id 3 to get subscriber lists', TEXT_DOMAIN),
			'value' => ASG_Helper::giveMeValueIfSet($settings,'list_id_3')
		)
	*/

	function setMailchimpFormStructre($structure = array() , $data) {  
		$settings = isset($data['settings']) ? $data['settings'] : $_REQUEST['setting'];
		$defaults = array(
							array(
								'label' => 'API KEY',
								'type'	=> 'text',
								'name'	=> 'mail_chimp_api',
								'model' => 'setting',
								'id'    => 'mail-chimp-api',
								'class' => '',
								'validator'    => '',
								'message'    => __('Set API Key for MailChimp' , TEXT_DOMAIN),
								'value' => ASG_Helper::giveMeValueIfSet($settings,'mail_chimp_api')
							)							
						);
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
	}

	function displayGeoLists() {
		?>
		<table class="setting-table form-table" data-parsley-validate >
			<tbody>
				<?php foreach ($this->_geoFields as $key => $field):?>
					<td><label for="<?php echo $field['label'] ; ?>"> <?php echo $field['label'] ; ?></label></td>
	  				<td>
	  					<select id="<?php echo $field['id'];?>">
	  						<?php foreach ( $field['value'] as $k => $value) { ?>
	  							<option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
	  						<?php } ?>
	  					</select>
	  				</td>
	  			<?php endforeach; ?>
	  			<td id="asg-geo-add" data-action="<?php echo __('Add' , TEXT_DOMAIN); ?>"><button><?php echo __('Add' , TEXT_DOMAIN); ?></button></td>
  				<td id="asg-geo-edit" data-action="<?php echo __('Edit' , TEXT_DOMAIN); ?>"><button><?php echo __('Edit' , TEXT_DOMAIN); ?></button></td>
  				<td id="asg-geo-del" ><button ><?php echo __('Delete' , TEXT_DOMAIN); ?></button></td>
			</tbody>
		</table>
		<?php
	}

	/**
	 *
	 *	This method is used to build form for geo section
	 * 	@access public
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function settingGeoFormDisplay() {  
		?>
		<table class="setting-table form-table"  >
			<tbody>
				<?php foreach ($this->_fields as $key => $value):
						$html = '<tr data-tid="' . $key . '">';
						foreach ($value as $k => $field) {
							switch ($field['type']) {
								case 'text':
									$html .= '<td><label for="">' . __( $field['label'] ) . '</label></td>
											  <td><input class="'. $field['class'] .'" type="' . $field['type'] . '" name="' . $field['name'] . '" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
										 	';
									break;
								case 'button':
									$html .= '<td>
												<input class="'. $field['class'] .'" type="' . $field['type'] . '" name="'  . $field['name'] . '" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .' onclick="' . $field['click'] . '"></td>';
									break;
								case 'select':
								    $html .= '<td><label for="">' . __( $field['label'] ) . '</label></td><td>';
								    $html .=  isset($field['wrapper']) && $field['wrapper'] ? '<div id="'. $field['wrapper_id'] .'">' : '';
									$html .= '<select class="'. $field['class'] . '" name="'. $field['name'] . '"  id="' . $field['id'] . '">';
											 foreach ($field['value'] as $key => $v) {
											   $selected = ($key == $field['selected_value']) ? 'selected="selected"' : '';
											   $html .= '<option value= "' . $v['id'] . '"'. $selected . '>' . $v['name'] . '</option>';
											 }
									$html .= '</select>';
									$html .=  isset($field['wrapper']) && $field['wrapper'] ? '</div>' : '';
									$html .= '</td>';
									break;
								
								default:
									$html .= '<td><label for="">' . __( $field['label'] ) . '</label></td>
											  <td><input class="'. $field['class'] .'" type="' . $field['type'] . '" name="' . $field['name'] . '" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
										 	';
									break;
							}
						}
						$html .= '</tr>';
						echo $html;
					?>
				<?php endforeach; ?>
			<tbody>
		</table>
	<div><!-- /.wrap -->
	<?php
	}

	/**
	 *
	 *	This method is used to build form for all settings
	 * 	@access public
	 *  @param  $id
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */
	
	function settingFormDisplay( $id = '' ) { 
		$style = $_REQUEST['tab'] == 'geography' ? 'style="display:none"' : '';
		$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : 'country';
		?>
			<form method="post" <?php echo $style; ?>>
				<table class="setting-table form-table" data-parsley-validate >
							<tbody>
							<?php foreach ($this->_fields as $field):
									$validators  = !empty($field['validator']) ? $field['validator'] : '';
									$error = ASG_Helper::getErrors( $this->_errors , $field['name']);
									$html  = '';
									switch ($field['type']) {
										case 'text':
											$html .= '<tr>
													<td><label for="">' . __( $field['label'] ) . '</label></td>
													<td><input class="'. $field['class'] .'" type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
												 	<td><p>' . $field['message'] . '</p></td>
												 	';
											break;
										case 'button':
											$html .= '<tr>
													<td><label for="">' . __( $field['label'] ) . '</label></td>
													<td>.
														<input class="'. $field['class'] .'" type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="' . $field['btn_value'] . '" class="regular-text" '. $validators .'>
														<input class="hidden" type="hidden" name="' . $field['model'] . '[' . $field['name'] . ']"  value="' . $field['value'] . '"/>
													</td>
												 	<td><p>' . $field['message'] . '</p></td>
												 	<td><img class= "' . $field['imgClass'] . '" src="' . $field['value'] . '" width="150" height="110"></td>';
											break;
										case 'select':
										    $html .= '<tr><td><label for="">' . __( $field['label'] ) . '</label></td><td>';
										    $html .=  isset($field['wrapper']) && $field['wrapper'] ? '<div id="'. $field['wrapper_id'] .'">' : '';
										    $html .= '<select class="'. $field['class'] . '" name="'. $field['model'] . '[' . $field['name'] . ']"  id="' . $field['id'] . '">';
													 foreach ($field['value'] as $key => $selectVal) {
													 	$key = $field['insert_not_key'] == true ? $selectVal['name'] : $selectVal['id'] ;
													   	$selected = ($key == $field['selected_value']) ? 'selected="selected"' : '';
													   	$html .= '<option value= "' . $key . '" '. $selected . '>' . $selectVal['name'] . '</option>';
													 }
											$html .= '</select>';
											$html .=  isset($field['wrapper']) && $field['wrapper'] ? '</div>' : '';
											$html .= '</td><td><p>' . $field['message'] . '</p></td>';
											
											break;
										
										default:
											$html .= '<tr>
													<td><label for="">' . __( $field['label'] ) . '</label></td>
													<td><input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
												  <td><p>' . $field['message'] . '</p></td>';
											break;
									}
									$html .= !empty($error)  ? '<td><p class="alert alert-danger">'. $error .'</p></td></tr>' : '</tr>';
									echo $html;
								endforeach;
							?>
							</tbody>
				</table>
				<input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo wp_create_nonce( 'asg_action_nonce'); ?>" />
				<input type="hidden" name="update_setting" value="<?php echo $this->_activeTab; ?>" />
				<input type="hidden" id="section" name="section" value="<?php echo $section; ?>" />
				<input type="hidden" name="action" value="" id="action" />
				<input type="submit" name="save_setting" id = "save-setting" value="<?php echo __('Save', TEXT_DOMAIN ); ?>" class="button button-primary"/>
			</form>
		</div><!-- /.wrap -->
		<?php
	}

	function _settingTabDisplay($data = array()) {
		echo apply_filters('asg_crud_message' , '');
		?>
		    <!-- Create a header in the default WordPress 'wrap' container -->
		    <div class="wrap asg-content">
		    	<div id="icon-themes" class="icon32"></div>
			        <?php 
			        	settings_errors(); 
			        	if( isset( $_GET[ 'tab' ] ) ) {
			                $this->_activeTab = $_GET[ 'tab' ];
			            } else {
			            	$this->_activeTab = 'general';
			            }
			        	$this->_currentTabDisplay($data);
			        ?>
		<?php
		} // end sandbox_theme_display

    /**
	 *
	 *	This function is used to display currently 
	 *  selected tab
	 *  @param $curTab
	 * 	@access private
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

    private function _currentTabDisplay($data = array()) { 
    	?>
    	 <h2 class="nav-tab-wrapper">
	        <?php foreach ($this->tabSettings as $key => $setting) { 
	        		
	        	?>
	        	<a href="?page=asg-admin-sub-page8&tab=<?php echo $key; ?>" class="nav-tab <?php echo $this->_activeTab ==  $key ? 'nav-tab-active' : ''; ?>"><?php echo  $setting['menu'];?></a>
	        <?php } ?>   
		</h2>
		<?php 
			$curTabInfo = $this->tabSettings[$this->_activeTab]; 
			$isGeoPage = $this->_activeTab == 'geography' ? true : false;
		?>
		    	<div id="<?php echo $this->_activeTab;?>">
		    		<h3><?php echo $curTabInfo['title'] ?></h3>
		    		<?php if(is_array($curTabInfo['submenu']) && count($curTabInfo['submenu']) > 0): ?>
			    		<ul class="subsubsub">
				  			<?php  foreach($curTabInfo['submenu'] as $key => $gmenu) { 
				  					$separator = $key == count($curTabInfo) ? '' : '|';
				  			?>
				  				<li>
				  					<a href="<?php echo $data['url'] . "&tab=" . $this->_activeTab ."&section=" . strtolower($gmenu['title']) ?>" class="current"><?php echo $gmenu['title'];?></a>
				  					<?php echo $separator; ?>
				  				</li>
				  			<?php } ?>
				  		</ul>
			  		<?php endif; ?>
		        	<?php 
		        		apply_filters($curTabInfo['hook_to_set_form'] , array() , $data);
		        		$isGeoPage ? apply_filters('asg_geo_list_display' , array()) : '';
		        		if($isGeoPage) {
		        			$title = __('Geo Form' , TEXT_DOMAIN);
		        			echo '<h2>'.$title.'</h2>';
		        		}
		        		apply_filters('asg_setting_form_display' , array());
					?>
				</div>
	<?php }


}
new ASG_Setting();



