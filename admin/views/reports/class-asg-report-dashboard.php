<?php

class ASG_Report_Dashboard_Layout {

	private $_titles = array();
	private $_info = array();

	function __construct($details) {
		$this->_titles = $details['titles'];
		$this->_info = $details['info'];
		$this->displayDashboard();
	}

	function displayDashboard() {
    ?>
        <div class="wrap asg-content" >
	        <div id="poststuff">
		    <!-- #post-body .metabox-holder goes here -->
				<div id="post-body" class="metabox-holder columns-2">
				    <!-- meta box containers here -->
				    <form  method="get" action="">
				        <?php wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false ); ?>
				        <?php wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false ); ?>
				        <?php foreach ($this->_titles as $key => $title) {
				        			$func = $key .'Display';
				        			$id = $key . '-asg';
									add_meta_box( 
								    $id,
								    $title,
								    array($this,$func),
								    'asg-admin-sub-page1'
								    );
								    
							}
						?>
	                <?php do_meta_boxes('asg-admin-sub-page1','advanced',null);?>
				    </form>
				</div>
			</div>
	   	</div>
	    <script type="text/javascript">
		jQuery(document).ready(function($){$(".if-js-closed").removeClass("if-js-closed").addClass("closed");
				       
					postboxes.add_postbox_toggles( 'asg-admin-sub-page1');
					});
	    </script>
	<?php    
	}


	function _layout($key) { 
		$key = trim($key);
		
		if(!array_key_exists($key, $this->_info)) 
			return false;
		$details = $this->_info[$key];
		?>
		 <a href="<?php echo $details['url'] ?>"/>
		    <div class='inside'>
		       <img src="<?php echo $details['icon'] ?>" />
			</div>
		</a>
		<?php
	}

	function feeDisplay($key) {
		$key = 'fee';
		$this->_layout($key);
	}	

	function monthlyDisplay($key) {
		$key = 'monthly';
		$this->_layout($key);
	
	}

	function cardDisplay($key) {
		$key = 'card';
		$this->_layout($key);
	
	}

	function printDisplay() {
		$key = 'print';
		$this->_layout($key);
	
	}

	function __call($method, $params) {
		$key = str_replace('Display', ' ', $method);
		$this->_layout($key);
	}

	/*function mailchimpDisplay() {
		$key = 'mailchimp';
		$this->_layout($key);
	
	}*/

	
}

