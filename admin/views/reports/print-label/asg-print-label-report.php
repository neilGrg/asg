<div id = "print-label-report">
	<div id="asg-download-opt">
		<a href="<?php echo $url . '&report=print_label&operation=csv';?>" >
			<i class="fa fa-table"></i>
			<?php //echo __('EXPORT TO CSV' , TEXT_DOMAIN); ?>
		</a>|
		<a href="<?php echo $url . '&report=print_label&operation=pdf'; ?>" >
			<?php //echo __('EXPORT TO PDF' , TEXT_DOMAIN); ?>
			<i class="fa fa-file-pdf-o"></i>
		</a>
	</div>
	<?php 
			$chunkReports = array_chunk($data, 3); 
		  	$html = '<table  style="width:100%" id="print-label-table">';
			  	foreach ($chunkReports as $key => $row):
				  	unset($titles);
					$titles = array();
					$html .= '<tr>';
				  	foreach ($row as $k => $col):
						array_push($titles , $col['title1'] , $col['title2']);
						$html .= sprintf('<td>
					  					<div>
											<h2>%s</h2> %s
											<p> %s</p>
											<p> %s </p>
											<p> %s, %s  %s </p>
										</div></td>',
										$col['chapter'],
										implode( ',' , $titles ),
										date('Y-m-d' , strtotime($col['expired_date'])),
										$col['name'],
										$col['address'],
										$col['city'],
										$col['state'],
										$col['work_phone']
							);
						$titles = [];
					endforeach;
					$html .= '</tr>';
				endforeach;
			$html .= '</table>';	
			update_option('report-html' , serialize($html));
			update_option('report-csv' , serialize($data));
			echo $html;
	?>
</div>


