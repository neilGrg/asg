<?php
class ASG_Reports {
	private $_defaultValues = array();
	private $_config = array();
	private $_fields = array();
	private $_presidents = array();
	private $_errors = array();
	private $_url = '';
	private $_chapters = array();
	private $_titles = array();
	private $_mailChimp = '';
	private $_path  = '';

	function __construct($data = array()) { 
		$this->_chapters = $this->_formatArrayForSelect(ASG_Chapter_Model::getAllChapters());
		$titleModel = new ASG_Title_Model();
		$this->_titles = $this->_formatArrayForSelect($titleModel->getTitles());
		$this->_path = ADMIN_DIR_PATH .  DS . 'views' . DS . 'reports';
		$this->_url = isset($_REQUEST['page']) ? ADMIN_URL .'?page=' . $_REQUEST['page'] : '';
		$this->_initDefaultValues();
		/*add_filter('setting_feilds' , array( $this , 'setFormStructre'), 10 , 2 );
		add_filter('setting_display' , array( $this , 'settingFormDisplay'));*/
		add_filter('report_dashboard' , array( $this , 'reportDashoard'));
		// filter for membership fee total report
		add_filter('membership_fee_total' , array( $this , 'displayMembershipFeeTotalReport'), 10 , 2 );
		// filter for monthly report
		add_filter('monthly_report' , array( $this , 'displayMonthlyReport'), 10 , 1);
		// filter for membership card report
		add_filter('membership_card' , array( $this , 'displayMembershipCardReport'), 10 , 2 );
		// filter for print label report
		add_filter('print_label' , array( $this , 'displayPrintLabelReport'), 10 , 1 );
		// filter for mailchimp report
		add_filter('mailchimp' , array( $this , 'displayMailchimpReport'), 10 , 2 );
		
		
	}




	/**
	 * This method is used to initialize default values
	 * @param null 
	 * @return null
	 * @author 
	 * 
	 **/			
	private function _initDefaultValues() {
		$this->_defaultValues['titles'] = array(
					 'fee' => __( 'MEMBERSHIP FEES TOTAL' , TEXT_DOMAIN ),
					 'monthly' 	   => __( 'MONTHLY REPORT' , TEXT_DOMAIN ),
					 'card'  => __( 'MEMBERSHIP CARDS' , TEXT_DOMAIN ),
					 'print'  => __( 'PRINT LABELS' , TEXT_DOMAIN ),
					 'mailchimp'  => __( 'MAILCHIMP SYNC REPORT' , TEXT_DOMAIN )
				); 
		$this->_defaultValues['info'] = array(
					'fee' => array(
										'url' => $this->_url . '&report=membership_fee_total',
										'page' => 'asg-admin-sub-page3',
										'icon' => ASG_UPLOAD_DIR . '/report/fees-total-icon.png',
									),
					'monthly' => array(
										'url' => $this->_url . '&report=monthly_report',
										'page' => 'asg-admin-sub-page6',
										'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
									),
					'card' => array(
										'url' => $this->_url . '&report=membership_card',
										'page' => 'asg-admin-sub-page2',
										'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
									),
					'print' => array(
										'url' => $this->_url . '&report=print_label',
										'page' => 'asg-admin-sub-page7',
										'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
									),
					'mailchimp' => array(
										'url' => $this->_url . '&report=mailchimp',
										'page' => 'asg-admin-sub-page7',
										'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
									)
				);
 		/*$this->_defaultValues  = array(
	 								array(
			 							'title' => __( 'MEMBERSHIP FEES TOTAL' , TEXT_DOMAIN ),
			 							'icon' => ASG_UPLOAD_DIR . '/report/fees-total-icon.png',
			 							'url' => $this->_url . '&report=membership_fee_total'

			 						),
			 						array(
			 							'title' => __( 'MONTHLY REPORT' , TEXT_DOMAIN ),
			 							'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
			 							'url' => $this->_url . '&report=monthly_report'
			 						),
			 						array(
			 							'title' => __( 'MEMBERSHIP CARDS' , TEXT_DOMAIN ),
			 							'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
			 							'url' => $this->_url . '&report=membership_card'
			 						),
			 						array(
			 							'title' => __( 'PRINT LABELS' , TEXT_DOMAIN ),
			 							'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
			 							'url' => $this->_url . '&report=print_label'
			 						),
			 						array(
			 							'title' => __( 'MAILCHIMP SYNC REPORT' , TEXT_DOMAIN ),
			 							'icon' => ASG_UPLOAD_DIR . '/report/report-icon.png',
			 							'url' => $this->_url . '&report=mailchimp'
			 						)
		 						);*/
 		$this->_defaultValues = apply_filters('report_dashboard_setting' , $this->_defaultValues);
 	} 	

 	/**
	 *
	 *	This method is used to simply display report dashboard
	 * 	@access private
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */
 	
 	function reportDashoard() {   
 		new ASG_Report_Dashboard_Layout($this->_defaultValues);
		/*$html = '<div class="wrap asg-content"><div class="meta-box-sortables ui-sortable">';
		
		foreach ($this->_defaultValues as $key => $value) {
			
				$html .= sprintf("<div id='postbox-container-%s'>
									<div id='dashboard_right_now-%s' class='postbox'>
										<button type='button' class='handlediv button-link' aria-hiddenexpanded='true'>
									        <span class='screen-reader-text'>Toggle panel: Activity</span>
									        <span class='toggle-indicator' aria-hidden='true'></span>
								    	</button>
									    <h2 class='hndle ui-sortable-handle'>
									        <a href='%s'><span>%s</span></a>
									    </h2>
									    <div class='inside'>
									       <img src='%s'/>
	       								</div>
									</div>
							    </div>" , $key+1 , $key+1 , $value['url'] , $value['title'] , $value['icon']);
		}
		$html .= '</div></div>';
		$html = apply_filters('asg_report_dashboard' , $html);
		echo $html;*/
	}


	/**
	 *
	 *	This method is used to build form for chapter section
	 * 	@access public
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function setFormStructre($structure = array() , $data) {  
		
	}


	public function displayReportForm() {  
		?>
		<div id="my-content-id">
				<h2><?php echo $this->_config['title']; ?></h2>
				<form method="post" id="<?php echo $this->_config['form_id']; ?>">
					<table class="setting-table" data-parsley-validate >
						<tbody>
						<?php foreach ($this->_fields as $field):
								$validators  = !empty($field['validator']) ? $field['validator'] : '';
								$error = ASG_Helper::getErrors( $this->_errors , $field['name']);
								$hidden = $field['hidden'] ? 'style="display:none"' : '';
								$html  = '';
								switch ($field['type']) {
									case 'text':
										$html .= '<tr class="user-login-wrap" ' . $hidden . ' id="' . $field['tr'] . '">
												<td><label for="' .  $field['name']  . '"> ' . $field['label']  . '</label></td>
												<td><input class="'. $field['class'] .'" type="' . $field['type'] . '" name="'. $field['name'] . '" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
											 	<td><p>' . $field['message'] . '</p></td>
											 	';
										break;
									case 'button':
										$html .= '<tr class="user-login-wrap" ' . $hidden . ' id="' . $field['tr'] . '">
												<td><label for="' .  $field['name']  . '">' . __( $field['label'] ) . '</label></td>
												<td>.
													<input class="'. $field['class'] .'" type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="' . $field['btn_value'] . '" class="regular-text" '. $validators .'>
													<input class="hidden" type="hidden" name="' . $field['name'] . '"  value="' . $field['value'] . '"/>
												</td>
											 	<td><p>' . $field['message'] . '</p></td>
											 	<td><img class= "' . $field['imgClass'] . '" src="' . $field['value'] . '"></td>';
										break;
									case 'select':
										$html  .= '<tr class="user-login-wrap" ' . $hidden . ' id="' . $field['tr'] . '">
													<td><label for="' .  $field['name']  . '">' . __( $field['label'] ) . '</label></td>
													<td><select class="'. $field['class'] . '" name="'. $field['name'] . '"  id="'. $field['id'] .'" ' . $event . '  style="width:400px;">';
													foreach ($field['value'] as $key => $value) {
															$selected = ($key == $field['selected_value']) ? 'selected="selected"' : '';
												   			$html .= '<option value=' . $key . ' ' . $selected . '>' . $value . '</option>';
												 		}
										$html .= '</select></td></tr>';
										break;
									case 'multiple':
										$html  .= '<tr class="user-login-wrap" ' . $hidden . ' id="' . $field['tr'] . '">
													<td><label for="' .  $field['name']  . '">' . __( $field['label'] ) . '</label></td>
													<td><select  class="'. $field['class'] . ' multiple" name="'. $field['name'] . '[]"' .'" ' . $event . ' multiple = "true" style="width:200px;height:200px">
													<option value ="1" id="opt-all-none" data-val ="' . __('Select None' , TEXT_DOMAIN ). '">' . __('Select all' , TEXT_DOMAIN ) . '</option>';
													foreach ($field['value'] as $key => $value) {
															$selected = in_array($key ,$field['selected_value']) ? 'selected="selected"' : '';
												   			$html .= '<option value=' . $key . ' ' . $selected . '>' . $value . '</option>';
												 		}

										$html .= '</select></td></tr>';
										break;
									case 'multiple-chosen':
										$html  .= '<tr class="user-login-wrap" ' . $hidden . ' id="' . $field['tr'] . '">
													<td><label for="' .  $field['name']  . '">' . __( $field['label'] ) . '</label></td>
													<td><select  class="'. $field['class'] . '" name="'. $field['name'] . '[]"' .'" ' . $event . ' multiple = "true"  style="width:400px;">
													<optgroup label="' . __('Select All' , TEXT_DOMAIN ) . '" id="select-all">';
													foreach ($field['value'] as $key => $value) {
															$selected = in_array($key ,$field['selected_value']) ? 'selected="selected"' : '';
												   			$html .= '<option value=' . $key . ' ' . $selected . '>' . $value . '</option>';
												 		}

										$html .= '</optgroup><optgroup label="None" id="select-none"><option style="display:none">None</option</optgroup></select></td></tr>';
										break;
									default:
										$html .= '<tr class="user-login-wrap" ' . $hidden . ' id="' . $field['tr'] . '">
												<td><label for="' .  $field['name']  . '">' . __( $field['label'] ) . '</label></td>
												<td><input type="' . $field['type'] . '" name="'  .  $field['name'] . '" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
											  <td><p>' . $field['message'] . '</p></td>';
										break;
								}
								$html .= !empty($error)  ? '<td><p class="alert alert-danger">'. $error .'</p></td></tr>' : '</tr>';
								echo $html;
							endforeach;
						?>
						</tbody>
					</table>
					<input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo wp_create_nonce( 'asg_action_nonce'); ?>" />
					<input type="hidden" name="report_type" value="<?php echo $this->_config['report_type']; ?>" />
					<input type="submit" name="run" id = "run" value="<?php echo __('Run', TEXT_DOMAIN ); ?>" class="button button-primary"/>
				</form>
			<div id="report">
			</div>
		</div>
		<?php
	}


	/**
	 * This method is used to display membership fee total 
	 * report
	 * @param null 
	 * @return null
	 * @author 
	 * 
	 **/	

	function displayMembershipFeeTotalReport() {
		$defaults = array(
							array(
								'label' => 'REPORT',
								'type'	=> 'select',
								'name'	=> 'report',
								'class' => 'fee-total-report',
								'validator'=> '',
								'value' => array(
												__('Total Report',TEXT_DOMAIN) ,
												__('Indivisual Transaction',TEXT_DOMAIN)
											),
								'selected_value' => array(
												__('Total Report',TEXT_DOMAIN) ,
												__('Indivisual Transaction',TEXT_DOMAIN)
											),
							),
							array(
								'label' => 'DATE',
								'type'	=> 'text',
								'name'	=> 'join_date',
								'value' => '',
								'class' => 'jquery-datepicker',
								'validator' => '',
							)
						);
		$this->_config = array(
								'title' => __('Membership Fee Totals' , TEXT_DOMAIN ),
								'form_id' => 'fee-report-form',
								'report_type' => 'membership_fee_total'
						);
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
		$this->displayReportForm();
	}

	/**
	 * This method is used to display membership monthly
	 * report
	 * @param null 
	 * @return null
	 * @author 
	 * 
	 **/	
	 		
	function displayMonthlyReport($data) {
		$options = array(
			__('CHAPTER REBATES', TEXT_DOMAIN),
			__('NEW MEMBERS', TEXT_DOMAIN),
			__('NEW MEMBERS JUNIOR', TEXT_DOMAIN),
			__('RENEWED MEMBERS', TEXT_DOMAIN),
			__('RENEWED MEMBERS JUNIOR', TEXT_DOMAIN),
			__('TRANSFERRED MEMBERS', TEXT_DOMAIN),
			__('MEMBERSHIP EXPIRATION', TEXT_DOMAIN),
			__('RENEWAL NOTICE', TEXT_DOMAIN),
			__('RENEWAL NOTICE CARDS', TEXT_DOMAIN),
			__('FULL MEMBERSHIP REPORT', TEXT_DOMAIN),
			);

		$renewalNoticeOpt = array(
			__('EXCLUDE AUTO RENEWAL', TEXT_DOMAIN),
			__('INCLUDE AUTO RENEWA', TEXT_DOMAIN),
			__('ONLY AUTO RENEWA', TEXT_DOMAIN),
			);

		$defaults = array(
							array(
								'label' => __('REPORT', TEXT_DOMAIN),
								'type'	=> 'select',
								'name'	=> 'report',
								'id'    => 'asg-monthly-report',
								'validator'=> '',
								'value' => $options,
								'selected_value' => $_POST['report']
							),
							array(
								'label' =>  __('START DATE', TEXT_DOMAIN),
								'type'	=> 'text',
								'name'	=> 'start_date',
								'value' => $_POST['start_date'],
								'class' => 'jquery-datepicker',
								'validator' => '',
								'hidden' => in_array($_POST['report'] , array(7,8)) ? true : false
							),
							array(
								'label' =>  __('END DATE', TEXT_DOMAIN),
								'type'	=> 'text',
								'name'	=> 'end_date',
								'id'	=> 'end_date',
								'value' => $_POST['end_date'],
								'validator' => ''
							),
							array(
								'label' => __('AUTO RENEWAL OPTION', TEXT_DOMAIN),
								'type'	=> 'select',
								'name'	=> 'auto_renewal_opt',
								'value' =>  $renewalNoticeOpt,
								'id' => 'auto-renewal-opt',
								'selected_value'=> !empty($_POST['auto_renewal_opt']) ? $_POST['auto_renewal_opt'] : array(),
								'validator' => '',
								'hidden' => $_POST['report'] != 8 ? true : false,
							),
							array(
								'label' => __('CHAPTER', TEXT_DOMAIN),
								'type'	=> 'multiple',
								'name'	=> 'chapter_id',
								'value' =>  $this->_chapters,
								'class' => '',
								'selected_value'=> !empty($_POST['chapter_id']) ? $_POST['chapter_id'] : array(),
								'validator' => '',
								'multiple' => true
							)
						);
		$this->_config = array(
								'title' => __('Monthly Reports' , TEXT_DOMAIN ),
								'form_id' => 'monthly-report-form',
								'report_type' => 'monthly_report'
						);
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
		$this->displayReportForm();
		!empty($data) ? 
			do_action( 'asg_view' , $this->_path , 'monthly/asg-monthly-report' ,  $data)
			: '';

	}

	/**
	 * This method is used to display membership monthly
	 * report
	 * @param null 
	 * @return null
	 * @author 
	 * 
	 **/	
	 		
	function displayMembershipCardReport($data) {
		$options = array(
				__('BY DATE', TEXT_DOMAIN),
				__('BY MEMBERID', TEXT_DOMAIN),
			);
		$defaults = array(

							array(
								'label' => 'MEMBERSHIP CARDS',
								'type'	=> 'select',
								'name'	=> 'report',
								'id'    => 'asg-membership-card',
								'validator'=> '',
								'value' => $options,
								'selected_value' => $_POST['report']
							),
							array(
								'label' => 'START DATE',
								'type'	=> 'text',
								'name'	=> 'start_date',
								'hidden'=> $_POST['report'] == 1 ? true : false,
								'value' => $_POST['start_date'],
								'class' => 'jquery-datepicker join-date',
								'validator' => ''
							),
							array(
								'label' => 'MEMBER ID',
								'type'	=> 'text',
								'hidden'=> $_POST['report'] == 0 ? true : false,
								'name'	=> 'member_id',
								'value' => $_POST['join_date'],
								'class' => 'memberId',
								'validator' => '',
							)
						);
		$this->_config = array(
								'title' => __('Membership Card Reports' , TEXT_DOMAIN ),
								'form_id' => 'membership-card',
								'report_type' => 'membership_card'
						);
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
		$this->displayReportForm();
		$arr = array(
					'data' => $data,
					'url'  => $this->_url
				);
		!empty($data) ? 
			do_action( 'asg_view' , $this->_path , 'membership-card/asg-membership-card-report' ,  $arr)
			: '';
	}

	/**
	 * This method is used to display membership monthly
	 * report
	 * @param null 
	 * @return null
	 * @author 
	 * 
	 **/	
	 		
	function displayPrintLabelReport($data) {
		$options = array(
					__('PRINT', TEXT_DOMAIN),
					__('PRINT NEW LABELS BETWEEN:', TEXT_DOMAIN),
					__('CHAPTER POSITIONS', TEXT_DOMAIN)
				);
		$defaults = array(

							array(
								'label' => 'CHAPTER LABEL AS OF',
								'tr' => 'report-type',
								'type'	=> 'select',
								'name'	=> 'report',
								'id'    => 'asg-print-label-report',
								'validator'=> '',
								'value' => $options,
								'selected_value' => $_POST['report']
							),
							array(
								'label' => 'START DATE',
								'tr' => 'start-date',
								'type'	=> 'text',
								'name'	=> 'start_date',
								'hidden'=> in_array($_POST['report'],array(0,1)) ? false : true,
								'value' => $_POST['start_date'],
								'class' => 'jquery-datepicker',
								'validator' => ''
							),
							array(
								'label' => 'END DATE',
								'type'	=> 'text',
								'name'	=> 'end_date',
								'id'	=> 'end_date',
								'hidden'=> $_POST['report'] == 1 ? false : true,
								'value' => $_POST['end_date'],
								'class' => '',
								'validator' => ''
							),
							array(
								'label' => 'Positions',
								'tr' => 'position',
								'type'	=> 'multiple-chosen',
								'name'	=> 'title',
								'hidden'=>  $_POST['report'] == 2 ? false : true,
								'value' => $this->_titles ,
								'selected_value'=> !empty($_POST['title']) ? $_POST['title'] : array(),
								'class' => 'asg-chosen',
								'validator' => ''
							),
							array(
								'label' => 'CHAPTER',
								'tr' => 'chapter',
								'type'	=> 'multiple',
								'name'	=> 'chapter_id',
								'value' =>  $this->_chapters,
								'selected_value'=> !empty($_POST['chapter_id']) ? $_POST['chapter_id'] : array(),
								'multiple' => true,
								'class' => '',
								'validator' => ''
							)
						);
		$this->_config = array(
								'title' => __('Print Label Reports' , TEXT_DOMAIN ),
								'form_id' => 'membership-card',
								'report_type' => 'print_label'
						);
		$this->_fields = !empty($structure) ? array_merge($defaults,$structure) : $defaults;
		$this->displayReportForm();
		$arr = array(
					'data' => $data,
					'url'  => $this->_url
				);
		!empty($data) ? 
			do_action( 'asg_view' , $this->_path , 'print-label/asg-print-label-report' ,  $arr)
			: '';
		
	}

	/**
	 * This method is used to display membership monthly
	 * report
	 * @param null 
	 * @return null
	 * @author 
	 * 
	 **/	
	 		
	function displayMailchimpReport() {
		$memberObj = new ASG_Member_Model();
		$chimpy = new ASG_Mailchimp_Helper();
		$data = array(
						'members' 	=> $memberObj->giveMeAllMemberInfo(),
						'pages' 	=> $memberObj->getNumPages(true),
						'chimpList' => $chimpy->lists
					);
		
		//$mailChimp = new ASG_MailChimp('db9da9f1d2b21d967f4067313c0f3d28-us12');
 		$data['sync'] = array('Pending' , 'Approved');
 		$data['action'] = array('Created' , 'Modified');
		/*$resp = $mailChimp->call('lists/members' , 
									array(
											'id' => '83234643ed' ,
											'offset' => 1,
											'count' => 1
										)
								);
		$data['members'] = !empty($resp) ? $resp['data'] : array();*/
		//$data['members'] = !empty($members) ? $members : array();
		do_action( 'asg_view' , $this->_path , 'mailchimp/asg-mailchimp-report' ,  $data);
		
	}

	/**
	 *
	 *	This method is used for formatting array
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _formatArrayForSelect ($data) { 
		$customArr = array();
		if(!is_array($data))
			return false;
		foreach ($data as $key => $value) {
			$v = array_pop($value);
			$k = array_pop($value);
			$customArr[$k] = $v;
			
			
		}
		return $customArr;
	}
}

new ASG_Reports();



