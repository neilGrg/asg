<?php echo apply_filters('asg_crud_message' , ''); ?>
<h2><?php echo __('Member Sync Report' , TEXT_DOMAIN) ?></h2>
	<label class="screen-reader-text" for="mailchimp-search-input"><?php echo __('Mailchimp' , TEXT_DOMAIN) ?></label>
<?php 
	echo apply_filters(
				'asg_limit_setting' ,
				'asg-mailchimp-limit' , 
				get_option('asg-mailchimp-limit')
			);
?>	

<form id="mailchimp-search">
	<p class="search-box asg-message">
		<!-- <label class="screen-reader-text" for="member-search-input"><?php echo __('Member' , TEXT_DOMAIN) ?></label> -->
		<input type="search" id="search-first-name" name="first_name" value="" placeholder="<?php echo __('Name' , TEXT_DOMAIN) ?>">
		<!-- <label class="screen-reader-text" for="member-search-input"><?php echo __('Mailchimp' , TEXT_DOMAIN) ?></label> -->
		<input type="search" id="search-last-name" name="attribute" value="" placeholder="<?php echo __('Attribute' , TEXT_DOMAIN) ?>">
		<!-- <label class="screen-reader-text" for="chapter-search-input"><?php echo __('Mailchimp' , TEXT_DOMAIN) ?></label> -->
		<input type="search" id="search-member-id" name="transfer_from" value="" placeholder="<?php echo __('Changed From' , TEXT_DOMAIN)  ?>" >
		<input type="search" id="search-member-id" name="transfer_to" value="" placeholder="<?php echo __('Changed To' , TEXT_DOMAIN)  ?>" >
		<input type="search" id="search-member-id" name="updated_by" value="" placeholder="<?php echo __('Updated By' , TEXT_DOMAIN)  ?>" >
		<label class="screen-reader-text" for="member-search-input"><?php echo __('Sync Status' , TEXT_DOMAIN) ?></label>
		<select class="sync-status" name="sync_status">
			<option value=""><?php echo __('Select' , TEXT_DOMAIN); ?></option>
		    <?php foreach ($sync as $key => $value) { ?>
				   <option value='<?php echo $key; ?>'> <?php echo $value; ?> </option>
			<?php } ?>
		</select>
		<label class="screen-reader-text" for="member-search-input"><?php echo __('Action' , TEXT_DOMAIN) ?></label>
		<!-- <select class="chapter" name="chapter" id="chapter">
			<option value=""><?php echo __('Select' , TEXT_DOMAIN); ?></option>
		    <?php foreach ($actions as $key => $value) { ?>
				   <option value='<?php echo $key; ?>'> <?php echo $value; ?> </option>
			<?php } ?>
		</select> -->
		<input type="button" id="asg-mailchimp-search" class="button" value="<?php echo __('Search'  , TEXT_DOMAIN ) ?>">
		<div id="sync-form-elements">
			<select name="list_id" class="list-id">
				<?php foreach($chimpList as $key=>$list): ?>
						<option value="<?php echo $key; ?>"><?php echo $list; ?></option>
				<?php endforeach; ?>
			</select>
			<input type="button" id="asg-start-sync" value="<?php echo __('Start Sync'  , TEXT_DOMAIN ) ?>">
		</div>
	</p>
</form>
<?php if(empty($members) || count($members) < 1): ?>
	<div id="no-result-found" >
		<p><?php echo __('No Result Found' , TEXT_DOMAIN); ?></p>
	</div>
<?php return false;endif; ?>
<form id="asg-mailchimp-sync" method = "POST">
    <table class="wp-list-table widefat fixed striped pages">
		<thead>
			<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
			<th><?php echo __('MEMBER ID' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('NAME' , TEXT_DOMAIN); ?></th>
			<!-- <th><?php echo __('EMAIL' , TEXT_DOMAIN); ?></th> -->
			<th><?php echo __('ATTRIBUTE' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('CHANGED FROM' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('CHANGED TO' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('UPDATED BY' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('SYNC STATUS' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('ACTION' , TEXT_DOMAIN); ?></th>
		</thead>
		<tbody>
			<?php foreach ($members as $key => $member) { 
					$memId = isset($member['id']) ? $member['id'] : '';
					$status = isset($member['sync_status']) ? $sync[$member['sync_status']] : '';
					$attribute = isset($member['attribute']) ? $member['attribute'] : 'Null';
					$changeFrom = isset($member['transfer_from']) ? $member['transfer_from'] : 'false';
					$changeTo = isset($member['transfer_to']) ? $member['transfer_to'] : 'false';
					$updatedBy = isset($member['updated_by']) ? $member['updated_by'] : 'admin';
					$name = isset($member['first_name']) ? $member['first_name'] . ' ' . $member['last_name']: '';
					$email = isset($member['user_email']) ? $member['user_email'] : '';
					$action = isset($member['action']) ? $member['action'] : 'CREATED';
			 ?>		

				<tr>
					<th scope="row" class="check-column">		
						<label class="screen-reader-text" for="cb-select-2"><?php echo __('Select Member' , TEXT_DOMAIN); ?></label>
						<input id="cb-select-2" type="checkbox" name="sync_member[]" value="<?php echo $memId; ?>">
					</th>
					<td><?php echo $memId; ?></td>
					<td><?php echo $name; ?></td>
					<!-- <td><?php echo $email; ?></td> -->
					<td><?php echo $attribute; ?></td>
					<td><?php echo $changeFrom; ?></td>
					<td><?php echo $changeTo; ?></td>
					<td><?php echo $updatedBy; ?></td>
					<td><?php echo $status; ?></td>
					<td><?php echo $action; ?></td>
				</tr>
			<?php }
			$pagination = apply_filters('asg_display_pagination' , 'Chapter' , $this->_pages ); 
		  	if(!empty($pagination)):
			?>
				<tr>
					<td>
						<div id="asg-pagination">
						 <?php echo $pagination; ?>
						</div>
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
		<thead>
			<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
			<th><?php echo __('MEMBER ID' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('NAME' , TEXT_DOMAIN); ?></th>
			<!-- <th><?php echo __('EMAIL' , TEXT_DOMAIN); ?></th> -->
			<th><?php echo __('ATTRIBUTE' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('CHANGED FROM' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('CHANGED TO' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('UPDATED BY' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('SYNC STATUS' , TEXT_DOMAIN); ?></th>
			<th><?php echo __('ACTION' , TEXT_DOMAIN); ?></th>
		</thead>
	</table>
	<input type="hidden" name="asg_list_id" value=""  id="asg-list-id"/>
	<input type="hidden" name="report_type" value="mailchimp_sync" />
</form>	
<div>
	<?php 
		$pagination = apply_filters('asg_display_pagination' , 'MailChimp' , $pages ); 
	  	if(!empty($pagination)):
		?>
			<div id="asg-pagination">
			 <?php echo $pagination; ?>
			</div>
	<?php endif; ?>
</div>

