<?php extract($data);?>
<h2><?php echo __('Monthly Report' , TEXT_DOMAIN) ?></h2>
<?php if(!$count): ?>
	<div id="no-result-found" >
		<p><?php echo __('No Result Found' , TEXT_DOMAIN); ?></p>
	</div>
<?php 
		return false;
	endif;
?>
<!-- <a href="#" class="all">Export All CSV</a> -->
<div id="tabs">
	<ul>
		<?php foreach ($chapters as $key => $chapter ) { ?>
				<li><a href="#<?php echo str_replace(' ',  '-' , $chapter); ?>"><?php echo ucfirst($chapter); ?></a></li>
		<?php } ?>
	</ul>

	<?php foreach ($datas as $key => $data) {  ?>
		<div id="<?php echo str_replace(' ',  '-' , $key); ?>">
			<?php if(is_array($data) && count($data) < 1): ?>
				<div id="no-result-found" >
					<p><?php echo __('No Result Found' , TEXT_DOMAIN); ?></p>
				</div>
			<?php else :?>
				<div id="asg-download-opt">
					<a href="#" onclick="exportParticularCsv(this);">
						<i class="fa fa-table"></i>
						<?php //echo __('EXPORT TO CSV' , TEXT_DOMAIN); ?>
					</a>|
					<a href="#" class="asg-pdf">
						<?php //echo __('EXPORT TO PDF' , TEXT_DOMAIN); ?>
						<i class="fa fa-file-pdf-o"></i>
					</a>
				</div>
				<table class="wp-list-table widefat fixed striped pages report" >
					<thead>
						<th><?php echo __('MEMBER ID' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('FIRST NAME' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('LAST NAME' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('ADDRESS' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('STATE' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('CITY' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('ZIP' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('TEL#' , TEXT_DOMAIN); ?></th>
						<th><?php echo __('JOIN DATE' , TEXT_DOMAIN); ?></th>
						<!-- <th><?php echo __('UPDATED BY' , TEXT_DOMAIN); ?></th> -->
					</thead>
					<tbody>
						<?php foreach ($data as $key => $member) { 
								$memId = isset($member['id']) ? $member['id'] : '';
								$firstName = isset($member['first_name']) ? $member['first_name'] : '';
								$lastName = isset($member['last_name']) ? $member['last_name'] : 'Null';
								$adddress = isset($member['address']) ? $member['address'] : 'false';
								$state = isset($member['state']) ? $member['state'] : 'false';
								$city = isset($member['city']) ? $member['city'] : 'admin';
								$zip = isset($member['zip']) ? $member['zip'] . ' ' . $member['last_name']: '';
								$tel = isset($member['work_phone']) ? $member['work_phone'] : '';
								$joinDate = isset($member['join_date']) ? date('Y-m-d'  , strtotime($member['join_date'])) : 'CREATED';
						 ?>		
							<tr>
								<td><?php echo $memId; ?></td>
								<td><?php echo $firstName; ?></td>
								<td><?php echo $lastName; ?></td>
								<td><?php echo $adddress; ?></td>
								<td><?php echo $state; ?></td>
								<td><?php echo $city; ?></td>
								<td><?php echo $zip; ?></td>
								<td><?php echo $tel; ?></td>
								<td><?php echo $joinDate; ?></td>
							</tr>
						<?php }
						$pagination = apply_filters('asg_display_pagination' , 'Chapter' , $this->_pages ); 
					  	if(!empty($pagination)):
						?>
							<tr>
								<td>
									<div id="asg-pagination">
									 <?php echo $pagination; ?>
									</div>
								</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	<?php  } ?>
</div>
