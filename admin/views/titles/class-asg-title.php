<?php 
class ASG_Title {

	private $_titles = array();
	private $title1 = Null;
	private $title2 = Null;
	private $title3 = Null;
	private $_titleModel = Null;

	function __construct() {
		global $data;
		$this->_titles = (!empty($data) && isset($data['titles'])) ? $data['titles'] : '';
		$this->_titleModel = new ASG_Title_Model();
		$this->_titleTabDisplay();
	}

	private function _titleTabDisplay() {
		?>
		    <!-- Create a header in the default WordPress 'wrap' container -->
		    <div id="icon-themes" class="icon32"></div>
	        <h2><?php echo __('Title Options') ?></h2>
	        <?php 
	        	settings_errors(); 
	        	if( isset( $_GET[ 'tab' ] ) ) {
	                $active_tab = $_GET[ 'tab' ];
	            } // end if
	        ?>
	        <div class="nav-tab-wrapper">
		        <?php foreach ($this->_titles as $key => $title) { ?>
		        	<a href="?page=asg-admin-sub-page4&tab=<?php echo $key ?>" class="nav-tab <?php echo $active_tab ==  $key ? 'nav-tab-active' : ''; ?>"><?php echo  $title;?></a>
		        <?php } ?>   
		        <?php if( $active_tab == 'tab3'): ?>
			        <div id="title3">
			        	<?php $this->_titleModel->display(new ASG_Title3_Model()); ?>
			        </div>
		    	<?php elseif( $active_tab == 'tab2'): ?>
			        <div id="title2">
			        	<?php $this->_titleModel->display(new ASG_Title2_Model()); ?>
			        </div>
			    <?php else : ?>
			    	<div id="title1">
			            <?php $this->_titleModel->display(new ASG_Title1_Model()); ?>
			        </div>
			        
    			<?php endif; ?> 
	        </div>
		<?php
		} // end sandbox_theme_display

		
}
// global variable file handler
$GLOBALS['Title']  = new ASG_Title();