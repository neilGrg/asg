<?php
class ASG_Chapters {
	private $_defaultValues = array();
	private $_chapters = array();
	private $_fields = array();
	private $_presidents = array();
	private $_errors = array();
	private $_postedFields = array();	
	private $_id = '';
	private $_url = '';

	function __construct() {
		global $data;
		$this->_url = (!empty($data) && isset($data['url'])) ? $data['url'] : array();
		$this->_chapters = (!empty($data) && isset($data['chapters'])) ? $data['chapters'] : array();
		$this->_pages = (!empty($data) && isset($data['pages'])) ? (int) $data['pages'] : '';
		$this->_presidents = $data['presidents'];
		$this->_states = $data['states'];
		if(
			($_REQUEST['operation'] == 'delete' || $_REQUEST['action'] == 'bulk_delete') && $data['success'] ||
			(ASG_DASHBOARD::$limitUpdate)
		) {
			ASG_DASHBOARD::$limitUpdate = false;
			ASG_Helper::asgRedirect($this->_url);
		}
		$this->_fieldsForChapters();
		$this->_chapterDisplay();
		$this->_displayForm();
		

	}

	/**
	 *
	 *	This method is used to form post data for chapter section
	 * 	@access private
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _giveMePostData() {

		$chapter = isset($_REQUEST['chapter']) ? $_REQUEST['chapter'] : array();
		$member = isset($_REQUEST['member']) ? $_REQUEST['member'] : array();
		$title1 = isset($_REQUEST['title1']) ? $_REQUEST['title1'] : array();
		return  array_merge( $chapter, $member , $title1);
	}

	private function _initDefaultValues() {
 		$this->_defaultValues  = array(
		 							'menus' => array(
		 										__( 'List Of Chapters' , TEXT_DOMAIN ),
		 										__( 'Change Passwords' , TEXT_DOMAIN ),
		 									),
		 							'title' => __( 'Chapter List' , TEXT_DOMAIN ),
		 							'member_id' => __( 'Chapter ID' , TEXT_DOMAIN ),
		 							'name' => __( 'Name' , TEXT_DOMAIN ),
		 							'fname' => __( 'First Name' , TEXT_DOMAIN ),
		 							'lname' => __( 'Last Name' , TEXT_DOMAIN ),
		 							'chapter' => __( 'Chapter' , TEXT_DOMAIN ),
		 							'operation' => __( 'Operations' , TEXT_DOMAIN ),
		 							'add' => __( 'Add Chapter' , TEXT_DOMAIN ),
		 							'save' => __( ' Save Chapter' , TEXT_DOMAIN ),
		 							'cancel' => __( ' Cancel' , TEXT_DOMAIN ),
		 							'edit' => __( 'Edit' , TEXT_DOMAIN ),
		 							'delete' => __( 'Delete' , TEXT_DOMAIN ),
		 							'search' => __( 'Search' , TEXT_DOMAIN ),
		 							'clear' => __( 'Clear' , TEXT_DOMAIN ),
		 						);
 	} 	

	/**
	 *
	 *	This method is used to build form for chapter section
	 * 	@access private
	 *  @param  null
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _chapterDisplay() { 
		?>
		    <!-- Create a header in the default WordPress 'wrap' container -->
		    <?php 
				echo apply_filters(
						'asg_limit_setting' ,
						'asg-chapter-limit' , 
						get_option('asg-chapter-limit')
					);
		    	echo apply_filters('asg_crud_message' , '');
			?>	
			<h2><?php echo __('Chapters Lists' , TEXT_DOMAIN) ?></h2>
	    	<form method="POST" id="bulk-action" >
	    	<div class="alignleft actions bulkactions">
				<label for="bulk-action-selector-top" class="screen-reader-text"><?php echo __('Select bulk action' , TEXT_DOMAIN); ?></label>
				<select name="action" id="bulk-action-selector-top">
					<option value="-1"><?php echo __('Bulk Actions' , TEXT_DOMAIN); ?></option>
					<option value="bulk_delete"><?php echo __('Delete' , TEXT_DOMAIN); ?></option>
				</select>
				<input type="button" id="doaction" class="button action" value="Apply">
			</div>
	    	<div id="add_chapter"><a href="<?php echo $this->_url . "#TB_inline?width=600&height=550&inlineId=my-content-id"; ?> " class="page-title-action thickbox" data-operation = "add" onclick="f.setChapterActionType(this);"><?php echo __('Add New Chapter' , TEXT_DOMAIN); ?></a></div>
	     	<p class="search-box">
				<label class="screen-reader-text" for="chapter-search-input"><?php echo __('Chapter Lists'  , TEXT_DOMAIN ) ?></label>
				<input type="search" id="chapter-search-input" name="chapter" value="" placeholder="<?php echo __('Search Chapters'  , TEXT_DOMAIN ) ?>">
				<input type="button" id="search-submit" class="button" value="<?php echo __('Search'  , TEXT_DOMAIN ) ?>">
				<input type="button" id="chapter-clear" name="clear" value="<?php echo __('Clear'  , TEXT_DOMAIN ) ?>" onclick="clearMe();">
			</p>
			<table class="wp-list-table widefat fixed striped pages asg-chapter">
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo __('CHAPTER', TEXT_DOMAIN ); ?></th>
					<th><?php echo __('PRESIDENT', TEXT_DOMAIN ); ?></th>
					<th><?php echo __('MEMBERID', TEXT_DOMAIN ); ?></th>
					<th ><?php echo __('EDIT', TEXT_DOMAIN ); ?></th>
					<th ><?php echo __('DELETE', TEXT_DOMAIN ); ?></th>
				</thead>
				<tbody>
					<?php foreach ($this->_chapters as $key => $chapter) { ?>
						<tr>
							<th scope="row" class="check-column">		
								<label class="screen-reader-text" for="cb-select-2"><?php echo __('Select Member' , TEXT_DOMAIN); ?></label>
								<input id="cb-select-2" type="checkbox" name="delete_chapter[]" value="<?php echo $chapter['id']; ?>">
							</th>
							<td><?php echo $chapter['chapter']; ?></td>
							<td><?php echo apply_filters('asg_get_full_name', $chapter); ?></td>
							<td><?php echo $chapter['member_id']; ?></td>
							<td><a href="<?php echo $this->_url . "#TB_inline?width=600&height=550&inlineId=my-content-id"; ?>" data-operation = "edit"  data-id="<?php echo $chapter['id'] ?>" class="page-chapter-action thickbox" onclick="f.setChapterActionType(this);"><i class="fa fa-pencil-square-o"></i></a></td>
							<td><a href="<?php echo $this->_url . "&operation=delete&chapter_id=". $chapter['id']  ?>" onclick ="return confirm('Are you sure you want to Delete?')"><i class="fa fa-trash-o"></i></a></td>
							<td></td>
						</tr>
					<?php } ?>
				</tbody>
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo __('CHAPTER', TEXT_DOMAIN ); ?></th>
					<th><?php echo __('PRESIDENT', TEXT_DOMAIN ); ?></th>
					<th><?php echo __('MEMBERID', TEXT_DOMAIN ); ?></th>
					<th ><?php echo __('EDIT', TEXT_DOMAIN ); ?></th>
					<th ><?php echo __('DELETE', TEXT_DOMAIN ); ?></th>
				</thead>
			</table>
			</form>
			<div>
				<?php 
					$pagination = apply_filters('asg_display_pagination' , 'Chapter' , $this->_pages ); 
				  	if(!empty($pagination)):
					?>
						<div id="asg-pagination">
						 <?php echo $pagination; ?>
						</div>
				<?php endif; ?>
			</div>
		<?php
	} // end sandbox_theme_display


	protected function _fieldsForChapters() {
		//k => index key for select , v => index value for select
		$this->_fields = array(
								array(
									'label' => 'MEMBERID',
									'type'	=> 'text',
									'name'	=> 'member_id',
									'model' => 'chapter',
									'id'    => 'member_id',
									'validator'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'member_id')
								),
								array(
									'label' => 'FIRST NAME',
									'type'	=> 'text',
									'name'	=> 'first_name',
									'model'=> 'member',
									'id'    => 'first_name',
									'validator' => '',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'first_name')
								),
								array(
									'label' => 'MIDDLE NAME',
									'type'	=> 'text',
									'name'	=> 'middle_name',
									'model'=> 'member',
									'id'    => 'middle_name',
									'validator'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'middle_name')
								),
								array(
									'label' => 'LAST NAME',
									'type'	=> 'text',
									'name'	=> 'last_name',
									'model'=> 'member',
									'id'    => 'last_name',
									'validator'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'last_name')
								),
								array(
									'label' => 'PRESIDENT',
									'type'	=> 'select',
									'name'	=> 'president',
									'model'=> 'title1',
									'class'    => 'president',
									'value' => $this->_presidents,
									'k' => 'id',
									'v' => 'first_name',
									'selected_value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'president')
								),
								array(
									'label' => 'CHAPTER',
									'type'	=> 'text',
									'name'	=> 'chapter',
									'model'=> 'chapter',
									'id'    => 'chapter',
									'validator'    => '',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'chapter')
								),
								array(
									'label' => 'EMAIL',
									'type'	=> 'text',
									'name'	=> 'email',
									'model'=> 'chapter',
									'id'    => 'email',
									'validator' =>'',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'email')
								),
								array(
									'label' => 'TEL',
									'type'	=> 'text',
									'name'	=> 'tel',
									'model'=> 'chapter',
									'id'    => 'tel',
									'validator' =>'',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'tel')
								),
								array(
									'label' => 'EXT',
									'type'	=> 'text',
									'name'	=> 'ext',
									'model'=> 'chapter',
									'id'    => 'ext',
									'validator' =>'',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'ext')
								),
								array(
									'label' => 'ADDRESS',
									'type'	=> 'text',
									'name'	=> 'address',
									'model'=> 'chapter',
									'id'    => 'address',
									'validator' => '',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'address')
								),
								array(
									'label' => 'CITY',
									'type'	=> 'text',
									'name'	=> 'city',
									'model'=> 'chapter',
									'id'    => 'city',
									'validator' => '',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'city')
								),
								array(
									'label' => 'STATE',
									'type'	=> 'select',
									'name'	=> 'state',
									'model'=> 'chapter',
									'class'    => 'state',
									'validator' => '',
									'value' => $this->_states,
									'k' => 'sort_name',
									'v' => 'sort_name',
									'selected_value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'state')
								),
								array(
									'label' => 'ZIP',
									'type'	=> 'text',
									'name'	=> 'zip',
									'model'=> 'chapter',
									'id'    => 'zip',
									'validator' =>'',
									'value' => ASG_Helper::giveMeValueIfSet($this->_postedFields,'zip')
								),
						);
	}


	private function _displayForm() { 
		add_thickbox(); 
		?>
		<div id="my-content-id" style="display:none;">
				<h2><?php echo __('Chapter Information' , TEXT_DOMAIN ); ?></h2>
				<form action="" method="post" id="chapter-form" onsubmit="event.preventDefault();" >
					<table class="form-table" data-parsley-validate >
						<tbody>
						<?php foreach ($this->_fields as $field):
								$validators  = !empty($field['validator']) ? $field['validator'] : '';
								$error = ASG_Helper::getErrors( $this->_errors , $field['name']);
								$html  = '';
								switch ($field['type']) {
									case 'text':
										$html .= '<tr class="user-login-wrap">
												<td><label for="">' . __( $field['label'] ) . '</label></td>
												<td><input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
											 ';
										break;
									case 'select':
									    $html .= '<tr class="-user-login-wrap">
												<td><label for="">' . __( $field['label'] ) . '</label></td>
												<td><select class="'. $field['class'] . '" name="'. $field['model'] . '[' . $field['name'] . ']"  id="display_name" onchange ="f.givePresidentInfo(this);">
													<option value="0">' . __('Select' , TEXT_DOMAIN) . '</option>';
												foreach ($field['value'] as $key => $value) {
												   $selected = ($key == $field['selected_value']) ? 'selected="selected"' : '';
												   $html .= '<option value= "' . $value[$field['k']] . '">' . $value[$field['v']] . '</option>';
												 }
										$html .= '</select></td>
											';
										
										break;
									
									default:
										$html .= '<tr class="user-login-wrap">
												<td><label for="">' . __( $field['label'] ) . '</label></td>
												<td><input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="' . $field['value'] . '" class="regular-text" '. $validators .'></td>
											  ';
										break;
								}
								$html .= !empty($error)  ? '<td class="asg-validation-error"><p class="alert alert-danger">'. $error .'</p></td></tr>' : '</tr>';
								echo $html;
							endforeach;
						?>
						</tbody>
					</table>
					<input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo wp_create_nonce( 'asg_action_nonce'); ?>" />
					<input type="hidden" name="operation" value="<?php echo $_REQUEST['operation'] ?>" id="operation" />
					<input type="hidden" name="chapter_id" value="" id="chapter_id" />
					<input type="submit" name="save_member" id = "save-member" onclick = "f.saveChapter(this)" value="<?php echo __('Save', TEXT_DOMAIN ); ?>" class="button button-primary" value="<?php echo $_REQUEST['operation'] ?>" />
				</form>
		</div>
		<?php
	}

	public function extractStringFromPipe($str) {
		if(preg_match_all('/\|\|(.*?)\|\|/', $str, $matches) !== FALSE){
		    debug($matches);
		}
	}
}

// global variable file handler
$GLOBALS['chapter']  = new ASG_Chapters();
