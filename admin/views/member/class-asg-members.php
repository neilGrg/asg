<?php 

 class ASG_Member {

 	private $_defaultValues = array();
 	private $_members = array();
 	private $_title1 = array();
 	private $_title2 = array();
 	private $_title3 = array();
 	private $_chapters = array();
 	private $_countries = array();
 	private $_states = array();
 	private $_cities = array();
 	private $_url = '';
 	private $_operation = '';
 	private $_formStructure = array();
 	private $_paymentType = array();
 	private $_sources = array();
 	private $_errors = array();
 	private $_memberInfo = array('country' => 'United States' , 'state' => 'Alaska' , 'city' => 'city1');
 	private $_id = '';
 	private $_pages = '';
 	private $_status = '';
 	private $_isCountryUS = true;
 	private $_userId = '';

 	function __construct() {   
 		global $data; 
 		$this->_url = (!empty($data) && isset($data['url'])) ? $data['url'] : '';
 		$this->_errors = (!empty($data) && isset($data['errors'])) ? $data['errors'] : array();
 		$this->_members = isset($data['members']) ? $data['members'] : null;
 		$this->_pages = (!empty($data) && isset($data['pages'])) ? (int) $data['pages'] : '';
 		$this->_operation = isset($_REQUEST['operation']) ? $_REQUEST['operation'] : null;
 		$this->_chapters = ASG_Chapter_Model::getAllChapters();
 		$this->_title1 = $this->formatArrayForSelect(ASG_Title1_Model::getTitles());
 		$this->_title2 = $this->formatArrayForSelect(ASG_Title2_Model::getTitles());
 		$this->_title3 = $this->formatArrayForSelect(ASG_Title3_Model::getTitles());
 		if(isset($_REQUEST['save_member']) && !empty($this->_errors)) {
 			$this->_memberInfo = array_merge($_POST['user'],$_POST['member']);
 		} else if(
 				   (isset($_REQUEST['save_member']) && $data['success']) ||
 				   (ASG_DASHBOARD::$limitUpdate) ||
 				   (($_REQUEST['operation'] == 'delete' || $_REQUEST['action'] == 'bulk_delete') && $data['success'])
 				)
 		{
 			ASG_DASHBOARD::$limitUpdate = false;
  			?>
  			<script type="text/javascript">
  			   window.location.href = "<?php echo $this->_url;?>";
  			</script>
  			<?php
  			exit();
  		}
  		$this->_initDefaultValues();
		if($this->_operation == 'add' || $this->_operation == 'edit') {
 			$this->_id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? $_REQUEST['id'] : null;
 			if(!empty($this->_id)) {
 				$this->_memberInfo = ASG_Member_Model::asgGetMemberInfo($this->_id);
 				$this->_userId = $this->_memberInfo['user_id'];
 				$this->_isCountryUS = $this->_memberInfo['country'] == 'United States' ? true : false;
 			}
 			$this->_setFormStructure();
 			$this->_displayForm();
 		} else {
 			$this->_displayMemberPage();
 		} 
 	}


 	private function _initDefaultValues() {
 		$this->_defaultValues  = array(
		 							'menus' => array(
		 										__( 'List Of Members' , TEXT_DOMAIN ),
		 										__( 'Change Passwords' , TEXT_DOMAIN ),
		 									),
		 							'title' => __( 'Member List' , TEXT_DOMAIN ),
		 							'member_id' => __( 'Member ID' , TEXT_DOMAIN ),
		 							'name' => __( 'Name' , TEXT_DOMAIN ),
		 							'fname' => __( 'First Name' , TEXT_DOMAIN ),
		 							'lname' => __( 'Last Name' , TEXT_DOMAIN ),
		 							'chapter' => __( 'Chapter' , TEXT_DOMAIN ),
		 							'operation' => __( 'Operations' , TEXT_DOMAIN ),
		 							'add' => __( 'Add Member' , TEXT_DOMAIN ),
		 							'save' => __( ' Save Member' , TEXT_DOMAIN ),
		 							'cancel' => __( ' Cancel' , TEXT_DOMAIN ),
		 							'pdf' => __( 'Pdf' , TEXT_DOMAIN ),
		 							'edit' => __( 'Edit' , TEXT_DOMAIN ),
		 							'delete' => __( 'Delete' , TEXT_DOMAIN ),
		 							'search' => __( 'Search' , TEXT_DOMAIN ),
		 							'clear' => __( 'Clear' , TEXT_DOMAIN ),
		 							'generate_pdf' => __( 'Generate PDF' , TEXT_DOMAIN ),
		 						);
 	    $this->_paymentType = array(	
							'CHECK' => __('CHECK' , TEXT_DOMAIN ),
							'CREDIT CARD' => __('CREDIT CARD' , TEXT_DOMAIN),
							'LIFETIME MEMBER' => __('LIFETIME MEMBER' , TEXT_DOMAIN),
							'MONEY ORDER' => __('MONEY ORDER' , TEXT_DOMAIN)
						);
 	    $this->_sources = array(	
							'ANNUAL CONFERENCE' => __('ANNUAL CONFERENCE' , TEXT_DOMAIN ),
							'ASG MEMBER' => __('ASG MEMBER' , TEXT_DOMAIN),
							'FORMER MEMBER' => __('FORMER MEMBER' , TEXT_DOMAIN),
							'NEW CHAPTER' => __('NEW CHAPTER' , TEXT_DOMAIN),
							'NEWSPAPER' => __('NEWSPAPER' , TEXT_DOMAIN),
							'NG MEETING' => __('NG MEETING' , TEXT_DOMAIN),
							'THREADS' => __('THREADS' , TEXT_DOMAIN),
							'SEWNEWS' => __('SEWNEWS' , TEXT_DOMAIN),
							'MAGAZINE' => __('MAGAZINE' , TEXT_DOMAIN)
						);

 	    $this->_defaultStatus(array());
 	    
 	}


 	/**
	 * This method is used to set default status
	 * @param null
	 * @return boolean
	 * @since 1.0
	 * 
	 **/
	private function _defaultStatus($exArr) {
		$this->_status = array(	
							'N' => __('NEW' , TEXT_DOMAIN ),
							'JN' => __('JUNIOR NEW' , TEXT_DOMAIN),
							'R' => __('RENEWAL' , TEXT_DOMAIN),
							'JR' => __('JUNIOR RENEWAL' , TEXT_DOMAIN),
							'L' => __('ADDS LATE FEE' , TEXT_DOMAIN),
							
						);
		$this->_status = apply_filters('asg_member_status' , array_merge($this->_status,$exArr));
	}	
	
 	private function _displayMemberPage() {
 		$message = get_option('asg-crud-message');
 	   ?>
 	   		<!-- Create a header in the default WordPress 'wrap' container -->
	    	<?php echo apply_filters('asg_crud_message' , ''); ?>
	    	<?php echo apply_filters(
						'asg_limit_setting' ,
						'asg-member-limit' , 
						get_option('asg-member-limit')
					);
			?>	
	    	<h2><?php echo $this->_defaultValues['title'] ?></h2>
	    	<form method="POST" id="bulk-action" >
	    	<div class="alignleft actions bulkactions">
				<label for="bulk-action-selector-top" class="screen-reader-text"><?php echo __('Select bulk action' , TEXT_DOMAIN); ?></label><select name="action" id="bulk-action-selector-top">
				<option value="-1"><?php echo __('Bulk Actions' , TEXT_DOMAIN); ?></option>
					<option value="bulk_delete"><?php echo __('Delete' , TEXT_DOMAIN); ?></option>
				</select>
				<input type="button" id="doaction" class="button action" value="Apply">
			</div>
	     	<label class="screen-reader-text" for="chapter-search-input"><?php echo $this->_defaultValues['title'] ?></label>
			<div id="add_title"><a href="<?php echo $this->_url . "&operation=add"; ?> " class="page-title-action thickbox"  onclick="f.setBodActionType(this);" data-operation = "add"><?php echo $this->_defaultValues['add']; ?></a></div>
			<p class="search-box asg-message">
				<label class="screen-reader-text" for="member-search-input"><?php echo $this->_defaultValues['fname']  ?></label>
				<input type="search" id="search-first-name" name="first_name" value="" placeholder="<?php echo $this->_defaultValues['fname']  ?>">
				<label class="screen-reader-text" for="member-search-input"><?php echo $this->_defaultValues['lname']  ?></label>
				<input type="search" id="search-last-name" name="last_name" value="" placeholder="<?php echo $this->_defaultValues['lname']  ?>">
				<label class="screen-reader-text" for="chapter-search-input"><?php echo $this->_defaultValues['fname']  ?></label>
				<input type="search" id="search-member-id" name="member_id" value="" placeholder="<?php echo $this->_defaultValues['member_id']  ?>" >
				<select class="chapter" name="chapter" id="chapter">
					<option value=""><?php echo __('Select' , TEXT_DOMAIN); ?></option>
				    <?php 
				    	$chapters = $this->formatArrayForSelect($this->_chapters , true);
				    	foreach ($chapters as $key => $value) { 
				    ?>
						   <option value='<?php echo $key; ?>'> <?php echo $value; ?> </option>
					<?php } ?>
				</select>
				<input type="button" id="asg-search-submit" class="button" value="<?php echo __('Search'  , TEXT_DOMAIN ) ?>" onclick = "f.searchMembers();">
				<input type="button" id="chapter-clear" name="clear" value="<?php echo __('Clear'  , TEXT_DOMAIN ) ?>" onclick="clearMe();">
			</p>
			<table class="wp-list-table widefat fixed striped pages">
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo strtoupper($this->_defaultValues['member_id']) ?></th>
					<th><?php echo strtoupper($this->_defaultValues['name']) ?></th>
					<th><?php echo strtoupper($this->_defaultValues['chapter']) ?></th>
					<th ><?php echo strtoupper($this->_defaultValues['pdf']) ?></th>
					<th ><?php echo strtoupper($this->_defaultValues['edit']) ?></th>
					<th ><?php echo strtoupper($this->_defaultValues['delete']) ?></th>
				</thead>
				<tbody>
					<?php foreach ($this->_members as $key => $member) {  ?>
						<tr>
							<th scope="row" class="check-column">		
							<label class="screen-reader-text" for="cb-select-2"><?php echo __('Select Member' , TEXT_DOMAIN); ?></label>
							<input id="cb-select-2" type="checkbox" name="delete_member[]" value="<?php echo $member['id'] . '.' . $member['user_id']; ?>">
							</th>
							<td><?php echo $member['id']; ?></td>
							<td><?php echo ASG_Helper::getFullName($member); ?></td>
							<td><?php echo $member['chapter']; ?></td>
							<td><a href="<?php echo $this->_url . "&operation=pdf&id=". $member['id'] ?>" class="page-chapter-pdf " ><i class="fa fa-file-pdf-o"></i></a></td>
							<td><a href="<?php echo $this->_url . "&operation=edit&id=". $member['id'] ?>" class="page-chapter-action thickbox" data-operation = "edit"  data-id="<?php echo $member['id'] ?>" class="page-bod-action thickbox" onclick="f.setBodActionType(this);"><i class="fa fa-pencil-square-o"></i></a></td>
							<td><a href="<?php echo $this->_url . "&operation=delete&id=".  $member['id'] . '.' . $member['user_id']  ?>" onclick ="return confirm('Are you sure you want to Delete?')"><i class="fa fa-trash-o"></i></a></td>
						
					<?php } ?>
				</tbody>
				<thead>
					<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php echo __('Select All' , TEXT_DOMAIN); ?></label><input id="cb-select-all-1" type="checkbox"></td>
					<th><?php echo strtoupper($this->_defaultValues['member_id']) ?></th>
					<th><?php echo strtoupper($this->_defaultValues['name']) ?></th>
					<th><?php echo strtoupper($this->_defaultValues['chapter']) ?></th>
					<th ><?php echo strtoupper($this->_defaultValues['pdf']) ?></th>
					<th ><?php echo strtoupper($this->_defaultValues['edit']) ?></th>
					<th ><?php echo strtoupper($this->_defaultValues['delete']) ?></th>
				</thead>
			</table>
			</form>
			<div>
				<?php 
					$pagination = apply_filters('asg_display_pagination' , 'member' , $this->_pages ); 
				  	if(!empty($pagination)):
					?>
						<div id="asg-pagination">
						 <?php echo $pagination; ?>
						</div>
				<?php endif; ?>
			</div>	
		<?php
 	}

 	private function _displayForm() {
		?>
		<div id="wrapper">
			<?php echo apply_filters('asg_crud_message' , ''); ?>
			<div class="center">
				<h3> <?php echo __('Members Information' , TEXT_DOMAIN); ?> </h3>
			</div>	
			
			<form action="" method="POST" >	
				<?php foreach ($this->_formStructure as $key => $structure) {  
						$fieldSetClass = isset($structure['class']) ? $structure['class'] : 'row';
				?>
					<legend><?php echo $structure['legend'] ?></legend>
					<?php 
						if( $fieldSetClass != 'row')
							echo '<div><button class="asg-membership-enable">' . __( 'Unlock' , TEXT_DOMAIN ) . '</button></div>';
					?>
					<fieldset class="<?php echo $fieldSetClass; ?>">
					
					<?php foreach ($structure['fieldset'] as $key => $fields): ?>
						<div class="box">
						    <?php foreach ( $fields as $key => $field): ?>
							<div class="block">
								<?php 
								    $validators  = !empty($field['validator']) ? $field['validator'] : '';
								    $class = !empty($field['class']) ? esc_attr( sanitize_html_class($field['class'])) : '';
									$event = !empty($field['onChange']) ? 'onchange = "' . $field['onChange'] . '"' : '';
									$error = ASG_Helper::getErrors( $this->_errors , $field['name']);
									$html  = '';
									switch ($field['type']) {
										case 'text':
											$html .= '<label for="">' . __( $field['label'] ) . '</label>';
											$html .= isset($field['wrapper']) && $field['wrapper'] ? '<div id="'. $field['wrapper_id'] .'">' : '';
											$html .= '<input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="' . $field['value'] . '" class="' . $class . '" '. $validators .'>
												  	';
											$html .=  isset($field['wrapper']) && $field['wrapper'] ? '</div>' : '';
											break;
										case 'checkbox':
											$checked =  $field['value'] == 1 ? 'checked' : '';
											$html .= '<label for="">' . __( $field['label'] ) . '</label>
												  	<input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" class = "'. $field['class'] .'" value= "'. $field['value'] .'" '. $checked .'>';
											
											break;

										case 'textarea':
											$html .= '<label for="">' . __( $field['label'] ) . '</label>
												  	<textarea  name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" cols="22" rows="3">' .  $field['value'] . '</textarea>';
												  
											break;
										case 'select':
											$html .= '<label for="">' . __( $field['label'] ) . '</label>';
											$html .=  isset($field['wrapper']) && $field['wrapper'] ? '<div id="'. $field['wrapper_id'] .'">' : '';
											$html .= '<select class="'. $field['class'] .'" name="'. $field['model'] . '[' . $field['name'] . ']"  id="'. $field['id'] .'" ' . $event . '>';
													 $html .= '<option value="">'. __('Select' , TEXT_DOMAIN) .'</option>';
													 foreach ($field['value'] as $key => $value) {
													   $key = $field['insert_not_key'] == true ? $value : $key ;
													   $selected = ($key == $field['selected_value']) ? 'selected="selected"' : '';
													   $html .= '<option value= "' . $key . '"  '. $selected .'>' . $value . '</option>';
													 }
											$html .= '</select>';
											$html .=  isset($field['wrapper']) && $field['wrapper'] ? '</div>' : '';
											break;
										
										default:
											$html .= '<label for="">' . __( $field['label'] ) . '</label>
												  	<input type="' . $field['type'] . '" name="' . $field['model'] . '[' . $field['name'] . ']" id="'. $field['id'] .'" value="" class="regular-text" '. $validators .'>';
											break;
									}
									$html .= !empty($error)  ? '<p class="alert alert-danger">'. $error .'</p>' : '';
									echo $html;
									?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
					</fieldset>	
				<?php } ?>
				<!--row-->
				<div >
					<div class="box">
						<div class="block">
							<input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo wp_create_nonce( 'asg_action_nonce'); ?>" />
							<input type="submit" value="<?php echo $this->_defaultValues['save']; ?> " class="button button-primary" name="save">
							<a href="<?php echo $this->_url; ?> " class="button button-primary" ><?php echo $this->_defaultValues['cancel']; ?></a>
							<input type="hidden" name="page" value="asg-admin-sub-page2" />
							<input type="hidden" name="user_id" value="<?php echo $this->_userId; ?>" />
							<input type="hidden" name="save_member" value="<?php echo $_REQUEST['operation'] ?>" />
						</div>
					</div>
				</div>		
			</form>
		</div>
		<?php
	}


	public function _setFormStructure() {
		$this->_formStructure = array (
								 	array (
									 	'legend' => __('Personal Info:' , TEXT_DOMAIN ),
									 	'fieldset' => array(
									 					'box1' => array(
										 					array(
																'label' =>  __( 'First Name:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'first_name',
																'model' => 'member',
																'for'   => 'first_name',
																'value' => $this->_giveMeValueIfSet('first_name'),
																'id'    => 'first_name',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Middle Name:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'middle_name',
																'model' => 'member',
																'for'   => 'middle_name',
																'value' => $this->_giveMeValueIfSet('middle_name'),
																'id'    => 'middle_name',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Last Name:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'last_name',
																'model' => 'member',
																'for'   => 'last_name',
																'value' => $this->_giveMeValueIfSet('last_name'),
																'id'    => 'last_name',
																'validator'    => ''
															)
														),
														'box2' => array(
										 					array(
																'label' => __( 'Address:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'address',
																'model' => 'member',
																'for'   => 'address',
																'value' => $this->_giveMeValueIfSet('address'),
																'id'    => 'address',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Country:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' =>  $this->formatArrayForSelect(apply_filters('asg_get_countries',array())),
																'name'	=> 'country',
																'model' => 'member',
																'for'   => 'country_id',
																'selected_value' => $this->_giveMeValueIfSet('country'),
																'id'    => 'country_id',
																'insert_not_key' => true,
																'wrapper' => true,
																'wrapper_id' => 'asg-country',
																'class' => 'dyn-state',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'State:' , TEXT_DOMAIN ),
																'type'	=> $this->_isCountryUS ? 'select' : 'text',
																'value' => $this->_isCountryUS ? $this->formatArrayForSelect(apply_filters('asg_get_states',array())): $this->_giveMeValueIfSet('state'),
																'name'	=> 'state',
																'model' => 'member',
																'for'   => 'state',
																'selected_value' => $this->_giveMeValueIfSet('state'),
																'id'    => 'state_id',
																'class' => 'dyn-city',
																'insert_not_key' => true,
																'wrapper' => true,
																'wrapper_id' => 'asg-state',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'City:' , TEXT_DOMAIN ),
																'type'	=> $this->_isCountryUS ? 'select' : 'text',
																'name'	=> 'city',
																'model' => 'member',
																'for'   => 'city',
																'value' =>  $this->_isCountryUS ? $this->formatArrayForSelect(apply_filters('asg_get_cities',array())): $this->_giveMeValueIfSet('city'),
																'selected_value' => $this->_giveMeValueIfSet('city'),
																'id'    => 'city_id',
																'class' => 'city_id',
																'insert_not_key' => true,
																'wrapper' => true,
																'wrapper_id' => 'asg-city',
																'validator'    => ''
															),															
															array(
																'label' =>  __( 'Zip:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'zip',
																'model' => 'member',
																'for'   => 'zip',
																'value' => $this->_giveMeValueIfSet('zip'),
																'id'    => 'zip',
																'validator'    => ''
															),
															
														),
														'box3' => array(
															array(
																'label' => __( 'Home#:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'home_phone',
																'model' => 'member',
																'for'   => 'home_phone',
																'value' => $this->_giveMeValueIfSet('home_phone'),
																'id'    => 'home_phone',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Cell#:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'cell_phone',
																'model' => 'member',
																'for'   => 'cell_phone',
																'value' => $this->_giveMeValueIfSet('cell_phone'),
																'id'    => 'cell_phone',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Work#:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'work_phone',
																'model' => 'member',
																'for'   => 'work_phone',
																'value' => $this->_giveMeValueIfSet('work_phone'),
																'id'    => 'work_phone',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Ext:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'ext',
																'model' => 'member',
																'for'   => 'ext',
																'value' => $this->_giveMeValueIfSet('ext'),
																'id'    => 'ext',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Email:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'user_email',
																'model' => 'user',
																'for'   => 'user_email',
																'value' => $this->_giveMeValueIfSet('user_email'),
																'id'    => 'user_email',
																'validator'    => ''
															)
														)
														
								 				)
										),
									array (
									 	'legend' => __('Membership Info:' , TEXT_DOMAIN ),
									 	'fieldset' => array(
									 					'box1' => array(
												 					array(
																		'label' => __( 'Chapter:' , TEXT_DOMAIN ),
																		'type'	=> 'select',
																		'value' => $this->formatArrayForSelect($this->_chapters),
																		'name'	=> 'chapter_id',
																		'model' => 'member',
																		'for'   => 'chapter_id',
																		'selected_value' => $this->_giveMeValueIfSet('chapter_id'),
																		'id'    => 'chapter_id',
																		'validator'    => ''
																	),
																),
														'box2' => array(
										 					array(
																'label' => __( 'Title 1:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => $this->_title1,
																'name'	=> 'title1_id',
																'model' => 'member',
																'for'   => 'title1_id',
																'selected_value' => $this->_giveMeValueIfSet('title1_id'),
																'id'    => 'title1_id',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Title 2:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => $this->_title2,
																'name'	=> 'title2_id',
																'model' => 'member',
																'for'   => 'title2_id',
																'selected_value' => $this->_giveMeValueIfSet('title2_id'),
																'id'    => 'title2_id',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Title 3:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => $this->_title3,
																'name'	=> 'title3_id',
																'model' => 'member',
																'for'   => 'title3_id',
																'selected_value' => $this->_giveMeValueIfSet('title3_id'),
																'id'    => 'title3_id',
																'validator'    => ''
															),
														),
														'box3' => array(
										 					array(
																'label' => __( 'Sale:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => array('N','Y'),
																'name'	=> 'sale_no_sale',
																'model' => 'member',
																'for'   => 'sale_no_sale',
																'selected_value' => $this->_giveMeValueIfSet('sale_no_sale'),
																'id'    => 'sale_no_sale',
																'validator'    => ''
															),
														),
								 					)
										),
									array (
									 	'legend' => __('' , TEXT_DOMAIN ),
									 	'fieldset' => array(
									 					'box1' => array(
										 					array(
																'label' => __( 'Status:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => $this->_status,
																'name'	=> 'status',
																'model' => 'member',
																'for'   => 'status',
																'selected_value' => $this->_giveMeValueIfSet('status'),
																'id'    => 'status',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Complimentary:' , TEXT_DOMAIN ),
																'type'	=> 'checkbox',
																'name'	=> 'complimentary',
																'class' => 'asg-checkbox',
																'model' => 'member',
																'for'   => 'complimentary',
																'value' => $this->_giveMeValueIfSet('complimentary'),
																'id'    => 'complimentary',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Payment Type:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => $this->_paymentType,
																'name'	=> 'payment_type',
																'model' => 'member',
																'for'   => 'payment_type',
																'selected_value' => $this->_giveMeValueIfSet('payment_type'),
																'id'    => 'payment-type',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Auto Renewal:' , TEXT_DOMAIN ),
																'type'	=> 'checkbox',
																'name'	=> 'auto_renewal',
																'model' => 'member',
																'for'   => 'auto_renewal',
																'value' => $this->_giveMeValueIfSet('auto_renewal'),
																'id'    => 'auto_renewal',
																'class' => 'asg-checkbox',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'check/Mo # ' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'check_no',
																'model' => 'member',
																'for'   => 'check_no',
																'value' => $this->_giveMeValueIfSet('check_no'),
																'id'    => 'check_no',
																'validator'    => ''
															),
														),
														'box2' => array(
										 					array(
																'label' => __( 'Comments:' , TEXT_DOMAIN ),
																'type'	=> 'textarea',
																'name'	=> 'comments',
																'model' => 'member',
																'for'   => 'comments',
																'value' => $this->_giveMeValueIfSet('comments'),
																'id'    => 'comments',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Source:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => $this->_sources,
																'name'	=> 'source',
																'model' => 'member',
																'for'   => 'source',
																'selected_value' => $this->_giveMeValueIfSet('source'),
																'id'    => 'source',
																'validator'    => ''
															)
														)
													)
										),
									array (
									 	'legend' => __('MEMBERSHIP FEES' , TEXT_DOMAIN ),
									 	'fieldset' => array(
										 				'box1' => array(
										 					array(
																'label' => __( 'New' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'new',
																'model' => 'member',
																'for'   => 'new',
																'value' => $this->_giveMeValueIfSet('new'),
																'id'    => 'new',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Renewal:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'renewal',
																'model' => 'member',
																'for'   => 'renewal',
																'value' => $this->_giveMeValueIfSet('renewal'),
																'id'    => 'renewal',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Donation:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'donation',
																'model' => 'member',
																'for'   => 'donation',
																'value' => $this->_giveMeValueIfSet('donation'),
																'id'    => 'donation',
																'validator'    => ''
															),
															array(
																'label' =>  __('Compl:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'compl',
																'model' => 'member',
																'for'   => 'compl',
																'value' => $this->_giveMeValueIfSet('compl'),
																'id'    => 'compl',
																'validator'    => ''
															),
															array(
																'label' => __('Late:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'late',
																'model' => 'member',
																'for'   => 'late',
																'value' => $this->_giveMeValueIfSet('late'),
																'id'    => 'late',
																'validator'    => ''
															),
														),
														'box2' => array(
										 					array(
																'label' => __( 'Join Date:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'join_date',
																'model' => 'member',
																'for'   => 'join_date',
																'value' => $this->getDatetimeFormat($this->_giveMeValueIfSet('join_date')),
																'id'    => 'join_date',
																'class' => 'jquery-datepicker',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Paid Thru:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'paid_thru',
																'model' => 'member',
																'for'   => 'paid_thru',
																'value' => $this->getDatetimeFormat($this->_giveMeValueIfSet('paid_thru')),
																'id'    => 'paid_thru',
																'validator'    => ''
															),
															array(
																'label' =>  __('Transfer Date:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'transfer_date',
																'model' => 'member',
																'for'   => 'transfer_date',
																'value' => $this->getDatetimeFormat($this->_giveMeValueIfSet('transfer_date')),
																'id'    => 'transfer_date',
																'class' => 'jquery-datepicker',
																'validator'    => ''
															),
															array(
																'label' => __('Transform From:' , TEXT_DOMAIN ),
																'type'	=> 'select',
																'value' => $this->_chapters,
																'name'	=> 'transfer_from',
																'model' => 'member',
																'for'   => 'transfer_from',
																'selected_value' => $this->_giveMeValueIfSet('transfer_from'),
																'id'    => 'transfer_from',
																'validator'    => ''
															)
														),
														'box3' => array(
										 					array(
																'label' => __( 'Last Updated:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'updated',
																'model' => 'member',
																'for'   => 'updated',
																'value' => $this->getDatetimeFormat($this->_giveMeValueIfSet('updated')),
																'id'    => 'updated',
																'class' => 'jquery-datepicker',
																'validator'    => ''
															),
															array(
																'label' =>  __( 'Updated By:' , TEXT_DOMAIN ),
																'type'	=> 'text',
																'name'	=> 'updated_by',
																'model' => 'member',
																'for'   => 'updated_by',
																'value' => $this->_giveMeValueIfSet('updated_by'),
																'id'    => 'updated_by',
																'validator'    => ''
															)
														)
								 					),
										'class' => 'row membership-fees'
										),
										
								);
	}

	/**
	 *
	 *	This method is used to check for wheather or 
	 *  not value is set
	 *  @access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function _giveMeValueIfSet( $key ) {
		if(empty($this->_memberInfo) && count($this->_memberInfo) <= 0)
			return '';
		return array_key_exists($key, $this->_memberInfo) ? $this->_memberInfo[$key] : '';
	}

	
	/**
	 *
	 *	This method is used for formatting array
	 * 	@access public
	 *  @param $opt = true results get array (key => value) 
	 *  or  false (value=> value)
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function formatArrayForSelect ($data , $opt = false ) { 
		$customArr = array();
		if(!is_array($data))
			return false;
		foreach ($data as $key => $value) {
			$v = array_pop($value);
			$k = array_pop($value);
			if($opt)
				$customArr[$k] = $v;
			else 
				$customArr[$v] = $v;
		}
		return $customArr;
	}

	/**
	 *
	 *	This method is used to get date and time format
	 *  according to the setting of wordpress 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public function getDatetimeFormat ($datetime) {
		$dateFormat = get_option('date_format');
		$timeFormat = get_option('time_format');
		return mysql2date( " Y-m-d " , $datetime );
	}
 }

new ASG_Member();