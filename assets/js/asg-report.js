// report handling js file
(function($){
	 chimpySearch = {
		options: function() {
			
		},
		search: function() {
			name = { handle:'element' };
		},
		buildHtml: function(members) {
				var html = '';
				for(var i in members['rows']) {
					member = members['rows'][i];
					sync_stat = member.sync_status == 0 ? 'Pending' : 'Approved';
					attr = member.attribute == 'undefined' ? false : true;
					html += '<tr><th scope="row" class="check-column">';
					html += '<input id="cb-select-2" type="checkbox" name="sync_member[]" value="'+ member.id +'"></th>';
					html += '<td>' +  member.id + '</td>';
					html += '<td>' +  member.first_name + ' ' + member.middle_name + ' '+ member.last_name + '</td>';
					html += '<td>' +  member.user_email + '</td>';
					html += '<td>' +  attr + '</td>';
					html += '<td>' +  member.transfer_from + '</td>';
					html += '<td>' +  member.transfer_to + '</td>';
					html += '<td>' +  member.updated_by + '</td>';
					html += '<td>' + sync_stat + '</td>';
					html += '<td>' + member.action + '</td>';
					html += '</tr>';
				} 
			return html;
		},
		ajaxSearch: function(formValue) {
			$.ajax({
				url: ajaxurl,
				method: "POST",
				data: { form_value:formValue,action:'asg_search_mailchimp_member' },
				success : function(responseText) {
					responObj = $.parseJSON(responseText);
					if( responObj.error == '' ) {
						//html = this.buildHtml(responObj);
						html = '';
						for(var i in responObj['rows']) {
							member = responObj['rows'][i];
							sync_stat = member.sync_status == 0 ? 'Pending' : 'Approved';
							attr = member.attribute == 'undefined' ? false : true;
							action = member.action != 'undefined'  ? member.action : 'CREATED';
							html += '<tr><th scope="row" class="check-column">';
							html += '<input id="cb-select-2" type="checkbox" name="sync_member[]" value="'+ member.id +'"></th>';
							html += '<td>' +  member.id + '</td>';
							html += '<td>' +  member.first_name + ' ' + member.middle_name + ' '+ member.last_name + '</td>';
							html += '<td>' +  member.user_email + '</td>';
							html += '<td>' +  attr + '</td>';
							html += '<td>' +  member.transfer_from + '</td>';
							html += '<td>' +  member.transfer_to + '</td>';
							html += '<td>' +  member.updated_by + '</td>';
							html += '<td>' + sync_stat + '</td>';
							html += '<td>' +  action + '</td>';
							html += '</tr>';
						} 
						jQuery('tbody').find('tr').remove();
						jQuery('tbody').append(html);
						jQuery('#asg-pagination').remove();
		 			} else {
		 				jQuery('tbody').find('tr').remove();
		 				jQuery('tr.ajax-message').remove();
		 				jQuery('tbody').append('<tr class="ajax-message"><td colspan="9">'+ responObj.error +'</td></tr>');
		 				jQuery('#asg-pagination').remove();
							 				
		 			}
				}
			});
		}
	}
	$('input#asg-start-sync').on('click' , function() {
		listId = $('select.list-id').find(":selected").val();
		$('input#asg-list-id').val(listId);
		$('form#asg-mailchimp-sync').submit();
	});

	// search members
	$('input#asg-mailchimp-search').on('click' , function() {
		formValue = $('#mailchimp-search').serialize();
		chimpySearch.ajaxSearch(formValue);
	});

	$('select#asg-monthly-report').on('change' , function() {
			var endDateElem = $('input[name="end_date"]'),
				startDateElem = $('input[name="start_date"]'),
				autoRenewalElem = $('select[name="auto_renewal_opt"]'),
				endDateParentElem = getParent(endDateElem , 'tr'),
				startDateParentElem =getParent(startDateElem , 'tr'),
				autoRenewalParentElem = getParent(autoRenewalElem , 'tr');
				selected = $(this).find(':selected').val();
				startDateElem.val('');
				endDateElem.val('');
				autoRenewalParentElem.val('');
				endDateParentElem.hide();
				startDateParentElem.hide();
				autoRenewalParentElem.hide();
				switch(selected) {
					case '7':
						endDateParentElem.show();
						break;
					case '8':
						endDateParentElem.show();
						autoRenewalParentElem.show();
						break;
					case '9':
						endDateParentElem.show();
						break;
					default:
						autoRenewalElem.val('');
						startDateParentElem.show();
						endDateParentElem.show();
						break;
				}
		});
	

})(jQuery);