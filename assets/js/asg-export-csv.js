function exportTableToCSV($table, filename) { 
    var $rows = jQuery($table[0]).find('tr:has(td)'),
        $header = jQuery($table[0]).find('tr:has(th)'),
        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',
        // Grab text from table into CSV formatted string
        csv = '"';
        csv += $header.map(function (i, row) {
                var $row = jQuery(row),
                    $cols = $row.find('th');

                return $cols.map(function (j, col) {
                    var $col = jQuery(col),
                        text = $col.text();

                return text.replace('"', '""'); // escape double quotes

                }).get().join(tmpColDelim);
            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim);
        
        csv += rowDelim;
        csv += $rows.map(function (i, row) {
            var $row = jQuery(row),
                $cols = $row.find('td');
                console.log(i);
            return $cols.map(function (j, col) {
                var $col = jQuery(col),
                    text = $col.text();
                return text.replace('"', '""'); // escape double quotes
            }).get().join(tmpColDelim);
        }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"',

        // Data URI
        csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
        
        jQuery(this)
            .attr({
            'download': filename,
            'href': csvData,
            'target': '_blank'
        });
}


function getAllSiblings(elem, filter) {
    var sibs = [];
    elem = elem.parentNode.firstChild;
    do {
        if (!filter || filter(elem)) sibs.push(elem);
    } while (elem = elem.nextSibling)
    return sibs;
}

function exportParticularCsv(elem ) {
    sibs = jQuery(elem).siblings('table');
    name = jQuery('li.ui-state-active a').text();
    exportTableToCSV.apply(elem, [sibs, name +'.csv']);
}

function exportPrintLabelCsv(elem , name) {
    tbl = jQuery('table#print-label-table');
    exportTableToCSV.apply(elem, [tbl, name +'.csv']);
}

(function($) {
    $('.all').live('click' , function() { 
        exportTableToCSV.apply(this, [$('table.report'), 'report.csv']);
    });
    /*$('div#tabs').find('a').attr('onclick','exportParticularCsv(this);');*/
})(jQuery);
    




 

