(function($,pdf) {
	var html = jQuery.parseJSON(pdf.html);
		html = html.replace(/,/g , ',');
		
	$('a.asg-card-pdf').live( 'click' , function(e){
		e.preventDefault();
		
		var pdf = new jsPDF();
		// we support special element handlers. Register them with jQuery-style 
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors 
        // (class, of compound) at this time.
        specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function(element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };
        margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
                html, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, {// y coord
                    'width': margins.width, // max width of content on PDF
                    'elementHandlers': specialElementHandlers
                },
        function(dispose) {
            // dispose: object with X, Y of the last line add to the PDF 
            //          this allow the insertion of new lines after html
            pdf.output('dataurlnewwindow');     //opens the data uri in new window
            //pdf.save('asg-membership-card.pdf');
           
        }
        , margins);
    });
})(jQuery,pdf);