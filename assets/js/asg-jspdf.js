(function($) {
   /* $.fn.reportPdf = function() {
    }*/
	var elem = $("#fee-report-form");
	if(elem.length > 0){
		elem.submit(function(event) {
		  	event.preventDefault();
		  	$('p.no-result').remove();
		  	$('div#fee-total-report').remove();
		  	$('table.report').remove();
		  	/*$('div#report #title').remove();
			$('table.report').remove();*/
		    formValue = $("#fee-report-form").serialize();
		    jQuery.ajax({
				url: ajaxurl,
				method: "POST",
				data: { form_value:formValue,action:'asg_report' },
				success : function(responseText) {
					var html = '';
					responObj = jQuery.parseJSON(responseText);
					if(responObj.result.length == 0) {
						html = '<p class="no-result">No Result Found</p>'
						$('div #report').append(html);
						return false;
					}
					errors = responObj.errors;
					if( errors == '') {
						date = responObj.date;
						results = responObj.result;
						reportType = $('select.fee-total-report').find(":selected").val();
						html = reportType == 1 ?  buildIndividualReport(results,date) : buildMemberReport(results[0],date);
						//$('table.report').remove();
						$('div #report').append(html);
						/*var pdf = new jsPDF();
						// we support special element handlers. Register them with jQuery-style 
			            // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
			            // There is no support for any other type of selectors 
			            // (class, of compound) at this time.
			            specialElementHandlers = {
			                // element with id of "bypass" - jQuery style selector
			                '#bypassme': function(element, renderer) {
			                    // true = "handled elsewhere, bypass text extraction"
			                    return true
			                }
			            };
			            margins = {
			                top: 80,
			                bottom: 60,
			                left: 40,
			                width: 522
			            };
			            // all coords and widths are in jsPDF instance's declared units
			            // 'inches' in this case
			            pdf.fromHTML(
			                    html, // HTML string or DOM elem ref.
			                    margins.left, // x coord
			                    margins.top, {// y coord
			                        'width': margins.width, // max width of content on PDF
			                        'elementHandlers': specialElementHandlers
			                    },
			            function(dispose) {
			                // dispose: object with X, Y of the last line add to the PDF 
			                //          this allow the insertion of new lines after html
			                pdf.output('dataurlnewwindow');     //opens the data uri in new window
			               
			            }
			            , margins);
*/

		 			} else {
		 				
		 			}
				}

			});
		});
	}

	$('a.asg-pdf').live( 'click' , function(e){
		e.preventDefault();
       	var pdf = new jsPDF('p','pt','a4');
		var tblContent = $(this).parent().siblings('table.report').get(0);
		title = $('#title h1').text();
		date = $('#title p').text();
		header = title != '' ? title + "\t\t\t" + "Date :" + date : '';
		var res = pdf.autoTableHtmlToJson(tblContent);
		var margins = {
		   top: 25,
		   bottom: 60,
		   left: 20,
		   width: 522
		};
		pdf.text(40 , 40 , header );
		pdf.autoTable(res.columns, res.data, {
		    startY: 60,
		    styles: {
		      overflow: 'linebreak',
		      fontSize: 10,
		      rowHeight: 30,
		      columnWidth: 'wrap'
		    },
		    columnStyles: {
		      1: {columnWidth: 'auto'}
		    }
		 });
		 pdf.output('dataurlnewwindow');
    });

    

    
}(jQuery));


// membership fee report form for all member
function buildMemberReport(responObj , date) {
	totalNew = responObj.new == null ? 0.00 : responObj.new;
	totalRenewal = responObj.renewal== null ? 0.00 : responObj.renewal;
	totalLate = responObj.late == null ? 0.00 : responObj.late;
	totalComplimentary = responObj.complimentary == null ? 0.00 : responObj.complimentary;
	totalDonation =  responObj.donation == null ? 0.00 : responObj.donation;
	total =  responObj.total == null ? 0.00 : responObj.total;
	html = '';
	html = '<div id="fee-total-report">';
	html += '<div id="asg-download-opt"><a href="#" class="all"><i class="fa fa-file-pdf-o"></i></a>|<a href="#" class="asg-pdf"><i class="fa fa fa-table"></i></a></div>';
	html += '<table class="wp-list-table widefat fixed striped stock report"><thead><td>Membership Fee Totals Reports</td><td>'+  date +'</td></thead>';
	html += '<tbody><tr><td>New</td><td>'+  totalNew   +'</td></tr>';
	html += '<tr><td>Renewal</td><td>'+totalRenewal  +'</td></tr>';
	html += '<tr><td>Late</td><td>'+ totalLate   +'</td></tr>';
	html += '<tr><td>Complimentary</td><td>'+ totalComplimentary  +'</td></tr>';
	html += '<tr><td>Donations</td><td>'+ totalDonation +'</td></tr>';
	html += '<tr><td>Total</td><td>'+ total +'</td></tr>';
	html += '</tbody></table></div>';
	return html;
}

// membership fee report form for individual
function buildIndividualReport(responObj , date) {
	heads = ['Id','Last Name','Middle Name','First Name','Renewal Fees','New Fees','Late Fees','Complimentary Fees','Donations'];
	html = '';
	html = '<div id="fee-total-report">';
	html += '<div id="asg-download-opt"><a href="#" class="all">EXPORT TO CSV</a>|<a href="#" class="asg-pdf">EXPORT TO PDF</a></div>';
	html += '<div id="title"><h1>Individual Members Fee Details</h1><p>'+ date +'</p></div>';
	html += '<table class="wp-list-table widefat fixed striped stock report"><thead>';
	for(i in heads) {
		html += '<td>'+ heads[i] +'</td>';
	}
	html += '</thead><tbody>';
	
	for(i in responObj) {
		html += '<tr><td>'+ responObj[i].id  +'</td>';
		html += '<td>'+ responObj[i].last_name  +'</td>';
		html += '<td>'+ responObj[i].middle_name  +'</td>';
		html += '<td>'+ responObj[i].first_name  +'</td>';
		html += '<td>'+ responObj[i].renewal  +'</td>';
		html += '<td>'+ responObj[i].new +'</td>';
		html += '<td>'+ responObj[i].late  +'</td>';
		html += '<td>'+ responObj[i].complimentary  +'</td>';
		html += '<td>'+ responObj[i].donation  +'</td></tr>';
	}
	html += '</tbody></table></div>';
	return html;
}