/**
 * 
 * Handles the form
 * @class      FormHandler
 * @copyright   Copyright (c) 2015, Neil
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
*/

function FormHandler () {
	
}

/**
 * This is  porotype used to set operation
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

FormHandler.prototype.setActionType = function(elem){
	clearMe();
	operation = elem.getAttribute('data-operation');

	titleId = elem.getAttribute('data-id');
	setterElem = document.getElementById("operation");
	idSetterElem = document.getElementById("titleId");
	titleElem = document.getElementById("title");
	setterElem.value = operation;
	idSetterElem.value = titleId;
	if(operation == 'edit') {
		titleElem.value = elem.getAttribute('data-value');
	}
	
}

/**
 * This is  porotype used to set operation
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

FormHandler.prototype.setChapterActionType = function(elem){ 
	operation = elem.getAttribute('data-operation');
	chapterId = elem.getAttribute('data-id');
	chapterId = (chapterId != null) ? chapterId : '';
	setterElem = document.getElementById("operation");
	idSetterElem = document.getElementById("chapter_id");
	chapterElem = document.getElementById("chapter");
	setterElem.value = operation;
	idSetterElem.value = chapterId;
	if(operation == 'edit') {
		currentChapterInfo = getChapterInfo(chapterId);
		for(var x in currentChapterInfo) {
		   if(x == 'member_id') {
				//console.log(currentChapterInfo[x]);
		   	    selectElement = jQuery('select.president option[value=' + currentChapterInfo[x] +']');
		   	    selectElement.prop('selected', true);
		   	   // setOption(,currentChapterInfo[x]);
         	} else if ( x == 'state' ) {
         		selectElement = jQuery('select.state option[value=' + currentChapterInfo[x] +']');
		   	    selectElement.prop('selected', true);
         	} 
			jQuery('#'+x).val(currentChapterInfo[x]);
		}
	}
	//chapterElem.value = value;
}





/**
 * This is  porotype used for search functionality
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

FormHandler.prototype.searchMembers = function(){
			first_name = jQuery('#search-first-name').val();
			last_name = jQuery('#search-last-name').val();
			id = jQuery('#search-member-id').val();
			chapter_id = jQuery('#chapter').find(":selected").val();
			jQuery('table.asg-chapter thead').find('td').show();
			jQuery.ajax({
			url: ajaxurl,
			method: "POST",
			data: { first_name:first_name,last_name:last_name,id:id,chapter_id:chapter_id,action:'search_member' },
			success : function(responseText) {
				responObj = jQuery.parseJSON(responseText);
				errors = responObj.error;
				if( errors == '' ) {
					var html = '';
					
					for(var i in responObj['rows']) {
						member = responObj['rows'][i];
						html += '<tr>';
						html += '<th scope="row" class="check-column"><input id="cb-select-2" type="checkbox" name="delete_member[]" value="'+ member.id +'"></th>';
						html += '<td>' +  member.id + '</td>';
						html += '<td>' +  member.first_name + ' ' + member.middle_name + ' '+ member.last_name + '</td>';
						html += '<td>' +  member.chapter + '</td>';
						html += '<td><a href="'+ responObj.url + '&operation=pdf&id=' + member.id + '" class="page-chapter-pdf " ><i class="fa fa-file-pdf-o"></i></a></td>';
						html += '<td><a href="'+ responObj.url + '&operation=edit&id=' + member.id + '" class="page-chapter-action thickbox" data-operation = "edit"  data-id="'+  member.id + '" class="page-bod-action thickbox" onclick="f.setBodActionType(this);"><i class="fa fa-pencil-square-o"></i></a></td>';
						html += '<td><a href="'+ responObj.url + '&operation=delete&id=' + member.id + '" class="page-chapter-action thickbox" data-operation = "delete"  data-id="'+ member.id + '" class="page-bod-action thickbox"><i class="fa fa-trash-o"></i></a></td>';
						html += '<td></td>';
						html += '</tr>';
						
					} 
					jQuery('#asg-pagination').remove();
					jQuery('tbody').find('tr').remove();
					jQuery('tbody').append(html);
	 			} else {
	 				jQuery('#asg-pagination').remove();
	 				jQuery('table.asg-chapter thead').find('td').hide();
	 				jQuery('tbody').find('tr').remove();
	 				jQuery('tr.ajax-message').remove();
	 				jQuery('tbody').append('<tr class="ajax-message"><td colspan="6">'+ errors +'</td></tr>');
				}
			}
		});
		//chapterElem.value = value;
}


/**
 * This is  porotype used for search functionality
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

jQuery('#search-submit').live('click' , function(){
			jQuery('table.asg-chapter thead').find('td').show();
			chapter = jQuery('#chapter-search-input').val();
			jQuery.ajax({
			url: ajaxurl,
			method: "POST",
			data: { chapter:chapter,action:'search_chapter' },
			success : function(responseText) {
				responObj = jQuery.parseJSON(responseText);
				imgLoader = new Image();// preload image
				imgLoader.src = tb_pathToImage;
				errors = responObj.error;
				if( errors == '' ) {
					
					var html = '';
					for(var i in responObj['rows']) {
						chapter = responObj['rows'][i];
						html += '<tr>';
						html += '<th scope="row" class="check-column"><input id="cb-select-2" type="checkbox" name="delete_chapter[]" value="'+ chapter.id +'"></th>';
						html += '<td>' +  chapter.chapter + '</td>';
						html += '<td>' +  chapter.first_name + ' ' + chapter.middle_name + ' '+ chapter.last_name + '</td>';
						html += '<td>' +  chapter.member_id + '</td>';
						html += '<td><a href="'+ responObj.url + '#TB_inline?width=600&height=550&inlineId=my-content-id" class="page-chapter-action thickbox" data-operation = "edit"  data-id="'+  chapter.id + '" class="page-bod-action thickbox" onclick="f.setChapterActionType(this);"><i class="fa fa-pencil-square-o"></i></a></td>';
						html += '<td><a href="'+ responObj.url + '&operation=edit&id=' + chapter.id + '" class="page-chapter-action thickbox" data-operation = "delete"  data-id="'+ chapter.id + '" class="page-bod-action thickbox" ><i class="fa fa-trash-o"></i></a></td>';
						html += '<td></td>';
						html += '</tr>';
						
					} 
					jQuery('table.asg-chapter tbody').find('tr').remove();
					jQuery('table.asg-chapter tbody').append(html);
					jQuery('#asg-pagination').remove();
					// ThickBox - this allows any dynamically created links that use thickbox to work!
					//tb_init('a.thickbox');//pass where to apply thickbox
	 			} else {
	 				jQuery('#asg-pagination').remove();
	 				jQuery('table.asg-chapter thead').find('td').hide();
	 				jQuery('table.asg-chapter tbody').find('tr').remove();
	 				jQuery('table.asg-chapter tr.ajax-message').remove();
	 				jQuery('table.asg-chapter tbody').append('<tr class="ajax-message"><td colspan = "6">'+ errors +'</td></tr>');
	 				
	 			}
			}
		});
		//chapterElem.value = value;
});

/*jQuery('#chapter-search-input').live('keyup' , function(){
		chapter = jQuery('#chapter-search-input').val();
		jQuery.ajax({
		url: ajaxurl,
		method: "POST",
		data: { chapter:chapter,action:'search_chapter' },
		success : function(responseText) {
			responObj = jQuery.parseJSON(responseText);
			imgLoader = new Image();// preload image
			imgLoader.src = tb_pathToImage;
			errors = responObj.error;
			if( errors == '' ) {
				var html = '';
				for(var i in responObj['rows']) {
					chapter = responObj['rows'][i];
					html += '<tr><td>' +  chapter.chapter; + '</td>';
					html += '<td>' +  chapter.first_name; + '</td>';
					html += '<td>' +  chapter.member_id + '</td>';
					html += '<td><a href="'+ responObj.url + '#TB_inline?width=600&height=550&inlineId=my-content-id" class="page-chapter-action thickbox" data-operation = "edit"  data-id="'+  chapter.id + '" class="page-bod-action thickbox" onclick="f.setChapterActionType(this);">Edit</a></td>';
					html += '<td><a href="'+ responObj.url + '&operation=edit&id=' + chapter.id + '" class="page-chapter-action thickbox" data-operation = "delete"  data-id="'+ chapter.id + '" class="page-bod-action thickbox" >Delete</a></td>';
					html += '</tr>';
					
				} 
				jQuery('tbody').find('tr').remove();
				jQuery('tbody').append(html);
				// ThickBox - this allows any dynamically created links that use thickbox to work!
				//tb_init('a.thickbox');//pass where to apply thickbox
				} else {
					jQuery('tbody').find('tr').remove();
					jQuery('tr.ajax-message').remove();
					jQuery('tbody').append('<tr class="ajax-message"><td colspan = "4">'+ errors +'</td></tr>');
					
				}
		}
	});
		//chapterElem.value = value;
});*/





/**
 * This is  porotype used to set operation for BOD
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

FormHandler.prototype.setBodActionType = function(elem){
	clearMe();
	operation = elem.getAttribute('data-operation');
	bodId = elem.getAttribute('data-id');
	bodId = (bodId != null) ? bodId : '';
	setterElem = document.getElementById("operation");
	idSetterElem = document.getElementById("bod-id");
	bodElem = document.getElementById("bod");
	setterElem.value = operation;
	idSetterElem.value = bodId;
	if(operation == 'edit') {
		currentBODInfo = getBODInfo(bodId);
		for(var x in currentBODInfo) {
		   if(x == 'member_id') {
				//console.log(currentChapterInfo[x]);
		   	    selectElement = jQuery('#member_id option[value=' + currentBODInfo[x] +']');
		   	    jQuery('#'+x).val(currentBODInfo[x]);
		   	    selectElement.prop('selected', true);
		   	   // setOption(,currentChapterInfo[x]);
         	} else if ( x == 'chapter_id' ) {
         		selectElement = jQuery('#chapter_id option[value=' + currentBODInfo[x] +']');
		   	    selectElement.prop('selected', true);
         	} else {
         		jQuery('#'+x).val(currentBODInfo[x]);
         	}
		}
	}
	//chapterElem.value = value;
}

/**
 * This is  porotype used to save data
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

FormHandler.prototype.saveTitle = function(elem){
	title = jQuery('#title').val();
	tblName = jQuery('#tblName').val();
	operation = jQuery('#operation').val();
	reqKey = jQuery('#title').attr("name");
	id = jQuery('#titleId').val();
	jQuery.ajax({
		url: ajaxurl,
		method: "POST",
		data: { title:title,tblName:tblName,reqKey:reqKey,operation:operation,id:id,action:'save_data' },
		success : function(responseText) {
			responObj = jQuery.parseJSON(responseText);
			errors = responObj.errors;
			if( errors == '') {
				location.reload();
 			} else {
 				jQuery('div.error').remove();
 				html = '<div class="error notice"><span>'+ errors +'</span></div>';
 				jQuery('div #TB_title').append(html).fadeIn(200);;
 			}
		}

	});
	
}

/**
 * This is  porotype used to save data
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

FormHandler.prototype.saveChapter = function(elem) {
	form = jQuery('#chapter-form');
	formValue = form.serializeArray();
	jQuery.ajax({
		url: ajaxurl,
		method: "POST",
		data: { form_value:formValue,action:'save_chapter' },
		success : function(responseText) {
			var html = '';
			responObj = jQuery.parseJSON(responseText);
			errors = responObj.errors;
			if( errors == '') {
				location.reload();
 			} else {
 				jQuery('.alert-danger').remove();
 				for(var x in errors) {
 					html = '<p class="alert alert-danger">'+ errors[x] +'</p>';
					if(x == 'member_id') {
						//console.log(currentChapterInfo[x]);
						    selectElement = jQuery('#member_id option[value=' + errors[x] +']');
						    jQuery('#'+x).append(html);
						    selectElement.prop('selected', true);
						   // setOption(,currentChapterInfo[x]);
					} else if ( x == 'chapter_id' ) {
						selectElement = jQuery('#chapter_id option[value=' + errors[x] +']');
					    selectElement.prop('selected', true);
					} else {
						jQuery('#'+x).parent('td').append(html);
					}
				}
 			}
		}

	});
	
}

/**
 * This is  porotype used to save data
 * @param elem
 * @return void
 * @since 1.0
 * 
 **/

FormHandler.prototype.saveBod = function(elem) {
	form = jQuery('#bod-form');
	formValue = form.serializeArray();
	jQuery.ajax({
		url: ajaxurl,
		method: "POST",
		data: { form_value:formValue,action:'save_bod_form' },
		success : function(responseText) {
			var html = '';
			responObj = jQuery.parseJSON(responseText);
			errors = responObj.errors;
			if( errors == '') {
				location.reload();
 			} else {
 				jQuery('.alert-danger').remove();
 				for(var x in errors) {
 					html = '<p class="alert alert-danger">'+ errors[x] +'</p>';
					if(x == 'member_id') {
						//console.log(currentChapterInfo[x]);
						    selectElement = jQuery('#member_id option[value=' + errors[x] +']');
						    jQuery('#'+x).append(html);
						    selectElement.prop('selected', true);
						   // setOption(,currentChapterInfo[x]);
					} else if ( x == 'chapter_id' ) {
						selectElement = jQuery('#chapter_id option[value=' + errors[x] +']');
					    selectElement.prop('selected', true);
					} else {
						jQuery('#'+x).parent('td').append(html);
					}
				}
 			}
		}

	});
	
}

/**
* This is simple prototype to handle
* all the select options for president 
* related work
* @param  elem
* @return html
**/

FormHandler.prototype.givePresidentInfo = function(elem){
	presidentId = jQuery(elem).find(":selected").val();
	jQuery.ajax({
		url: ajaxurl,
		method: "POST",
		data: { presidentId:presidentId,action:'display_president_info' },
		success : function(responseText) {
			responObj = jQuery.parseJSON(responseText);
			if(responObj != null) {
				var mapper = {
							'first_name': 'first_name',
							'middle_name': 'middle_name',
							'last_name': 'last_name',
							'email': 'email',
							'work_phone': 'tel',
							'ext': 'ext',
							'address': 'address',
							'city': 'city',
							'state': 'state',
							'address':'address',
							'zip': 'zip',
							'id' : 'member_id',
						};
				for(var x in responObj) {
 					if(x == 'user_id') 
 						continue;
 					if(x == 'state') {
 						selectElement = jQuery('select.state option[value=' + responObj[x] +']');
					    selectElement.prop('selected', true);
					}
 					jQuery('#'+mapper[x]).val(responObj[x]);
				}
			}
		}

	});
}


/**
* This is simple prototype to handle
* all the select options for member
* related work
* @param  elem
* @return html
**/

FormHandler.prototype.getMemberInfoById = function(elem){
	memberId = jQuery(elem).find(":selected").val();
	jQuery.ajax({
		url: ajaxurl,
		method: "POST",
		data: { memberId:memberId,action:'display_member_info' },
		success : function(responseText) {
			responObj = jQuery.parseJSON(responseText);
			if(responObj.error == false) { 
				responObj = responObj.result;
				jQuery('#address').val(responObj.address);
				jQuery('#city').val(responObj.city);
				selectElement = jQuery('#chapter_id option[value=' + responObj.chapter_id +']');
				selectElement.prop('selected', true);
				jQuery('#state').val(responObj.state);
				jQuery('#zip').val(responObj.zip);
				jQuery('#phone').val(responObj.home_phone);
			}
		}

	});
}


function getChapterInfo( chapterId ) {
	var chapterInfo = null;
    jQuery.ajax({
		url: ajaxurl,
		async: false,
	    type: "POST",
	    global: false,
		data: { chapterId:chapterId,action:'get_chapter_info' },
		success : function(responseText) {
			chapterInfo =  jQuery.parseJSON(responseText);

		}
	});
	return chapterInfo;
}


function getBODInfo( bodId ) {
	var bodInfo = null;
    jQuery.ajax({
		url: ajaxurl,
		async: false,
	    type: "POST",
	    global: false,
		data: { bodId:bodId,action:'get_bod_info' },
		success : function(responseText) {
			bodInfo =  jQuery.parseJSON(responseText);

		}
	});
	return bodInfo;
}

function getCountryInfo( id ) {
	var countryInfo = null;
    jQuery.ajax({
		url: ajaxurl,
		async: false,
	    type: "POST",
	    global: false,
		data: { id:id,action:'get_country_info' },
		success : function(responseText) {
			countryInfo =  jQuery.parseJSON(responseText);

		}
	});
	return countryInfo;
}

function getStateInfo( id ) {
	var stateInfo = null;
    jQuery.ajax({
		url: ajaxurl,
		async: false,
	    type: "POST",
	    global: false,
		data: { id:id,action:'get_state_info' },
		success : function(responseText) {
			stateInfo =  jQuery.parseJSON(responseText);

		}
	});
	return stateInfo;
}

function findStateByCountryName(value) {
	var stateInfo = null;
    jQuery.ajax({
		url: ajaxurl,
		async: false,
	    type: "POST",
	    global: false,
		data: { value:value,action:'find_state_by_country_name' },
		success : function(responseText) {
			stateInfo =  jQuery.parseJSON(responseText);

		}
	});
	return stateInfo;
}

function getCityInfo( id ) {
	var cityInfo = null;
    jQuery.ajax({
		url: ajaxurl,
		async: false,
	    type: "POST",
	    global: false,
		data: { id:id,action:'get_city_info' },
		success : function(responseText) {
			cityInfo =  jQuery.parseJSON(responseText);

		}
	});
	return cityInfo;
}

function findCityByStateName(name) {
	var stateInfo = null;
    jQuery.ajax({
		url: ajaxurl,
		async: false,
	    type: "POST",
	    global: false,
		data: { name:name,action:'find_city_by_state_name' },
		success : function(responseText) {
			stateInfo =  jQuery.parseJSON(responseText);
		}
	});
	return stateInfo;
}

function delCountryById() {

}


function delStateById() {

}

function delCityById() {

}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function clearMe() {
	jQuery('input[type=search]').val('');
}

function stateTemplate(results , name ) {
	name = name || "member[city]";
	if(typeof results !== 'undefined' && results !== null && results.length > 0) {
		html = '<select class="dyn-city" name="'+ name +'" id="state_id">';
		for(var x in results) {
			firstStateName = results[0].sort_name;
			html += '<option value="'+ results[x].sort_name +'">'+ results[x].sort_name +'</option>';
		}
		html += '</select>';
	} else {
		firstStateName = null;
		html = '<input type="text" name="'+ name +'" id="state_id"/>'
	}
	return {html:html , firstStateName:firstStateName}
}

function cityTemplate(results) {
	if(typeof results !== 'undefined' && results !== null && results.length > 0) {
		html = '<select class="city_id" name="member[city]" id="select-city_id">';
		for(var x in results) {
			html += '<option value="'+ cities[x].name +'">'+ cities[x].name +'</option>';
		}
		html += '</select>';
	} else {
		html = '<input type="text" name="member[city]" id="city_id"/>';
	}
	return {html:html};
}

function getParent( elem , opt ) {
	return elem.parents(opt);
}


var f = new FormHandler();


// This is for datepicker
(function($) {
	$.fn.getType = function() { 
		return  this[0].tagName == "INPUT" ? 
				this[0].type.toLowerCase() : 
				this[0].tagName.toLowerCase(); 
	}

	getCurrentDate = function() { 
		var d = new Date();
	    month = (d.getMonth()+1) > 9 ? '0'. d.getMonth()+1 : d.getMonth()+1;
		return [d.getFullYear() , month ,  d.getDate()].join('-');
	}

	// To get state list dynamically	
	// To generate dynamically state according to country id
	$.fn.generateGeoList = function( type , appendElem) {
		$(this).live('change',function(){
			value = $(this).find(":selected").val();
			appendElemName = appendElem.children('select').attr('name');
			results = (type == 'state') ? findStateByCountryName(value) : findCityByStateName(value);
			layout = (type == 'state') ? stateTemplate(results, appendElemName) : cityTemplate(results);
			cityElem = $('#asg-city');
			if(cityElem.length > 0 && type == 'state') { 
				//cityElem = $('#asg-city');
				cities = firstStateName !== null ? findCityByStateName( firstStateName ) : null;
				template = cityTemplate(cities); 
				cityElem.children().remove();
				cityElem.append(template.html);
			}
			/*if(typeof results !== 'undefined' && results !== null && results.length > 0) {
				// first state id 
				firstStateName = results[0].name;
				for(var x in results) {
					//console.log(results[x].name);
					html += '<option value="'+ results[x].name +'">'+ results[x].name +'</option>';
				}
				html += '<option></option></select>';
				if($('#asg-city').length > 0) { 
					cities = findCityByStateName( firstStateName );
					console.log(cities);
					cityElem = $('#asg-city');
					var cityHtml = '<select class="city_id" name="member[city_id]" id="select-city_id">';
					if(typeof cities !== 'undefined' && cities.length > 0) {
						console.log('not empty');
						for(var x in cities) {
							//console.log(cities[x].name);
							cityHtml += '<option value="'+ cities[x].name +'">'+ cities[x].name +'</option>';
							cityHtml += '<option></option>';
							cityHtml += '</select>';
						}
					} else { 
						cityHtml = '<input type="text" name="member[city_id]" id="city_id"/>';
					}
					
					cityElem.children().remove();
					cityElem.append(cityHtml);
				} 
				
			} else { 
				if($('div#asg-city').length > 0) {
					$('div#asg-city').children().remove();
					$('div#asg-city').append('<input type="text" name="member[city_id]" id="city_id"/>');

				} 
				html = '<input type="text" name="member[state_id]" id="state_id"/>';
				
			}*/
			$(appendElem).children().remove();
			$(appendElem).append(layout.html);
		});
	}

	$.fn.asgDynamicReportFields = function() { 
		this.on('change' , function() {
			var endDateElem = $('input[name="end_date"]'),
				startDateElem = $('input[name="start_date"]'),
				titleElem = $('select[name="title[]"]'),
				endDateParentElem = getParent(endDateElem , 'tr'),
				startDateParentElem =getParent(startDateElem , 'tr'),
				titleParentElem = getParent(titleElem , 'tr');
				selected = $(this).find(':selected').val();
				endDateParentElem.hide();
				startDateParentElem.hide();
				titleParentElem.hide();
				switch(selected) {
					case '1':
						titleElem.val('');
						startDateParentElem.show();
						endDateParentElem.show();
						break;
					case '2':
						startDateElem.val('');
						endDateElem.val('');
						titleParentElem.show();
						break;
					default:
						titleElem.val('');
						endDateElem.val('');
						startDateParentElem.show();
						break;
				}
		});
	}
	
	$('#asg-geo-add').trigger('click');

	$('#asg-geo-add,#asg-geo-edit').on('click',function() {
		$( "form input:text" ).val('');
		child = $(this).children('button');
		form = $('form');
		var section = $('#section').val();
		var id = '';
		curText = child.text();
		chkText = $(this).data('action');
		//text = curText == 'Hide' ? chkText : 'Hide';
		//$(this).children('button').text(text);
		//$('#action').val(chkText);
		form.slideDown();
		if(chkText == 'Edit') {
			id = jQuery('#'+section).find(":selected").val();
			switch(section) {
				case 'country':
						countryInfo = getCountryInfo(id);
						for(var x in countryInfo) {
							jQuery('#'+x).val(countryInfo[x]);
						}	
						break;
				case 'state':
						stateInfo = getStateInfo(id);
						for(var x in stateInfo) {
							var elemId = '#'+x;
							elem = $(elemId);
							if(elem != 'undefined') {
								type = $(elemId).getType();
								switch(type) {
									case 'select':
											selectElement = jQuery(elemId + ' option[value=' + stateInfo[x] +']');
										    selectElement.prop('selected', true);
											break;
									case 'text': 
											jQuery(elemId).val(stateInfo[x]);
											break;
								}
							}

							//console.log(type);
							/*if(x == 'country_id') {
								selectElement = jQuery('#member_id option[value=' + errors[x] +']');
							    jQuery('#'+x).append(html);
							    selectElement.prop('selected', true);
							}
							jQuery('#'+x).val(stateInfo[x]);*/
						}	
						break;
				case 'city':
						cityInfo = getCityInfo(id);
						$('select.country-id').addClass('dyn-state');
						for(var x in cityInfo) {
							var elemId = '#'+x;
							elem = $(elemId);
							if(elem != 'undefined') {
								type = $(elemId).getType();
								switch(type) {
									case 'select':
											selectElement = jQuery(elemId + ' option[value=' + cityInfo[x] +']');
										    selectElement.prop('selected', true);
											break;
									case 'text': 
											jQuery(elemId).val(cityInfo[x]);
											break;
								}
							}

							//console.log(type);
							/*if(x == 'country_id') {
								selectElement = jQuery('#member_id option[value=' + errors[x] +']');
							    jQuery('#'+x).append(html);
							    selectElement.prop('selected', true);
							}
							jQuery('#'+x).val(cityInfo[x]);*/
						}	
						break;
				default : 
					break;
			}
		}


		form.submit(function(e){
			e.preventDefault();
			formValue = $(this).serialize();
			$.ajax({
				url: ajaxurl,
				method: "POST",
				data: { form_value:formValue,id:id,action:'asg_save_geo_location' },
				success : function(responseText) {
					responObj = $.parseJSON(responseText);
					if(responObj.result == 1) {
						$('div.message').remove();
						$('#screen-meta').after('<div class="message updated notice notice-success is-dismissible"><p> '  + section + ' ' + chkText +'ed Successfully</p></div>');
						setInterval(function(){ location.reload(); }, 1000);
					}else {
						$('div.error').remove();
						$('#screen-meta').after('<div class="error"><p>OOPS ! something got wrong, try again</p></div>');
					}
				}
			});
			
		});
		
	});

	$('#asg-geo-del').on('click',function() { 
			var section = $('#section').val();
			id = $('#'+section).find(":selected").val();
			$.ajax({
					url: ajaxurl,
					method: "POST",
					data: { id:id,section:section,action:'asg_del_geo_info' },
					success : function(responseText) {
						responObj = $.parseJSON(responseText);
						if(responObj.result == 1) {
							$('div.message').remove();
							$('#screen-meta').append('<div class="message"><p>Deleted Successfully</p></div>');
							setInterval(function(){ location.reload(); }, 1000);
						}else {
							$('div.error').remove();
							$('#screen-meta').append('<div class="error"><p>OOPS ! something got wrong, try again</p></div>');
						}
					}
			});
	});

	// To generate state dynamically
	appendElem = $('#asg-state');
	if($('.dyn-state').length > 0 ) {
		$('select.dyn-state').generateGeoList('state' , appendElem);
	}

	// To generate state dynamically
	elem = $('fieldset.membership-fees');
	if(elem.length > 0 ) {
		childInput = $('fieldset.membership-fees > input');
		$('fieldset.membership-fees :input').attr("disabled", true);

	}

	//settig default value
	elem = $('input.asg-checkbox')
	if(elem.length > 0 ) {
		childInput = elem.val(0)
	} 
	// member checkbox option
	$('input.asg-checkbox').change(function(){
	     if($(this).attr('checked')){
	          $(this).val(1);
	     }else{
	          $(this).val(0);
	     }
	});


	// asg limit save 
	if($('select#asg-select-limit').length > 0) {
		$('select#asg-select-limit').on('change', function() { 
			$('form#asg-limit').submit();
		});
	}

	var checker = 0;
	$('button.asg-membership-enable').on('click' , function() {
			if(checker == 1){
				$('.asg-membership-enable').text('Unlock');
				$('fieldset.membership-fees :input').attr("disabled", true);
				checker = 0;
				return false;
			}
			$('.asg-membership-enable').text('Lock');
			$('fieldset.membership-fees :input').attr("disabled", false);
			checker = 1;
			return false;
	});
	
	// to display tab
	$( "#tabs" ).tabs();
	
	appendElem = $('#asg-city');
	if($('.dyn-city').length > 0 ) {
		$('select.dyn-city').generateGeoList('city' , appendElem , 'id');
	}
	
	// jquery datepicker
    $('.jquery-datepicker').datepicker({
    	dateFormat: 'yy-mm-dd',
    	onSelect:function(selected){
		    	var dtMax = new Date(selected);
	            dtMax.setDate(dtMax.getDate()); 
	            var dd = dtMax.getDate();
	            var mm = dtMax.getMonth() + 1;
	            var y = dtMax.getFullYear();
	            var dtFormatted =   y + '-'+ mm  + '-'+ dd;
	            if( $("#end_date").length > 0)
	            	$("#end_date").datepicker("option", "minDate", dtFormatted)
		    }
    });
    
    // jquery datepicker
    $('#end_date').datepicker({
    	dateFormat: 'yy-mm-dd',
    	maxDate: getCurrentDate(),
    	/*onSelect:  function (selected) {
            var dtMax = new Date(selected);
            dtMax.setDate(dtMax.getDate()); 
            var dd = dtMax.getDate();
            var mm = dtMax.getMonth() + 1;
            var y = dtMax.getFullYear();
            var dtFormatted =   y + '-'+ mm  + '-'+ dd;
            $(".jquery-datepicker").datepicker("option", "maxDate", dtFormatted);
        }*/
    });

    // bulk action for delete operation
    $('input#doaction').on('click',function() {
    	
    	value = $('#bulk-action-selector-top').find(":selected").val();
    	if(value == 'bulk_delete') {
    		if(confirm('Are you sure you want to Delete?'))
    			$('form#bulk-action').submit();
    	}
    	
    });

    //chosen for multiselect jquery
     $(".asg-chosen").chosen({
	    disable_search_threshold: 10,
	    no_results_text: "Oops, nothing found!",
	    width: "95%"
	  });


    // Get unselected items in this group
	
    // Add select/deselect all toggle to optgroups in chosen
	$(document).on('click', '.group-result', function() {
	    var unselected = $(this).nextUntil('.group-result').not('.result-selected'),
	    	siblings = $(this).siblings('li.group-result');
	    val = $(this).text();
	    if(val == "None") {
	        siblings.nextUntil('.group-result').each(function() {
	            // Deselect all items in this group
	            $('a.search-choice-close[data-option-array-index="' + $(this).data('option-array-index') + '"]').trigger('click');
	        });
	        return false;
	    }
	    if(unselected.length) {
	        // Select all items in this group
	        unselected.trigger('mouseup');
	    } else {

	    }
	});

	$('select.multiple #opt-all-none').on('click' , function() {
		//selected = $(this).find(':selected').val();
		val = $(this).text();
		replace = $(this).data('val');
		siblings = $(this).siblings('option');
		curVal = $(this).val();
		changeVal = curVal == 1 ? 0 : 1;
		$(this).prop('selected', false);
		if(curVal == 1) {
			siblings.each(function(i,e) {
		            // Deselect all items in this group
		           $(e).prop('selected', true);

		        });
		} else {
			siblings.each(function(i,e) {
		            // Deselect all items in this group
		           $(e).prop('selected', false);

		        });
		}
		$(this).data('val' , val);
		$(this).val(changeVal);
		$(this).text(replace);
	});
	// select all and select none option 
	/*$(document).on('click' , 'optgroup #select-all' , function() {
		console.log('selecting all');
	});

	$(document).on('click' , 'optgroup #select-none' , function() {
		console.log('selecting none');
	});*/
	
	
	// membership card report section
	elem = $('#asg-membership-card');
	if(elem.length > 0 ) {
		$('#asg-membership-card').on('change',function(){
			elem = $( "#asg-membership-card option:selected" );
			val = elem.val();
			if(val == 1) {
				$('table tr:eq(1) input').val('');
				$('table tr:eq(1)').hide();
				$('table tr:eq(2)').show();
			} else {
				$('table tr:eq(2) input').val('');
				$('table tr:eq(1)').show();
				$('table tr:eq(2)').hide();
			}
		})
	}



    // Uploading files
	var file_frame;

	  $('#upload_image').live('click', function( event ){
	  	curImgTag = $(this).closest('td').siblings().find('img');
	  	curHiddenInput = $(this).closest('td').children(".hidden");
	  	event.preventDefault();

	    // If the media frame already exists, reopen it.
	    if ( file_frame ) {
	      file_frame.open();
	      return;
	    }
	    // Create the media frame.
	    file_frame = wp.media.frames.file_frame = wp.media({
	      title: $( this ).data( 'uploader_title' ),
	      button: {
	        text: $( this ).data( 'uploader_button_text' ),
	      },
	      scale: true,
	      multiple: false  // Set to true to allow multiple files to be selected
	    });

	    // When an image is selected, run a callback.
	    file_frame.on( 'select', function() {
	      // We set multiple to false so only get one image from the uploader
	      attachment = file_frame.state().get('selection').first().toJSON();
	      url = attachment.url;
	      curImgTag.attr("src", attachment.url);
	      curHiddenInput.val(url);
	      // Do something with attachment.id and/or attachment.url here
	    });
	    
	    // Finally, open the modal
	    file_frame.open();
	});

	// To dynamically append form reports fields
	$('#asg-print-label-report').asgDynamicReportFields();	
	
}(jQuery));