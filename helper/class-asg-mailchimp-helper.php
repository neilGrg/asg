<?php

class ASG_Mailchimp_Helper {

	private $_appId = '';
	public  $chimpy = '';	
	public 	$lists = '';

 	function __construct() { 
 		$this->_appId = apply_filters('asg_get_settings' , 'mail_chimp_api');
 		$this->_connectToMailChimp();
 		//add_action('asg_contenct_to_mailchimp' , array( $this , 'connectToMailChimp') , 10 , 1 );
		//$listId = apply_filters('asg_get_settings' , 'list_id');
 		/*add_filter('asg_display_pagination' , array( __CLASS__ , 'asgPagination') , 10 , 3 );
 		add_filter('asg_get_full_name' , array( __CLASS__ , 'getFullName') , 10 , 1 );
 		add_filter('asg_crud_message' , array( __CLASS__ , 'asgDisplayMessage') , 10 , 1 );
 		add_action('asg_check_for_nonce' , array( __CLASS__ , 'asgCheckNonce') , 10 , 1 );
 		add_filter('asg_format_setting_array' , array( __CLASS__ , 'asgSetValues') , 10 , 1 );
 		add_filter('asg_value_formatter' , array( __CLASS__ , 'formatArray') , 10 , 1 );
 		add_filter('asg_select_array_formatter' , array( __CLASS__ , 'formatArrayForSelect') , 10 , 1 );
 		add_filter('asg_ajax_form_data_formatter' , array( __CLASS__ , 'asgFormDataFormatter') , 10 , 1 );
 		add_action('asg_limit_setting' , array($this , 'asgLimitSetting') , 10 , 2 );
 		add_action('asg_view' , array($this , 'asgView') , 10 , 3 );*/
 	}

 	
 	/**
	 *
	 *	This method is used to connect to mailchimp
	 *  api
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function  _connectToMailChimp() {
		if(empty($this->_appId)) {
			trigger_error('You have to insert app id of mailchimp api to sync with it');
		}
		$this->chimpy = new ASG_MailChimp($this->_appId);
		$this->_setLists();
	}

	/**
	 *
	 *	This method is used to connect to mailchimp
	 *  api
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	private function _setLists() {
		$lists = $this->chimpy->call('lists/list');
		if(count($lists) < 1 || !array_key_exists('data', $lists) ) {
			return false;
		}
		foreach ($lists['data'] as $list) {
			$id = $list['id'];
			$this->lists[$id] = $list['name'];
		}
	}
}

