<?php 

 class ASG_Helper {

 	private $_defaultConfigs = array();

 	function __construct() { 
 		add_filter('asg_display_pagination' , array( __CLASS__ , 'asgPagination') , 10 , 3 );
 		add_filter('asg_get_full_name' , array( __CLASS__ , 'getFullName') , 10 , 1 );
 		add_filter('asg_crud_message' , array( __CLASS__ , 'asgDisplayMessage') , 10 , 1 );
 		add_action('asg_check_for_nonce' , array( __CLASS__ , 'asgCheckNonce') , 10 , 1 );
 		add_filter('asg_format_setting_array' , array( __CLASS__ , 'asgSetValues') , 10 , 1 );
 		add_filter('asg_value_formatter' , array( __CLASS__ , 'formatArray') , 10 , 1 );
 		add_filter('asg_select_array_formatter' , array( __CLASS__ , 'formatArrayForSelect') , 10 , 1 );
 		add_filter('asg_ajax_form_data_formatter' , array( __CLASS__ , 'asgFormDataFormatter') , 10 , 1 );
 		add_action('asg_limit_setting' , array($this , 'asgLimitSetting') , 10 , 2 );
 		add_action('asg_view' , array($this , 'asgView') , 10 , 3 );
 		add_action('init' , array($this , '_setDefaultLabels'));
 	}


 	/**
	 *
	 *	This method is used to set default labels 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public function _setDefaultLabels(  ) { 
 		$this->_defaultConfigs = array(
 										'menus' => array(
		 										__( 'List Of Members' , TEXT_DOMAIN ),
		 										__( 'Change Passwords' , TEXT_DOMAIN ),
		 									),
			 							'title' => __( 'Member List' , TEXT_DOMAIN ),
			 							'member_id' => __( 'Member ID' , TEXT_DOMAIN ),
			 							'name' => __( 'Name' , TEXT_DOMAIN ),
			 							'fname' => __( 'First Name' , TEXT_DOMAIN ),
			 							'lname' => __( 'Last Name' , TEXT_DOMAIN ),
			 							'chapter' => __( 'Chapter' , TEXT_DOMAIN ),
			 							'operation' => __( 'Operations' , TEXT_DOMAIN ),
			 							'add' => __( 'Add Member' , TEXT_DOMAIN ),
			 							'save' => __( ' Save Member' , TEXT_DOMAIN ),
			 							'cancel' => __( ' Cancel' , TEXT_DOMAIN ),
			 							'pdf' => __( 'Pdf' , TEXT_DOMAIN ),
			 							'edit' => __( 'Edit' , TEXT_DOMAIN ),
			 							'delete' => __( 'Delete' , TEXT_DOMAIN ),
			 							'search' => __( 'Search' , TEXT_DOMAIN ),
			 							'clear' => __( 'Clear' , TEXT_DOMAIN ),
			 							'generate_pdf' => __( 'Generate PDF' , TEXT_DOMAIN ),
 									);
 	}

 	
 	/**
	 *
	 *	This method is used to get full name
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function asgDisplayMessage(  ) { 
 		$message = get_option('asg-crud-message');
 		//echo get_option('asg-error-message');
 		//$errors = isset(get_option('asg-error-message') !== null) ? get_option('asg-error-message') : 0;
 		$class = get_option('asg-error-message') == 1 ? 'error' : 'updated';
 		delete_option('asg-crud-message');
 		delete_option('asg-error-message');
 		if(empty($message))
 			return false;
 		?>
 		<div class="<?php echo $class;?>">
 			<p><?php echo $message; ?></p>
 		</div>
 		<?php
 	}

 	/**
	 *
	 *	This method is used to get full name
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function asgLimitSetting( $name , $value = '') { 
 		$i = 5;
 		?> 
 		<div id="asg-limit-set" style="float:right">
 			<form action="" method="POST" id="asg-limit">
	 			<label for="<?php echo $name ?>"><?php echo __('Limit' , TEXT_DOMAIN); ?></label>
	 			<select  name="<?php echo $name ?>" id="asg-select-limit">
	 				<?php while($i  <  101 ): 
	 						$selected = ($i == get_option($name)) ? 'selected="selected"' : '';
	 				?>
	 					<option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
	 				<?php 
	 					$i += 5;
	 					endwhile; 
	 				?>
	 			</select>
	 			<input type="hidden" value="Save" id="asg-limit-save" /> 
	 			<input type="hidden" name="limit" value="1"  />
 			</form>
 		</div>
 		<?php
 	}

 	/**
	 *
	 *	This method is used to get full name
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function asgCheckNonce( ) { 
 		$reqNonce = isset($_REQUEST['_wpnonce']) ? $_REQUEST['_wpnonce'] : '';
 		if( wp_verify_nonce( $reqNonce, 'asg_action_nonce') !== 1) {
	 		$message = __( 'Invalid Nonce Value, Try again !', TEXT_DOMAIN );
			update_option('asg-crud-message' , $message);
			return true;
 		}
 		return false;
	}


 	/**
	 *
	 *	This method is used to get full name
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function getFullName( $member ) { 
 		if(empty($member) || count($member) <= 0)
 			return false;
 		return $member['first_name'] . ' ' . $member['middle_name']  . ' ' . $member['last_name'];
 	}

 	/**
	 *
	 *	This method is used to get full name
	 * 	@access public
	 *  @param $url
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function asgRedirect( $url ) { 
 		?>
 		<script type="text/javascript">
		   window.location.href = "<?php echo $url;?>";
		</script>
 		<?php
 	}

 	/**
	 *
	 *	This method is used to get error for errors
	 *  using key
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function getErrors( $errors , $key ) { 
 		if(empty($errors) || count($errors) <= 0 || !array_key_exists($key, $errors))
 			return false;
 		$value = $errors[$key];
 		return is_array($value) ? array_pop($value) : $value;
 	}

 	/**
	 *
	 *	This method is used to give me value if set
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function giveMeValueIfSet( $arr , $key ) {
		if(empty($arr) && count($arr) <= 0)
			return '';
		return array_key_exists($key, $arr) ? $arr[$key] : '';
	}

 	/**
	 *
	 *	This method is used to build form dynamically
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function formatArray( $postDatas = array() ) { 
 		$regExp = '/\[([^)]+)\]/';
		$formattedData = array();
		if(!is_array($postDatas))
			return false;
	 	foreach ($postDatas as $key => $value) {
	 		$name = isset($value['name']) ? $value['name'] : '';
	 		$data = isset($value['value']) ? sanitize_text_field(esc_js($value['value'])) : '';
	 		$modelName = explode('[',$name);
	 		$modelName = $modelName[0];
	 		preg_match( $regExp , $name , $match);
	 		$name = $match[1];
	 		$formattedData["$modelName"]["$name"] = $data;//array( => array("$name" => $data ));
	 	}
	 	return $formattedData;
 	}

 	/**
	 *
	 *	This method is used to build form dynamically
	 *	for auto login form
	 * 	@access public
	 *  @param $title
	 *  @param $pages
	 *  @param $url
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function asgPagination( $title , $pages , $url = '' ) { 
 		global $wp_query;
		$big = 999999999; // need an unlikely integer
		$translated = __( $title , TEXT_DOMAIN ); // Supply translatable string
		$current = isset($_REQUEST['paged']) ? absint($_REQUEST['paged']) : 1;
		$url = !empty($url) ? $url . '%_%' : str_replace( $big, '%#%', get_pagenum_link( $big ) );
		$link = paginate_links( array(
			'base' => $url,//str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => $current,
			'total' =>  $pages,
			'prev_text'          => __( 'Previous page', TEXT_DOMAIN ),
		    'next_text'          => __( 'Next page', TEXT_DOMAIN ),
		    //'type' => 'list',
		    'before_page_number' => '<span class="screen-reader-text">'. $translated .' </span>'
		));
		/*debug(array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => $current,
			'total' =>  $pages,
			'prev_text'          => __( 'Previous page', TEXT_DOMAIN ),
		    'next_text'          => __( 'Next page', TEXT_DOMAIN ),
		    //'type' => 'list',
		    'before_page_number' => '<span class="screen-reader-text">'. $translated .' </span>'
		));die;*/
		//apply_filters('asg_pagination' , $link);
		return str_replace('#038;', '&', $link);
		//return $link;
 	}

 	/**
	 *
	 *	This method is used to build form dynamically
	 *	for auto login form
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function asgSetValues( $settingValues = array()) {  //die('hell');
 		if(!is_array($settingValues) && count($settingValues) < 1) 
 			return false;
 		$formattedData = array();
 		foreach ($settingValues as $key => $value) {
 			$v = array_pop($value);
 			$k = array_pop($value);
 			$formattedData["$k"] = $v;
 		}
 		return $formattedData;
 	}

 	/**
	 *
	 *	This method is used to build form dynamically
	 *	for auto login form
	 *  @param $rawFormData
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

 	public static function asgFormDataFormatter( $rawFormData) {  
 		$rawString = rawurldecode($rawFormData);
 		// To remove square brackets
	    $value = str_replace(array('[',']'),'',$rawString);
	    // To generate key ($r[1]) & value ($r[2])
	    preg_match_all("/([^&= ]+)=([^&= ]+)/", $value, $r); 
	    // combine these two array as key & value
	    $result = array_combine($r[1], $r[2]);
	    return $result;
 	}

 	/**
	 *
	 *	This method is used for formatting array
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	public static function formatArrayForSelect ($data) { 
		$customArr = array();
		if(!is_array($data))
			return false;
		foreach ($data as $key => $value) {
			$v = array_pop($value);
			$k = array_pop($value);
			$customArr[$k] = $v;
			
			
		}
		return $customArr;
	}

	/**
	 *
	 *	This is generic method to include file in view folder
	 *	for asg 
	 * 	@access public
	 * 	@author 
	 * 	@since  1.0
	 * 	@return void
	 * 	
	 */

	function asgView($path , $filename,$data = array()) {
		if(is_array($data))	
			extract($data);
		include_once( $path . DS . $filename .'.php');
	}


	function saveLimit() {
		unset($_POST['action']);
		$post = array_map( 'sanitize_text_field', $_POST );
		if(!empty($post))
			foreach ($post as $key => $value) {
				update_option($key , $value);
			}
		wp_die();
	}


}

//new ASG_Helper();